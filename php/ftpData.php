<?php

class ftpData {
    //取ftp的ip跟port
    function getFtpIpAndPort($db) {
        $rs = $db -> query(" select * from ftp_config ");   
        while($rows = $rs -> fetch())
        {
            //echo $rows['ftp_ip']."\t".$rows['ftp_port']."\n";
            $ftp_ip = $rows['ftp_ip'];
            $ftp_port = $rows['ftp_port'];        
        }
        return array($ftp_ip,$ftp_port);
    }

    //依vender的id查出ftp登入帳號/密碼
    function getFtpAccountAndPwdByVenderId($vender_id, $db) {
        $sql = "select *  from vender where id=?";
        $stmt = array();
        $stmt[1] = $vender_id;
        $rows = $db->exec($sql, $stmt);
        foreach ($rows as $row) {
            $ftp_user = $row['ftp_user'];
            $ftp_pass = $row['ftp_pass'];
        }
        return array($ftp_user,$ftp_pass);
    }
}
?>

