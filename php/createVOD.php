<?php
/**
 * POST METHOD
 * 
 * @version 1.0
 * @author rickeysu@cht.com.tw
 * @date 03/27/19
 * @since 1.0 CHANNEL TO VOD 提供api，頻道自動轉至VOD內容資料
 */
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

/**
 * @API功能：頻道自動轉至VOD內容資料
 * @Param JSON 新增或更新欄位VOD且STATUS=4，URLS新增至URL資料表及JOB_ID
 * {
 *     "company_name":"中華", 
 *     "company_code"=>"CHT",
 *     "channel_name":"CHT_CHANNEL",
 *     "vod_title":"text0402",
 *     "urls":[
 *          	{format："aa",quality："bb",devie："cc",drm："dd",url："ee"},
 *           	{format："aa2",quality："bb2",devie："cc2",drm："dd2",url："ee2"}
 *  	],
 *     "optional"=>"其他"
 * }
 * @RETURN　JSON 回傳成功或失敗訊息
 *     400 =>bad request
 *     404 =>no company data
 *     200 =>ok
 */
function create($db, $data)
{
    if (json_encode($data) == "null") {
        $msg = array('status' => "Fail",
            'code' => "400",
            'text' => "Bad Request");
		echo json_encode($msg, JSON_UNESCAPED_UNICODE);
		return "";
    } else {
		//step1：檢查request的company_code格式
        $sql = " select * from company where 1=1 ";
        $whereStmt = array();
        $i = 1;
        if (!empty($data[0]->company_code) && $data[0]->company_code != 'undefined') {            
            $sql .= " and company = ?";
            $whereStmt[$i++] = $data[0]->company_code;
        } else {
			$msg = array('status' => "Fail",
			'code' => "400",
			'text' => "Bad Request");
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);
			return "";
        }
        // echo $sql.'wherStmt'.print_r($whereStmt);
        $rows = $db->exec($sql, $whereStmt);
        // echo 'company exists=====' . count($rows);

        //step2：檢查公司資料表，request company_code存不存在
        if (count($rows) <= 0) {
            $msg = array('status' => "Fail",
                'code' => "404",
                'text' => "No Company Data Found");
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);
			return "";
        } else {
            $company_id = $rows[0]['id'];           

            //step3：檢查 vod　資料表中的 company_code 及 VOD_TITLE 的存不存在
            $sql = " select c.* from content  c where 1=1 ";
            $whereStmt = array();
            $i = 1;
            if (!empty($data[0]->vod_title) && $data[0]->vod_title != 'undefined'
                && !empty($data[0]->company_code) && $data[0]->company_code != 'undefined') {
                $sql .= " and c.title = ? and c.company_id = ?";
                $whereStmt[$i++] = $data[0]->vod_title;
                $whereStmt[$i++] = (int) $company_id;
            }

            // echo $sql.'wherStmt'.print_r($whereStmt);
            $rows = $db->exec($sql, $whereStmt);
            // echo 'vod content exists=====' . count($rows);
            //STEP3-1： content 的代碼存在，則更新
            if (count($rows) > 0) {
                $content['id'] = $rows[0]['id'];
                //替換company的id
                $id = $rows[0]['id'];
                $job_id = $rows[0]['job_id'];
                //更新vod content的channel_name
                if (!empty($data[0]->channel_name) && $data[0]->channel_name != "undefined") {
                    $content['channel_name'] = $data[0]->channel_name;
                    $content['status'] = "4";//已審核
                    $content_json = json_encode(array($content));
                    $content_arr = json_decode($content_json);
                    rest::update($db, $content_arr, 'content', false);
                }                
                //先刪除url的job_id再新增
                if (!empty($data[0]->urls) && $data[0]->urls != "undefined") {                    
                    $jobsql = " delete from url where job_id=? ";
                    $whereStmt = array();
                    $i = 1;
                    $whereStmt[$i++] = $job_id;
                    $db->exec($jobsql, $whereStmt);
                     
                    $data = json_decode(json_encode($data[0]->urls, JSON_UNESCAPED_UNICODE));
                    //print_r($data);//{format："aa",quality："bb",devie："cc",drm："dd",url："ee"},
                    foreach ($data as $value) {                        
                        // echo "url=".$urldata['format'].','.$urldata['quality'].','.$urldata['devie'].','.$urldata['drm'].','.$urldata['url'];
                        $url['job_id'] = $job_id;
                        $url['format'] = $value -> format;
                        $url['quality'] = $value -> quality;
                        $url['device'] = $value -> device;
                        $url['drm'] = $value -> drm;
                        $url['url'] = $value -> url;
                        $url_json = json_encode(array($url));
                        $url_arr = json_decode($url_json);
                        rest::create($db, $url_arr, 'url', false);
                    }
                }  
                
				$msg = array('status' => "OK",
                    'code' => "200",
                    'text' => "Update Success");
            } else {
                //STEP3-2檢查 content 的代碼不存在，則先捉MAC(url.JOB_ID) + 1 新增至 CONTENT/URL資料表
                $jobsql = " select max(job_id) + 1 as job_id from url ";
                $jobrows = $db->exec($jobsql);
                $job_id = "1";
                if (count($jobrows) > 0) {
                    $job_id = $jobrows[0]['job_id'];
                }
                $content['company_id'] = $company_id;
                $content['title'] = $data[0]->vod_title;
                $content['channel_name'] = $data[0]->channel_name;
                $content['job_id'] = $job_id;
                $content['status'] = "4";//已審核
                $content_json = json_encode(array($content));
                $content_arr = json_decode($content_json);
                rest::create($db, $content_arr, 'content', false);
                
                if (!empty($data[0]->urls) && $data[0]->urls != "undefined") {
                    $data = json_decode(json_encode($data[0]->urls, JSON_UNESCAPED_UNICODE));
                    // print_r($data);//{format："aa",quality："bb",devie："cc",drm："dd",url："ee"},
                    foreach ($data as $value) {                                                
                        $url['job_id'] = $job_id;
                        $url['format'] = $value -> format;
                        $url['quality'] = $value -> quality;
                        $url['devie'] = $value -> devie;
                        $url['drm'] = $value -> drm;
                        $url['url'] = $value -> url;
                        $url_json = json_encode(array($url));
                        $url_arr = json_decode($url_json,true);
                        rest::create($db, $url_arr, 'url', false);
                    }
                }                  
               
				$msg = array('status' => "OK",
                    'code' => "200",
                    'text' => "Insert Success");
			}
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);
			return "";
        }
    }

}
