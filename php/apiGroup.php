<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){
	$perpage = 10;
	$page = 1;
	if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
	}

	$sql = "SELECT g.* 
		FROM  `group` as g		
		where  g.company_id=?";
	$whereSql = "";
	$whereStmt = array();  	
	$i=1;
	$whereStmt[$i++] = $_SESSION['company_id'];
	if ($data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
        $whereSql .= " and g.code like ?";
        $whereStmt[$i++] = '%' . $data['searchTitle'] . '%';
    }

    if (!empty($data['id'])) {
        $whereSql .= " and g.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
    }
	
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
    } 
    // echo print_r($orderSql).','.print_r($orderStmt);
    $rows = $db->exec($orderSql, $orderStmt);    

	$rs = array("data" =>array());
	$i = 0;
	foreach($rows as $row) {
        $accounts = getAccounts($db, $row, $company);
        if (count($accounts) > 0) {
            $row['account_name'] = $accounts['account_name'];
		}
		$venders = getVenders($db,$row,$company);	 
		if(count($venders)>0){			
			$row['vender_name'] = $venders['name'];	
		}
		$rs["data"][$i] = $row;
		$i++;
	}

	$count_sql = "SELECT COUNT(*)  as total from `group` as g        
		where g.company_id=? ";
	$count_sql .= $whereSql;
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
	}
	rest::send($rs);
}

function getAccounts($db, $row, $company)
{
    $accounts = array("account_name" => array());
    $sql = "SELECT a.name account_name
	FROM  group_account ga
	left join account a on a.id=ga.account_id
	where ga.group_id=?";
    $stmt = array();
    $j = 1;
    $stmt[$j++] = $row['id'];    
    $rs = $db->exec($sql, $stmt);
    $accounts = array();
    for ($i = 0; $i < count($rs); $i++) {
        $accounts['account_name'][$i] = $rs[$i]['account_name'];
    }
    return $accounts;
}

function getVenders($db,$row,$company){
	$venders = array("id"=>array(),"name"=>array());
	$sql = "select v.name,v.id from group_vender gv 
			left join vender v on v.id = gv.vender_id
			where gv.group_id=? ";
	$stmt = array(); $i=1;
	$stmt[$i++] = $row['id'];	
	$rs = $db->exec($sql,$stmt);
	$venders = array();
	$vender_names = array();
	for($i=0;$i<count($rs);$i++) {
		$venders['id'][$i] = $rs[$i]['id'];
		$venders['name'][$i] = $rs[$i]['name'];
	}
	return $venders;
}

function create($db, $data)
{
	$sql = "select max(id)+1 id from `group` ";
    $rows = $db->exec($sql);
    foreach ($rows as $row) {
        echo 'id====' . $row['id'];
		$id = $row['id'];
		
		//新增群組對帳號資料對應表
        if (!empty($data[0]->account_list)) {
            foreach ($data[0]->account_list as $account) {
                $count = 0;
                $newid = 0;
                foreach ($account as $key => $value) {
                    $count++;
                    if ($count == 1 && $key == "id") {
                        $a['id'] = $value;
                        $newid = $value;
                    }
                }
                // echo 'add content_tag.new_id=' . $newid . "content_id=" . $id;
                $group_account['group_id'] = (int) $id;
                $group_account['account_id'] = (int) $newid;
                echo $group_account_json = json_encode(array($group_account));
                echo 'add group_account table' . print_r($group_account_arr = json_decode($group_account_json));
                rest::create($db, $group_account_arr, 'group_account', false);
            }
		}
		
		//新增群組對內容商資料對應表
        if (!empty($data[0]->vender_list)) {
            foreach ($data[0]->vender_list as $vender) {
                $count = 0;
                $newid = 0;
                foreach ($vender as $key => $value) {
                    $count++;
                    if ($count == 1 && $key == "id") {                        
                        $newid = $value;
                    }
                }
                // echo 'add content_tag.new_id=' . $newid . "content_id=" . $id;
                $group_vender['group_id'] = (int) $id;
                $group_vender['vender_id'] = (int) $newid;
                $group_vender_json = json_encode(array($group_vender));
                $group_vender_arr = json_decode($group_vender_json);
                rest::create($db, $group_vender_arr, 'group_vender', false);                
            }
        }
    }
    $data[0]->id = $id;
	rest::create($db, $data, 'group', false);
}

function update($db, $data)
{
	rest::update($db, $data, 'group', false);
	
	//echo print_r($data);
	//先刪除再新增group_account	
	$delSql = "delete from group_account where group_id=?";
	$delStmt = array();
	$i = 1;
	$delStmt[$i++] = $data[0]->id;
	$db->exec($delSql, $delStmt);
    if (!empty($data[0]->account_list)) {        
        $group_id = $data[0]->id;
        foreach ($data[0]->account_list as $account) {
            $count = 0;
            $newid = 0;
            foreach ($account as $key => $value) {
                $count++;
                if ($count == 1 && $key == "id") {
                    $newid = $value;
                }
            }
            $group_account['group_id'] = (int) $group_id;
            $group_account['account_id'] = (int) $newid;
            $group_account_json = json_encode(array($group_account));
            $group_account_arr = json_decode($group_account_json);
            rest::create($db, $group_account_arr, 'group_account', false);
            echo $count;
        }
	}

	//先刪除再新增 group_vender
	 //先刪除content_id中content_id，重新新增一次
	 $delSql = "delete from group_vender where group_id=?";
	 $delStmt = array();
	 $i = 1;
	 $delStmt[$i++] = $data[0]->id;
	 $db->exec($delSql, $delStmt);
    if (!empty($data[0]->vender_list)) {       
        $group_id = $data[0]->id;
        foreach ($data[0]->vender_list as $vender) {
            $count = 0;
            $newid = 0;
            foreach ($vender as $key => $value) {
                $count++;
                if ($count == 1 && $key == "id") {
                    $newid = $value;
                }
            }
            $group_vender['group_id'] = (int) $group_id;
            $group_vender['vender_id'] = (int) $newid;
            $group_vender_json = json_encode(array($group_vender));
            $group_vender_arr = json_decode($group_vender_json);
            rest::create($db, $group_vender_arr, 'group_vender', false);
        }
	}	
}

function delete($db, $data)
{
    $delSql = "delete from group_account where group_id=?";
	$delStmt = array();
	$i = 1;
    $delStmt[$i++] = $data[0]->id;
    $db->exec($delSql, $delStmt);
    
    $delSql = "delete from group_vender where group_id=?";
    $delStmt = array();
    $i = 1;
    $delStmt[$i++] = $data[0]->id;
    $db->exec($delSql, $delStmt);

    rest::delete($db, $data, 'group');
}

?>