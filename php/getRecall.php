<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data){	
	$day = 10;
	if (!empty($data['recallDay'])) {
		$day = (int) $data['recallDay'];
	}
	
	// if(empty($data['id'])) rest::state(400,'存取方式錯誤'); //bad request\
	//判斷end_time已過期n小時：(現時間-n小時) < end_time < 現在時間
	$sql = "SELECT 
		c.id,
		IFNULL(vender.name,'') as vender,
		IFNULL(c.title,'') as title,
		IFNULL(main.name,'') as category,
		IFNULL(sub.name,'') as subcategory,
		IFNULL(c.description,'') as description,		
		IFNULL(c.thumbnail,'') as thumbnail,
		IFNULL(c.start_time,'') as start_time,
		IFNULL(c.end_time,'') as end_time,
		IFNULL(c.publish_year,'') as publish_year,
		IFNULL(c.create_time,'') as create_time,
		IFNULL(c.run_time,'') as run_time,
		IFNULL(c.update_time,'') as update_time,
		c.job_id	
		FROM content c
		left join category main on main.id = c.category_id
		left join subcategory sub on sub.id = c.subcategory_id
		left join status on status.code = c.status
		left join vender on vender.id = c.vender_id				
		left join rss_config rss on rss.company_id=c.company_id
		where status in (3,4)  and rss.token=?
		and UNIX_TIMESTAMP(c.end_time) - UNIX_TIMESTAMP(DATE_ADD(now(), INTERVAL - rss.recall_hour hour))   >= 0 
		and UNIX_TIMESTAMP(c.end_time) - UNIX_TIMESTAMP(NOW()) < 0 ";
		

		$whereStmt = array();
		$i = 1;
		$whereStmt[$i++] = $data['token'];					
		
		if (!empty($data['limit_rows']) && $data['limit_rows'] != 'undefined') {
			$whereStmt[$i++] = (int) $data['limit_rows'];
			$sql .= " limit ? ";
		} else {
			$sql .= " limit 1000 " ;
		}
		$rows = $db->exec($sql, $whereStmt);
    
    // if(count($rows)==0) rest::state(404,'查無此服務或服務中止'); //no content
				

		$rs = array( "title"=>"HamiVideo 企業影視服務",
			"copyright"=>"中華電信數據通信分公司版權所有 © 2008 HiNet Internet Service by Chunghwa Telecom All Rights Reserved.",
			"version"=>"1.0",
			"contents" => array());
    $i = 0;
    foreach ($rows as $row) {
		// if($row['job_id']!=null){
		$tagSql = sprintf("select  text from  content_tag ct ,tag t 
		   where  ct.tag_id=t.id and ct.content_id=%s", (int)$row['id']);
		$tagData = $db->exec($tagSql);
		if(count($tagData)!=0) {
			//  echo print_r($tagData);			 
			 $tagArray = array();
			 $j = 0;
			 foreach ($tagData as $tag) {
				array_push($tagArray, $tag['text']);;
				 $j ++;
			 }
			 $row['tag'] = $tagArray;
		} else {
			$row['tag'] = "";
		}
		$urlSql = sprintf("select  format,device,quality,drm,url from  url 
			where  job_id=%s", (int)$row['job_id']);
		$urlData = $db->exec($urlSql);
		if(count($urlData)!=0) {
			//  echo print_r($tagData);			 
			 $Array = array();
			//  $j = 0;
			//  foreach ($urlData as $url) {
			// 	array_push($tagArray, $tag['text']);;
			// 	 $j ++;
			//  }
			 $row['urls'] = $urlData;
		} else {
			$row['urls'] = array();
		}
		unset($row['job_id']);
		$rs["contents"][$i] = $row;
        $i++;

	}
	rest::send($rs);
}
?>