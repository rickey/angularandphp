<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){
	$perpage = 10;
	$page = 1;
	if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
	}

	$sql = "SELECT l.*,c.company_name,
			lu.id lu_id, lu.device,lu.format,lu.quality,lu.drm,lu.url
			 FROM   live l
			left join company c on c.id = l.company_id	
			left join live_url lu on  l.id=lu.live_id  
			where 1=1  and c.id=? ";
	$whereSql = "";
	$whereStmt = array();
	$i = 1;
	$whereStmt[$i++] = (int)$_SESSION['company_id'];
	if (!empty($data['searchTitle'])  && $data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
		$str = "%".$data['searchTitle']."%";
		$whereSql .= " and (l.title like ?)";
		$whereStmt[$i++] = $str;
	}

	if (!empty($data['lu_id'])) {
        $whereSql .= " and lu.id = ?";
        $whereStmt[$i++] = (int) $data['lu_id'];
	}	 
	
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 

	// echo print_r($data);
	// echo print_r($orderSql).','.print_r($orderStmt);
	$rows = $db->exec($orderSql, $orderStmt);    
	
	$rs = array("data" =>array());
	$i = 0;
	foreach($rows as $row) {		
		$rs["data"][$i] = $row;
		$i++;
	}

	$count_sql = "SELECT COUNT(*)  as total FROM  live l
		left join company c on c.id=l.company_id
		left join live_url lu  on   l.id=lu.live_id
		where 1=1  and c.id=? ";
	$count_sql .= $whereSql;	
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
	}

	rest::send($rs);
}
 

function create($db,$data){
	 //echo print_r($data);
	//rest::create($db,$data,'live',false);

	//檢查company的代碼不存在，則新增
	$db->begin();
	$live_id = "";
	foreach ($data as $d) {
		$c = new DB\SQL\Mapper($db,'live');        
		$c = rest::copyFrom($c,$d);     
		$c->id = 0;
		$c->title = $data[0]->title;
		$c->start_time =  $data[0]->start_time;
		$c->end_time = $data[0]->end_time;
		$c->save();	       
		//echo 'content last id===='.$c->id;       
		$live_id = $c->id; 
		$db->commit();
		
	}
	if(
		 (!empty($data[0]->device) && $data[0]->device != 'undefined') ||
		(!empty($data[0]->format) && $data[0]->format != 'undefined') ||
		(!empty($data[0]->quality) && $data[0]->quality != 'undefined') ||
		(!empty($data[0]->drm) && $data[0]->drm != 'undefined') ||
		(!empty($data[0]->url) && $data[0]->url != 'undefined') 
	  ) {
		$live_url['live_id'] = (int) $live_id;
		$live_url['device'] = empty($data[0]->device) ?"":$data[0]->device;
		$live_url['format'] = empty($data[0]->format) ?"":$data[0]->format;
		$live_url['quality'] = empty($data[0]->quality) ?"":$data[0]->quality;
		$live_url['drm'] = empty($data[0]->drm) ?"":$data[0]->drm;
		$live_url['url'] = empty($data[0]->url) ?"":$data[0]->url;
		$live_url_json = json_encode(array($live_url));
		$live_url_arr = json_decode($live_url_json);
		rest::create($db, $live_url_arr, 'live_url', false);
	}

	if(
		(!empty($data[0]->device2) && $data[0]->device2 != 'undefined') ||
	   (!empty($data[0]->format2) && $data[0]->format2 != 'undefined') ||
	   (!empty($data[0]->quality2) && $data[0]->quality2 != 'undefined') ||
	   (!empty($data[0]->drm2) && $data[0]->drm2 != 'undefined') ||
	   (!empty($data[0]->url2) && $data[0]->url2 != 'undefined') 
	 ) {
		$live_url2['live_id'] = (int) $live_id;
		$live_url2['device'] = empty($data[0]->device2) ?"":$data[0]->device2;
		$live_url2['format'] = empty($data[0]->format2) ?"":$data[0]->format2;
		$live_url2['quality'] = empty($data[0]->quality2) ?"":$data[0]->quality2;
		$live_url2['drm'] = empty($data[0]->drm2) ?"":$data[0]->drm2;
		$live_url2['url'] = empty($data[0]->url2) ?"":$data[0]->url2;
		$live_url_json2 = json_encode(array($live_url2));
		$live_url_arr2 = json_decode($live_url_json2);
		rest::create($db, $live_url_arr2, 'live_url', false);
	}	

}

function update($db,$data){
	rest::update($db,$data,'live',false);
	 
	$delSql = "update live_url set device=?,format=?,quality=?,drm=?,url=? where id=?";
	$delStmt = array();
	$i = 1;
	$delStmt[$i++] = $data[0]->device;
	$delStmt[$i++] = $data[0]->format;
	$delStmt[$i++] = $data[0]->quality;
	$delStmt[$i++] = $data[0]->drm;
	$delStmt[$i++] = $data[0]->url;
	$delStmt[$i++] = $data[0]->lu_id;
	$db->exec($delSql, $delStmt);
}

function delete($db,$data){
	rest::delete($db,$data,'live');
}
 
?>