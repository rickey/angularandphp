<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data){
	// echo print_r(urlencode($data['token']));
	$sql = "SELECT distinct
		l.id,IFNULL(l.title,'') as title,l.start_time,l.end_time
		FROM live l		
		left join liverss_config rss on rss.company_id=l.company_id
		left join live_epg l_epg on l_epg.live_id=l.id
		where  rss.token=?
		and  (UNIX_TIMESTAMP(DATE_ADD(concat(DATE_FORMAT(now(), '%Y-%m-%d') , ' 00:00:00'), INTERVAL + rss.recall_day Day))  - UNIX_TIMESTAMP(l_epg.end_time)) >= 0
		and  (UNIX_TIMESTAMP(concat(DATE_FORMAT(now(), '%Y-%m-%d') , ' 00:00:00')) - UNIX_TIMESTAMP(l_epg.end_time)) < 0 ";//status in (3,4) AND NOW < epg.end_time < now+ N Day
		$whereStmt = array();
		$i = 1;
		$whereStmt[$i++] = $data['token'];			
	
	if (!empty($data['limit_rows']) && $data['limit_rows'] != 'undefined') {
		$whereStmt[$i++] = (int) $data['limit_rows'];
		$sql .= " limit ? ";
	} else {
		$sql .= " limit 1000 " ;
	}
	
	//echo $sql;	
	$whereSql = "";
	$whereSql .= $sql;
	$rows = $db->exec($whereSql,$whereStmt);
    
    //if(count($rows)==0) rest::state(404,'查無此服務或服務中止'); //no content			
	
	$rs = array( "title"=>"HamiVideo 企業影視服務",
		"copyright"=>"中華電信數據通信分公司版權所有 © 2008 HiNet Internet Service by Chunghwa Telecom All Rights Reserved.",
		"version"=>"1.0",
		"channel" => array());
    $i = 0;
    foreach ($rows as $row) {		
		$sql = "select  
		   IFNULL(l_url.format,'') as format, 
		   IFNULL(l_url.device,'') as device, 
		   IFNULL(l_url.quality,'') as quality, 
		   IFNULL(l_url.drm,'') as drm, 
		   IFNULL(l_url.url,'') as url
		   from  live_url l_url 
		   where l_url.live_id=?
		  ";
		$whereStmt = array();
		$j = 1;
		$whereStmt[$j++] = (int)$row['id'];				
		$liveUrlData = $db->exec($sql, $whereStmt);
		if(count($liveUrlData)!=0) {
			 $row['urls'] = $liveUrlData;
		} else {
			$row['urls'] = "";
		}
		$sql = "select  
				IFNULL(l_epg.text,'') as text, 
				IFNULL(l_epg.start_time,'') as start_time, 
				IFNULL(l_epg.end_time,'') as end_time
			from  live_epg l_epg			
			left join live l		on l.id=l_epg.live_id
			left join liverss_config rss on rss.company_id=l.company_id
			where live_id=?
			and  (UNIX_TIMESTAMP(DATE_ADD(concat(DATE_FORMAT(now(), '%Y-%m-%d') , ' 00:00:00'), INTERVAL + rss.recall_day Day))  - UNIX_TIMESTAMP(l_epg.end_time)) >= 0
			and  (UNIX_TIMESTAMP(concat(DATE_FORMAT(now(), '%Y-%m-%d') , ' 00:00:00')) - UNIX_TIMESTAMP(l_epg.end_time)) < 0 ";
		$whereStmt = array();
		$j = 1;
		$whereStmt[$j++] = (int)$row['id'];			
		$liveEpgData = $db->exec($sql, $whereStmt);
		if(count($liveEpgData)!=0) {
			 $row['epgs'] = $liveEpgData;
		} else {
			$row['epgs'] = array();
		}
		unset($row['id']);
		$rs["channel"][$i] = $row;
        $i++;

	}
	rest::send($rs);
}
?>