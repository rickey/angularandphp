<?php
/**
 * POST METHOD
 * 
 * @version 1.0
 * @author rickeysu@cht.com.tw
 * @date 03/27/19
 * @since 1.0 修改取資料表取最大值，由sql改用框架去取新增完後的id值
 */
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

/**
 * @API功能：新增或更新至內容商資料表(domain_name)
 * @Param JSON 新增或更新欄位vender
 * {
 *     "vender_name"=>"HBO電影台1",
 *     "vender_code"=>"HBO3",
 *     "company_code"=>"CHT",
 *     "domain_names"=>["domain.name1", "domain.name2"],
 *     "optional"=>"其他"
 * }
 * @RETURN　JSON 回傳成功或失敗訊息
 *     400 =>bad request
 *     404 =>no company data
 *     200 =>ok
 */
function create($db, $data)
{
    if (json_encode($data) == "null") {
        $msg = array('status' => "Fail",
            'code' => "400",
            'text' => "Bad Request");
		echo json_encode($msg, JSON_UNESCAPED_UNICODE);
		return "";
    } else {
		//step1：檢查request的company_code格式
        $sql = " select * from company where 1=1 ";
        $whereStmt = array();
        $i = 1;
        if (!empty($data[0]->company_code) && $data[0]->company_code != 'undefined') {            
            $sql .= " and company = ?";
            $whereStmt[$i++] = $data[0]->company_code;
        } else {
			$msg = array('status' => "Fail",
			'code' => "400",
			'text' => "Bad Request");
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);
			return "";
        }
        // echo $sql.'wherStmt'.print_r($whereStmt);
        $rows = $db->exec($sql, $whereStmt);
        // echo 'company exists=====' . count($rows);

        //step2：檢查公司資料表，request company_code存不存在
        if (count($rows) <= 0) {
            $msg = array('status' => "Fail",
                'code' => "404",
                'text' => "No Company Data Found");
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);
			return "";
        } else {
            $company_id = $rows[0]['id'];           

            //step3：檢查vender資料表中的company_code及vendoer_code的存不存在
            $sql = " select v.* from vender  v where 1=1 ";
            $whereStmt = array();
            $i = 1;
            if (!empty($data[0]->vender_code) && $data[0]->vender_code != 'undefined'
                && !empty($data[0]->company_code) && $data[0]->company_code != 'undefined') {
                $sql .= " and v.un = ? and v.company_id = ?";
                $whereStmt[$i++] = $data[0]->vender_code;
                $whereStmt[$i++] = (int) $company_id;
            }

            // echo $sql.'wherStmt'.print_r($whereStmt);
            $rows = $db->exec($sql, $whereStmt);
            // echo 'vender exists=====' . count($rows);
            //STEP3-1：vender 的代碼存在，則更新
            if (count($rows) > 0) {
                $vender['id'] = $rows[0]['id'];
                //替換company的id
                $id = $rows[0]['id'];
                $vender['name'] = $data[0]->vender_name;
                if (!empty($data[0]->domain_names) && $data[0]->domain_names != "undefined") {
                    $vender['domain_names'] = json_encode($data[0]->domain_names, JSON_UNESCAPED_UNICODE);
                }  
                $vender_json = json_encode(array($vender));
                $vender_arr = json_decode($vender_json);
				rest::update($db, $vender_arr, 'vender', false);
				$msg = array('status' => "OK",
                    'code' => "200",
                    'text' => "Update Success");
            } else {
                //STEP3-2檢查 vender 的代碼不存在，則新增
                $vender['company_id'] = $company_id;
                $vender['un'] = $data[0]->vender_code;
                $vender['name'] = $data[0]->vender_name;
                if (!empty($data[0]->domain_names) && $data[0]->domain_names != "undefined") {
                    $vender['domain_names'] = json_encode($data[0]->domain_names, JSON_UNESCAPED_UNICODE);
                }                  
                $vender_json = json_encode(array($vender));
                $vender_arr = json_decode($vender_json);
				rest::create($db, $vender_arr, 'vender', false);
				$msg = array('status' => "OK",
                    'code' => "200",
                    'text' => "Insert Success");
			}
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);
			return "";
        }
    }

}
