<?php 
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=big5");
    header("Content-disposition: attachment; filename=uploadContent.csv");
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    // 這裏要注意是\r\n是Windows的換行符號，\n為Unix的換行符號
    $content = "匯入Content資料，請依下列步驟進行修改:" . "\r\n";
    $content .= "1.選擇要匯入的欄位並在第一行輸入欄位名稱(用逗號隔開)，欄位對照如下：" . "\r\n";
    $content .= "----必填----" . "\r\n";
    $content .= "   所屬內容商(vender_name) : 至本系統查詢" . "\r\n";
    $content .= "   標題(title) "  . "\r\n";
    $content .= "   主分類(category) : 至本系統查詢" . "\r\n";
    $content .= "   次分類(subcategory) : 至本系統查詢" . "\r\n";
    $content .= "----選填----" . "\r\n";
    $content .= "   開始時間(start_time) : ex:2019-03-01 00:00:00" . "\r\n";
    $content .= "   結束時間(end_time) : : ex:2019-12-31 23:59:59" . "\r\n";
    $content .= "   檔案路徑(file_path)，檔案路徑格式第一碼請輸入符號為/，限英文或數字和小位數" . "\r\n";
    // $content .= "   發行年份(publish_year)" . "\r\n";
    // $content .= "   標籤(tag)" . "\r\n";
    $content .= "2.第二行開始輸入要匯入資料" . "\r\n";
    $content .= "3.若要另外建立新檔案，請於儲存時將編碼選為UTF-8" . "\r\n";
    $content .= "以下為簡單範例" . "\r\n";
    $content .= "----------------------以上請刪除-----------------------------" . "\r\n";
    $content .= "vender_name,title,category,subcategory,start_time,end_time" . "\r\n";//,file_path
    $content .= "hichannel,title13,電影,浪漫,2019-03-01 00:00:00,2015-03-31 23:59:59" . "\r\n";//,/file123.mpg
    $content .= "hichannel,title14,電影,浪漫,2019-03-01 00:00:00,2015-03-31 23:59:59" . "\r\n";//,/file567.mpg

    // 將字串編碼由utf-8轉為big5(非必要)
    $content = iconv("utf-8", "utf-8", $content);
    echo $content;
?>