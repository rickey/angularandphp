<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
    $sql = "SELECT a.id id,a.name text
        FROM  group_account ga 
        left join account a on a.id=ga.account_id 
        where 1=1 ";
    $stmt = array();
    $i = 1;
    // $stmt[$i++] = $company;   
    if (!empty($data['id'])) {
        $sql .= " and ga.group_id = ?";
        $stmt[$i++] = (int) $data['id'];
    }

    $rows = $db->exec($sql, $stmt);

    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $rs["data"][$i] = $row;
        $i++;
    }
    rest::send($rs);
}

function create($db, $data)
{
    rest::create($db, $data, 'group_account', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'group_account', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'group_account');
}
