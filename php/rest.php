<?php
include_once('orm.php');
session_start();
		

class rest
{
	public static function getReq()
	{
		// get our verb
		$request_method = strtolower($_SERVER['REQUEST_METHOD']);
		$return_obj		= new RestRequest();
		// we'll store our data here
		$data			= array();

		switch ($request_method)
		{
			// gets are easy...
			case 'get':
				$data = $_GET;
				break;
			case 'post':
			case 'put':
			case 'delete':
				$data = json_decode(file_get_contents('php://input'));
				break;
		}
		$return_obj->setData($data);
		// store the method
		$return_obj->setMethod($request_method);

		// set the raw data, so we can access it if needed (there may be
		// other pieces to your requests)
		// $return_obj->setParams($data);

		// if(isset($data['data']))
		// {
		// 	// translate the JSON to an Object for use however you want
		// 	$return_obj->setData(json_decode($data['data']));
		// }
		return $return_obj;
	}

	public static function state($status=200,$msg=""){
		$status_header = 'HTTP/1.1 ' . $status . ' ' . 
			rest::getStatusCodeMessage($status);
		// set the status
		header($status_header);
		// set the content type
		header('Content-type: text/html' );
		header('Access-Control-Allow-Origin:*'); 
		$msg = array('status' => rest::getStatusCodeMessage($status),
			'code'=>$status,
			'text'=>$msg );
		echo json_encode($msg,JSON_UNESCAPED_UNICODE);
		exit;
	}


	public static function send($body='')
	{
		$status = 200;
		$status_header = 'HTTP/1.1 ' . $status . ' ' . rest::getStatusCodeMessage($status);
		// set the status
		header($status_header);
		// set the content type
		header('Content-type: application/json;charset=utf-8' );

		// pages with body are easy
		if($body != '')
		{
			// send the body
			echo json_encode($body, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
			//echo json_encode($url, JSON_UNESCAPED_SLASHES), "\n";
			exit;
		}
		
	}

	public static function sendXml($body='')
	{
		$status = 200;
		$status_header = 'HTTP/1.1 ' . $status . ' ' . rest::getStatusCodeMessage($status);
		// set the status
		header($status_header);
		// set the content type
		header('Content-type: application/xml;charset=utf-8' );

		// pages with body are easy
		if($body != '')
		{
			// send the body
			echo $body;
			exit;
		}
		
	}

	public static function chk($data){
		return $data?"'".$data."'":'null';
	}

	public static function getStatusCodeMessage($status)
	{
		// these could be stored in a .ini file and loaded
		// via parse_ini_file()... however, this will suffice
		// for an example
		$codes = Array(
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorized',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported'
		);

		return (isset($codes[$status])) ? $codes[$status] : '';
	}

	public static function copyFrom($target,$from){
		
		$props = get_object_vars($from);
		// echo print_r($from);
		// echo print_r($props);
		foreach ($target->fields() as $key) {
			if(property_exists($from,$key)){
				// if($target->fields[$key]['type']=='bit(1)'){
				// 	$target[$key] = true ;
				// }	
				// else 
				$props[$key] = trim($props[$key]);
				if($props[$key]=="")$target[$key]=null;
				else $target[$key] = $props[$key];
			}
			
		}
		return $target;
	}

	public static function pad($num, $size) {
		$s = $num."";
		while (strlen($s) < $size) $s = "0".$s;
		return $s;
	}
	
	public static function now(){
		date_default_timezone_set("Asia/Taipei");
		return date('Y-m-d H:i:s');
	}

	public static function create($db,$data,$table,$update=true){
		foreach ($data as $d) {
			$c = new DB\SQL\Mapper($db,$table);
			$c = rest::copyFrom($c,$d);
			if($update)$c->update_time = rest::now();
			$c->save();	
		}
	}

	public static function update($db,$data,$table,$update=true){
		foreach ($data as $d) {
			$c = new DB\SQL\Mapper($db,$table);
			$c->load(array('id=?',$d->id));
			$c = rest::copyFrom($c,$d);
			if($update)$c->update_time = rest::now();
			$c->update();
		}
	}

	public static function delete($db,$data,$table){
		foreach ($data as $d) {
			$c = new DB\SQL\Mapper($db,$table);
			$c->erase(array('id=?',$d->id));
		}
	}

	public static function go($req,$db){
		$data = $req->getData();
		$company = "SUPER";//$_SESSION['company'];
		// $company_id = rest::company_id();//"SUPER";
		switch($req->getMethod()){
			case 'get':
				query($db,$data,$company);
				break;
			case 'post':
				create($db,$data,$company);
				break;
			case 'put':
				update($db,$data,$company);
				break;
			case 'delete':
				delete($db,$data,$company);
				break;
		};
	}

	public static function company(){		
		if(!isset($_SESSION['company'])||trim($_SESSION['company'])=='') {
			echo 'company invalid';
			die(); 
		}
		return ($_SESSION['company']=="SUPER"&&
			isset($_GET['company'])&&trim($_GET['company'])!='')?
			trim($_GET['company']):$_SESSION['company'];
	}

	public static function company_id(){		
		if(!isset($_SESSION['company_id'])||trim($_SESSION['company_id'])=='') {
			echo 'Companyid invalid';
			die(); 
		}
		return  (isset($_GET['company_id'])&&trim($_GET['company_id'])!='')?
			trim($_GET['company_id']):$_SESSION['company_id'];
	}

	public static function toHtmlTable($rows){
		if(count($rows)==0||empty($rows[0])){
			echo '查無資料';
			die();	
		} 
		echo '總共有'.count($rows).'筆資料<br/>';
		$colNames = array_keys($rows[0]);
		echo '<table border="1">';
	 	echo '<tr>';
	    foreach($colNames as $colName)
	    	echo "<th>$colName</th>";
		echo '</tr>';
		foreach($rows as $row)
		{
			echo "<tr>";
			foreach($colNames as $colName)
				echo "<td>".$row[$colName]."</td>";
			echo "</tr>";
		}
	    echo '</table>';
		die();
	}
}

class RestRequest
{
	private $params;
	private $data;
	private $http_accept;
	private $method;

	public function __construct()
	{
		$this->request_vars		= array();
		$this->data				= '';
		$this->http_accept		= (strpos($_SERVER['HTTP_ACCEPT'], 'json')) ? 'json' : 'xml';
		$this->method			= 'get';
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function setMethod($method)
	{
		$this->method = $method;
	}

	public function setParams($params)
	{
		$this->params = $params;
	}

	public function getData()
	{
		return $this->data;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function getHttpAccept()
	{
		return $this->http_accept;
	}

	public function getParams()
	{
		return $this->params;
	}
}

?>
