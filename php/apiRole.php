<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
	$perpage = 10;
	$page = 1;
	if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
	}
	
	$sql = "SELECT r.*	FROM role r	        
		where  r.company_id=?";    
	$whereSql = "";
	$whereStmt = array();     
    $i = 1;
    $whereStmt[$i++] = $_SESSION['company_id'];
    if (!empty($data['searchTitle']) && $data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
        $whereSql .= " and r.name like ?";
        $whereStmt[$i++] = '%' . $data['searchTitle'] . '%';
    }

    if (!empty($data['id'])) {
        $whereSql .= " and r.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
    }
		
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 
    $rows = $db->exec($orderSql, $orderStmt);    

    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $accounts = getAccounts($db, $row, $company);
        if (count($accounts) > 0) {
            $row['account_name'] = $accounts['account_name'];
		}
		$functions = getFunctions($db, $row, $company);
        if (count($functions) > 0) {
            $row['function_name'] = $functions['function_name'];
        }
        $rs["data"][$i] = $row;
        $i++;
	}
	
	$count_sql = "SELECT COUNT(*)  as total FROM role r         
		where  r.company_id=?"; 
	$count_sql .= $whereSql;	
	// echo $count_sql;
	// echo 'whereStmt';
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
	}
	// echo print_r($rs);
    rest::send($rs);
}

function getAccounts($db, $row, $company)
{
    $accounts = array("account_name" => array());
    $sql = "SELECT a.name account_name
	FROM  role_account ra
	left join account a on a.id=ra.account_id
	where ra.role_id=?";
    $stmt = array();
    $j = 1;
    $stmt[$j++] = $row['id'];
    // $stmt[$j++]=$company;
    $rs = $db->exec($sql, $stmt);
    $accounts = array();
    for ($i = 0; $i < count($rs); $i++) {
        $accounts['account_name'][$i] = $rs[$i]['account_name'];
    }
    return $accounts;
}

function getFunctions($db, $row, $company)
{
    $functions = array("function_name" => array());
    $sql = "SELECT f.name function_name
	FROM  role_function rf 
	left join funclist f on f.id=rf.function_id 
	where rf.role_id=?";
    $stmt = array();
    $j = 1;
    $stmt[$j++] = $row['id'];
    // $stmt[$j++]=$company;
    $rs = $db->exec($sql, $stmt);
    $functions = array();    
    for ($i = 0; $i < count($rs); $i++) {        
        $functions['function_name'][$i] = $rs[$i]['function_name'];
    }
    return $functions;
}

function create($db, $data)
{
    $sql = "select max(id)+1 id from role";
    $rows = $db->exec($sql);
    foreach ($rows as $row) {
        echo 'id====' . $row['id'];
		$id = $row['id'];
		
		//新增角色對帳號資料對應表
        if (!empty($data[0]->accounts)) {
            foreach ($data[0]->accounts as $account) {
                $count = 0;
                $newid = 0;
                foreach ($account as $key => $value) {
                    $count++;
                    if ($count == 1 && $key == "id") {
                        $a['id'] = $value;
                        $newid = $value;
                    }
                }
                // echo 'add content_tag.new_id=' . $newid . "content_id=" . $id;
                $role_account['role_id'] = (int) $id;
                $role_account['account_id'] = (int) $newid;
                echo $role_account_json = json_encode(array($role_account));
                echo 'add role_account table' . print_r($role_account_arr = json_decode($role_account_json));
                rest::create($db, $role_account_arr, 'role_account', false);
            }
		}
		
		//新增角色對功能資料對應表
        if (!empty($data[0]->functionlist)) {
            foreach ($data[0]->functionlist as $function) {
                $count = 0;
                $newid = 0;
                foreach ($function as $key => $value) {
                    $count++;
                    if ($count == 1 && $key == "id") {                        
                        $newid = $value;
                    }
                }
                // echo 'add content_tag.new_id=' . $newid . "content_id=" . $id;
                $role_function['role_id'] = (int) $id;
                $role_function['function_id'] = (int) $newid;
                $role_function_json = json_encode(array($role_function));
                $role_function_arr = json_decode($role_function_json);
                rest::create($db, $role_function_arr, 'role_function', false);                
            }
        }
    }
    $data[0]->id = $id;
    echo 'add role table';
    rest::create($db, $data, 'role', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'role', false);

	//echo print_r($data);
	//先刪除再新增role_account
	//先刪除content_id中content_id，重新新增一次
	$delSql = "delete from role_account where role_id=?";
	$delStmt = array();
	$i = 1;
	$delStmt[$i++] = $data[0]->id;
	$db->exec($delSql, $delStmt);
    if (!empty($data[0]->accounts)) {        
        $role_id = $data[0]->id;
        foreach ($data[0]->accounts as $account) {
            $count = 0;
            $newid = 0;
            foreach ($account as $key => $value) {
                $count++;
                if ($count == 1 && $key == "id") {
                    $newid = $value;
                }
            }
            $role_account['role_id'] = (int) $role_id;
            $role_account['account_id'] = (int) $newid;
            $role_account_json = json_encode(array($role_account));
            $role_account_arr = json_decode($role_account_json);
            rest::create($db, $role_account_arr, 'role_account', false);
            echo $count;
        }
	}

	//先刪除再新增role_function
	 //先刪除content_id中content_id，重新新增一次
	 $delSql = "delete from role_function where role_id=?";
	 $delStmt = array();
	 $i = 1;
	 $delStmt[$i++] = $data[0]->id;
	 $db->exec($delSql, $delStmt);
    if (!empty($data[0]->functionlists)) {       
        $role_id = $data[0]->id;
        foreach ($data[0]->functionlists as $function) {
            $count = 0;
            $newid = 0;
            foreach ($function as $key => $value) {
                $count++;
                if ($count == 1 && $key == "id") {
                    $newid = $value;
                }
            }
            $role_function['role_id'] = (int) $role_id;
            $role_function['function_id'] = (int) $newid;
            $role_function_json = json_encode(array($role_function));
            $role_function_arr = json_decode($role_function_json);
            rest::create($db, $role_function_arr, 'role_function', false);
        }
	}	

}

function delete($db, $data)
{
    $delSql = "delete from role_account where role_id=?";
	$delStmt = array();
	$i = 1;
	$delStmt[$i++] = $data[0]->id;
    $db->exec($delSql, $delStmt);
    
    $delSql = "delete from role_function where role_id=?";
    $delStmt = array();
    $i = 1;
    $delStmt[$i++] = $data[0]->id;
    $db->exec($delSql, $delStmt);

    rest::delete($db, $data, 'role');
}
?>