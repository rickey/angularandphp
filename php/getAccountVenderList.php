<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){
	if(!empty($_SESSION['vender_list']) && $_SESSION['vender_list'] !="undefined") {
		// echo 'get session vender_list'.print_r($_SESSION['vender_list']);
		rest::send($_SESSION['vender_list']);
	} else {
		$stmt = array();$i=1;
		$sql="";
		if ($_SESSION['admin'] || $_SESSION['super_user']) {
			$sql=" select  distinct v.name,v.id
			from  vender v
			where  v.id is not null	";			
		} else {
			$sql=" select  distinct v.name,v.id
			,ga.account_id
					from group_account ga
					left join group_vender gv on ga.group_id=gv.group_id
					left join vender v on gv.vender_id=v.id
			where  v.id is not null ";
			if (!empty($data['account_id'])) {
				$sql .= " and ga.account_id = ?";
				$stmt[$i++] = (int) $data['account_id'];
			}
		}		
		// echo print_r($sql).','.print_r($stmt);
		$rows = $db->exec($sql,$stmt);
		$rs = array("data" => array());
		$i = 0;
		foreach ($rows as $row) {
			$rs["data"][$i] = $row;
			$i++;
		}	
		// echo print_r($rs);
		rest::send($rs);
	}
	
}

?>