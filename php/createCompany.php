<?php
/**
 * POST METHOD
 * 
 * @version 1.0
 * @author rickeysu@cht.com.tw
 * @date 03/27/19
 * @since 1.0 修改取資料表取最大值，由sql改用框架去取新增完後的id值
 */
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

/**
 * @API功能：新增或更新至公司資料表(含api_user)及account資料表
 * @Param JSON 新增或更新欄位company/account
 * {
 *     "company_name" => "中華",
 *     "company_code" => "CHT01",
 *     "api_user" => "C方案帳號2(tenant)",
 *     "api_pass" => " C方案密碼v2(tenant)",
 *     "optional" => "其他"
 * }
 * @RETURN　JSON 回傳成功或失敗訊息
 *     400 =>bad request
 *     404 =>no company data
 *     200 =>ok
 */
function create($db, $data)
{	
	// print_r($data);
    if (json_encode($data) == "null") {
        $msg = array('status' => "Fail",
            'code' => "404",
            'text' => "Format Error");
		echo json_encode($msg, JSON_UNESCAPED_UNICODE);	
		return "";	
    } else {        
        // $sql = "select  AUTO_INCREMENT as id from information_schema.tables where table_name='company' AND table_schema = 'ott2b';";
        // $rows = $db->exec($sql);
        $company_id = "";
        // foreach ($rows as $row) {
        //     $id = $row['id'];
        // }

        //step1：檢查company的代碼存不存在
        $sql = " select * from company where 1=1 ";
        $whereStmt = array();
        $i = 1;
        if (!empty($data[0]->company_code) && $data[0]->company_code != 'undefined') {
            $sql .= " and company = ?";
            $whereStmt[$i++] = $data[0]->company_code;
        } else {			
            $msg = array('status' => "Fail",
                'code' => "400",
                'text' => "Bad Request");
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);			
			return "";
        }
        //echo $sql.'wherStmt'.print_r($whereStmt);
        $rows = $db->exec($sql, $whereStmt);
        // echo 'company exists====='.count($rows);
        
        //step2：新增或更新company
        if (count($rows) > 0) {
            //檢查company的代碼存在，則更新
            //echo 'company_id='.$rows[0]['id'];  
            $company['id'] = $rows[0]['id'];
            //替換company的id
            $company_id = $rows[0]['id'];
            $company['company'] = $data[0]->company_code;
			$company['company_name'] = $data[0]->company_name;
			$company['ftp_user'] = $data[0]->api_user;
			$company['ftp_pass'] = $data[0]->api_pass;
            $company_json = json_encode(array($company));
            $company_arr = json_decode($company_json);
            rest::update($db, $company_arr, 'company', false);
        } else {
            //檢查company的代碼不存在，則新增
            $db->begin();
            foreach ($data as $d) {
                $c = new DB\SQL\Mapper($db,'company');        
                $c = rest::copyFrom($c,$d);     
                $c->id = 0;
                $c->company = $data[0]->company_code;
                $c->company_name =  $data[0]->company_name;
                $c->ftp_user = $data[0]->api_user;;
                $c->ftp_pass = $data[0]->api_pass;
                $c->save();	       
                //echo 'content last id===='.$c->id;       
                $company_id = $c->id; 
                $db->commit();
            }
            // $company['company'] = $data[0]->company_code;
			// $company['company_name'] = $data[0]->company_name;
			// $company['ftp_user'] = $data[0]->api_user;
			// $company['ftp_pass'] = $data[0]->api_pass;
            // $company_json = json_encode(array($company));
            // $company_arr = json_decode($company_json);
            // rest::create($db, $company_arr, 'company', false);
        }

        //step3：新增或更新account
        if (!empty($data[0]->api_user) && !empty($data[0]->api_pass)) {
            //2-1先檢查account中的company_iD+ACCOUNT是否存在
            $sql = " select * from account where 1=1 ";
            $whereStmt = array();
            $i = 1;
            if (!empty($data[0]->company_code) && $data[0]->company_code != 'undefined') {
                $sql .= " and company_id = ? and name = ? ";
                $whereStmt[$i++] = (int) $company_id;
                $whereStmt[$i++] = $data[0]->api_user;
            }
            $rows = $db->exec($sql, $whereStmt);
			// echo 'account exists====='.count($rows);
			// echo "company_id=".$id;
            //2-2檢查 account 的代碼存在，則更新
            if (count($rows) > 0) {
				$account['id'] = (int) $rows[0]['id'];
                $account['company_id'] = (int) $company_id;
                $account['pass'] = $data[0]->api_pass;
                $account['admin'] = 1;
                $account_json = json_encode(array($account));
                $account_arr = json_decode($account_json);
                rest::update($db, $account_arr, 'account', false);
                $msg = array('status' => "OK",
                    'code' => "200",
                    'text' => "Update Success");
            } else {
                //2-3檢查 account 的代碼不存在，則新增
                $account['company_id'] = (int) $company_id;
                $account['name'] = $data[0]->api_user;
                $account['pass'] = $data[0]->api_pass;
                $account['admin'] = 1;
                $account_json = json_encode(array($account));
                $account_arr = json_decode($account_json);
                rest::create($db, $account_arr, 'account', false);
                $msg = array('status' => "OK",
                    'code' => "200",
                    'text' => "Insert Success");
            }
			echo json_encode($msg, JSON_UNESCAPED_UNICODE);	
			return "";		
        }
    }

}
