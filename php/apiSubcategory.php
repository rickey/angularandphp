<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
    $perpage = 10;
    $page = 1;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
    }

    $sql = "SELECT s.*,c.name as category_name
			FROM subcategory s
			left join category c on c.id = s.category_id             
			where 1=1 and s.company_id=?";
    $whereSql = "";
    $whereStmt = array();
    $i = 1;
    $whereStmt[$i++] = $_SESSION['company_id'];    

    if (!empty($data['key'])  && $data['key'] != 'undefined' && isset($data['key']) && trim($data['key']) != '') {        
        $whereSql .= " and (s.name like ?)";
        $whereStmt[$i++] = '%' . $data['key'] . '%';
    }
    if (!empty($data['id'])) {
        $whereSql .= " and (s.id = ?)";
        $whereStmt[$i++] = $data['id'];
    }
    if (!empty($data['category_id']) && $data['category_id'] !='undefined' && isset($data['category_id']) && trim($data['category_id']) != '') {
        $whereSql .= " and (s.category_id = ?)";
        $whereStmt[$i++] = (int)$data['category_id'];
    }
    
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 
    $rows = $db->exec($orderSql, $orderStmt);

    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $rs["data"][$i] = $row;
        $i++;
    }

    $count_sql = "SELECT COUNT(*)  as total FROM subcategory s         
		where s.company_id=?";
	$count_sql .= $whereSql;	
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
    }

    rest::send($rs);
}

function create($db, $data)
{
    foreach ($data as $d) {
        $c = new DB\SQL\Mapper($db, 'subcategory');
        $c = rest::copyFrom($c, $d);
        $c->save();
    }
}

function update($db, $data)
{
    foreach ($data as $d) {
        $c = new DB\SQL\Mapper($db, 'subcategory');
        $c->load(array('id=?', $d->id));
        $c = rest::copyFrom($c, $d);
        $c->save();
    }
}

function delete($db, $data)
{
    rest::delete($db, $data, 'subcategory');
}
