<?php
set_time_limit(10);
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
    $loginFtpSuccess = true;
    $ftp_user = "";
    $ftp_pass = "";
    $ftp_ip = "";
    $ftp_port = "";
    $conn_id = "";

    //取ftp的ip跟port
    $checkFtp = new ftpData;
    $ftpIpAndPort = $checkFtp->getFtpIpAndPort($db);    
    $ftp_ip = $ftpIpAndPort[0];
    $ftp_port = $ftpIpAndPort[1];
    //echo "ftp_id".$ftpIpAndPort[0];
    //echo "ftp_port".$ftpIpAndPort[1];

    //依vender的id查出ftp登入帳號/密碼
    $ftpAccountAndPwd = $checkFtp->getFtpAccountAndPwdByVenderId($data["vender_id"], $db);
    $ftp_user = $ftpAccountAndPwd[0];
    $ftp_pass = $ftpAccountAndPwd[1];
    //echo "ftp_account".$ftpAccountAndPwd[0];
    //echo "ftp_pwd".$ftpAccountAndPwd[1];

    //驗証ftp的ip/port/account/pwd是否可以登入
    $isSuccess = checkFtp($ftp_ip, $ftp_port, $ftp_user, $ftp_pass);
    $response = array();
    $response['isSuccess'] = $isSuccess;
    rest::send($response);
}

//驗証ftp的ip/port/account/pwd是否可以登入
function checkFtp($ftp_ip, $ftp_port, $ftp_user, $ftp_pass) {
    $isSuccess = true;
    try {
        if (!$socket = @fsockopen($ftp_ip, $ftp_port, $errno, $errstr, 5)) { 
            // echo "Offline!"."\n";    
             $isSuccess = false;        
         } else {               
            // echo "Online!\n";                                                  
             $conn_id = ftp_connect($ftp_ip, $ftp_port); //server IP
             if (false === $conn_id) {
                 $isSuccess = false;
                 throw new Exception('Unable to connect');                
             }
             $object = ftp_raw($conn_id,"USER ".$ftp_user);
             $object = ftp_raw($conn_id,"PASS ".$ftp_pass);
             //print_r ($object[0]);
             //print_r (substr(trim($object[0]), 0, 1) == 2);
     
             if(substr(trim($object[0]), 0, 1) != 2) {
                 //echo "fail";   
                 $isSuccess = false;            
                 //exit(1);
             }
             fclose($socket);                     
         }        
    } catch (Exception $e) {
        $isSuccess = false;
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
    

   return $isSuccess;    
}
?>