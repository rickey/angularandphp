<?php
include_once('rest.php');
require_once('./util/getFunclist.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){
	// echo 'req Data===='.print_r($data);
	$stmt = array();$i=1;
	$sql="";
	if (!empty($_SESSION['admin'])) {
		$sql=" select  distinct f.id,f.name,f.url,f.seq,f.parent_id,f.level
		from  funclist f 
		where  f.id is not null	and f.url<>'company' ";		
	} else if (!empty($_SESSION['super_user'])) { //super user才能設定公司功能
		$sql=" select  distinct f.id,f.name,f.url,f.seq,f.parent_id,f.level
		from  funclist f 
		where  f.id is not null	";		
	} else {
		$sql=" select  distinct f.id,f.name,f.url,f.seq,f.parent_id,f.level
		from role_account ra
		left join role_function rf on ra.role_id=rf.role_id
		left join funclist f on rf.function_id=f.id	
		where  f.id is not null	";
		if (!empty($data['account_id'])) {
			$sql .= " and ra.account_id = ?";
			$stmt[$i++] = (int) $data['account_id'];
		}
	}	
	$sql .= " order by  f.level,f.parent_id,f.seq; ";

	// echo print_r($sql).','.print_r($stmt);
	$rows = $db->exec($sql,$stmt);
	$rs = array("data" => array());
    $i = 0;
	foreach ($rows as $row) {
		$row['children'] = array();
        $rs["data"][$i] = $row;
        $i++;
	}	

	// $sql2 = "";
	
	// echo  empty(!$_SESSION['super_user'];
	// echo empty(!$_SESSION['admin'];
	//用函數取上層parent_id
	// foreach ($rows as $row) {
	// 	if ($row['parent_id'] !== "0") {
	// 		$sql2 = " select * From funclist where FIND_IN_SET(id, getParentList(?)) and id<>? ; ";
	// 		$stmt2 = array();$j=1;
	// 		$stmt2[$j++] = (int) $row['id'];
	// 		$stmt2[$j++] = (int) $row['id'];
	// 		$rows2 = $db->exec($sql2,$stmt2);			
	// 		foreach ($rows2 as $row2) {
	// 			$rs["data"][$i] = $row2;
	// 			$i++;
	// 		}	
	// 	}			
	// }		
	 
 
	if (empty($_SESSION['admin']) && empty($_SESSION['super_user'])) {
		$fun = new Func;	
		$funAll = $fun->getAll($db);
		$result = array();
		foreach ($rows as $row) {
			$var = $fun->getParentTree($funAll, $row['id']);	
			foreach($var as $value) {
				$rs["data"][$i] = $value;				
				$i++;
			}
		}	
	}
	//$var = $fun->getParentTree($funAll, 3);
	//echo "new====".print_r($var);
	// foreach($var as $value) {
	// 	echo "new2====".print_r($value)."<BR>"; 
	// }

	// if (empty($_SESSION['admin']) && empty($_SESSION['super_user'])) {
	// 	$rs = $db->exec("select * From funclist");
		// foreach ($funAll as $allFunc) {
		//	print_r($funAll);
		// }
	// }

	rest::send($rs);
}

?>