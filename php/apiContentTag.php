<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
    $sql = "SELECT  t.id,t.text
			from content c, content_tag  ct
            left join tag t on t.id=ct.tag_id
			where ct.content_id = c.id and ct.company=?";
    $stmt = array();
    $i = 1;
    $stmt[$i++] = $company;
   
    if (!empty($data['id'])) {
        $sql .= " and c.id = ?";
        $stmt[$i++] = (int) $data['id'];
    }
    // $sql .= " limit ?";
    // $stmt[$i++] = (isset($data['rows']) && trim($data['rows'])!='')?(int)$data['rows']:10;
    $rows = $db->exec($sql, $stmt);

    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $rs["data"][$i] = $row;
        $i++;
    }
    rest::send($rs);
}

function create($db, $data)
{
    rest::create($db, $data, 'content_tag', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'content_tag', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'content_tag');
}
