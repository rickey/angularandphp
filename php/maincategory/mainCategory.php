<?php
require_once '../db.php';
class MainCategory
{
    private $conn;
    public function __construct()
    {
        // Create connection
        $conn = new mysqli(servername, username, password, dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } else {
            $this->conn = $conn;
        }

    }

    public function category_list($page = 1, $compCatalog = '')
    {        
        $perpage = 10;
        $page = ($page - 1) * $perpage;
        $search = '';
        if ($compCatalog != '') {
            $search = "WHERE ( name LIKE '%$compCatalog%' OR company like '%$compCatalog%' )";
        }
        $sql = "SELECT * FROM category $search ORDER BY id desc LIMIT $page,$perpage";

        $query = $this->conn->query($sql);
        $category = array();
        if ($query->num_rows > 0) {
            while ($row = $query->fetch_assoc()) {
                $category['category_data'][] = $row;
            }
        }

        $count_sql = "SELECT COUNT(*) FROM category";
        $query = $this->conn->query($count_sql);
        $total = mysqli_fetch_row($query);
        $category['total'][] = $total;

        return $category;
    }

    public function view_category_by_id($id)
    {
        if (isset($id)) {
            $id = mysqli_real_escape_string($this->conn, trim($id));
            $sql = "Select * from category where id='$id'";
            $result = $this->conn->query($sql);
            return $result->fetch_assoc();
        }
    }

    public function update_category_info($post_data = array())
    {
        if (isset($post_data->id)) {
            $id = mysqli_real_escape_string($this->conn, trim($post_data->id));

            $name = '';
            if (isset($post_data->name)) {
                $name = mysqli_real_escape_string($this->conn, trim($post_data->name));
            }
            $company = '';
            if (isset($post_data->company)) {
                $company = mysqli_real_escape_string($this->conn, trim($post_data->company));
            }

            $sql = "UPDATE category SET name='$name',company='$company' WHERE id =$id";

            $result = $this->conn->query($sql);

            unset($post_data->id);
            if ($result) {
                return 'Succefully Updated Category Info';
            } else {
                return 'An error occurred while inserting data';
            }

        }
    }

    public function delete_category_info_by_id($id)
    {

        if (isset($id)) {
            $student_id = mysqli_real_escape_string($this->conn, trim($id));

            $sql = "DELETE FROM  category  WHERE id =$id";
            $result = $this->conn->query($sql);

            if ($result) {
                return 'Successfully Deleted Student Info';
            } else {
                return 'An error occurred while inserting data';
            }

        }

    }

    public function create_category_info($post_data = array())
    {

        $name = '';
        if (isset($post_data->name)) {
            $name = mysqli_real_escape_string($this->conn, trim($post_data->name));
        }
        $company = '';
        if (isset($post_data->company)) {
            $company = mysqli_real_escape_string($this->conn, trim($post_data->company));
        }

        $sql = "INSERT INTO category (name, company) VALUES ('$name', '$company')";

        $result = $this->conn->query($sql);

        if ($result) {
            return 'Succefully Created Student Info';
        } else {
            return 'An error occurred while inserting data';
        }

    }
}
