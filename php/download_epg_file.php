<?php 
    header('Content-Description: File Transfer');
    header("Content-type: text/csv; charset=big5");
    header("Content-disposition: attachment; filename=upload_epg_content.csv");
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    // 這裏要注意是\r\n是Windows的換行符號，\n為Unix的換行符號
    $content = "匯入Content資料，請依下列步驟進行修改:" . "\r\n";
    $content .= "1.選擇要匯入的欄位並在第一行輸入欄位名稱(用逗號隔開)，欄位對照如下：" . "\r\n";
    $content .= "----必填----" . "\r\n";
    $content .= "   所屬頻道名稱(channel_name) : 至本系統查詢" . "\r\n";
    $content .= "   標題(title) "  . "\r\n";    
    $content .= "----選填----" . "\r\n";
    $content .= "   開始時間(start_time) : ex:2019-03-01 00:00:00" . "\r\n";
    $content .= "   結束時間(end_time) : : ex:2019-12-31 23:59:59" . "\r\n";    
    $content .= "2.第二行開始輸入要匯入資料" . "\r\n";
    $content .= "3.若要另外建立新檔案，請於儲存時將編碼選為UTF-8" . "\r\n";
    $content .= "以下為簡單範例" . "\r\n";
    $content .= "----------------------以上請刪除-----------------------------" . "\r\n";
    $content .= "channel_name,title,start_time,end_time" . "\r\n";
    $content .= "CHT_CHANNEL,epg_title,2019-03-01 00:00:00,2015-03-31 23:59:59" . "\r\n";    

    // 將字串編碼由utf-8轉為big5(非必要)
    $content = iconv("utf-8", "big5", $content);
    echo $content;
?>