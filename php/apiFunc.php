<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){
	$perpage = 10;
	$page = 1;
	if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
	}

	$sql = "SELECT * 
			FROM funclist f			
			where 1=1  ";
	$whereSql = "";
	$whereStmt = array();  
	$i=1;	
	if (!empty($data['searchTitle']) && $data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
        $whereSql .= " and f.name like ?";
        $whereStmt[$i++] = '%' . $data['searchTitle'] . '%';
    }

    if (!empty($data['id'])) {
        $whereSql .= " and f.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
    }
	
	$orderSql = $sql;
	$orderSql .= $whereSql;
	$orderSql .= " order by f.level,f.parent_id,f.seq ";
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 

	// echo print_r($orderSql).','.print_r($orderStmt);
    $rows = $db->exec($orderSql, $orderStmt); 

	$rs = array("data" =>array());
	$i = 0;
	foreach($rows as $row) {
		$rs["data"][$i] = $row;
		$i++;
	}

	$count_sql = "SELECT COUNT(*)  as total FROM funclist f		
		where 1=1 ";
	$count_sql .= $whereSql;
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
	}
	rest::send($rs);
}
  
function create($db, $data)
{
    rest::create($db, $data, 'funclist', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'funclist', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'funclist');
}

?>