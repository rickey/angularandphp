<?php
include_once("file.php");
include_once("rest.php");

rest::go(rest::getReq(),orm::db());

function create($db,$post,$company){
	$csv = file::parseCSV($_FILES['csvUpload']['tmp_name']);
	$data = array("success"=>true,"cnts"=>array());
	$importIds = array();
	$i=0;
	foreach ($csv as $d) {
		// $v = new DB\SQL\Mapper($db,'vender');
		// $c->load(array('id=?',$d->id));	
		if(in_array($d['import_unique_id'],$importIds)){
			$d['repeated'] = true;
			$data['cnts'][$i++] = $d;
			continue;
		}
		array_push($importIds,$d['import_unique_id']);

		$d = checkDBExisted($db,$d,$company);
		$d = checkVenderContract($db,$d,$company);
		$d = checkRating($db,$d);
		$d = checkCategorySubcategory($db,$d,$company);
		$d['company'] = $company;
		// echo print_r($d);
		$data['cnts'][$i++] = $d;
	}
	// echo print_r($data);
	rest::send($data);
	// echo stripslashes(json_encode(array("success"=>true,'datas'=>$data),JSON_UNESCAPED_UNICODE));
}

function checkDBExisted($db,$data,$company){
	$sql = "select id from content where import_hash=? and company=?";
	$stmt=array();$i=1;
	$stmt[$i++]=$data['import_unique_id'];
	$stmt[$i++]=$company;	
	$rows=$db->exec($sql,$stmt);
	if(!empty($rows)){
		$data['id'] = $rows[0]['id'];
	}	
	return $data;
}

function checkVenderContract($db,$data,$company){
	$sql = "select c.id as contract_id,v.id as vender_id,v.name as vender_name,c.title as contract_title 
		from vender v left join contract c on c.vender_id=v.id and c.code like ? 
		where v.name like ? and v.company=? and c.company=?";
	$stmt=array();$i=1;
	$stmt[$i++]="%".$data['contract_code']."%";
	$stmt[$i++]="%".$data['vender_name']."%";
	$stmt[$i++]=$company;
	$stmt[$i++]=$company;
	// echo print_r($d);
	$rows=$db->exec($sql,$stmt);
	
	if(!empty($rows)){
		$data['vender_id'] = $rows[0]['vender_id'];
		$data['vender_name'] = $rows[0]['vender_name'];
		$data['contract_id'] = $rows[0]['contract_id'];
		$data['contract_title'] = $rows[0]['contract_title'];
	}	
	return $data;
}

function checkCategorySubcategory($db,$data,$company){
	$sql = "select c.name as category_id,s.name as subcategory 
		from category c 
		left join subcategory s on c.id=s.category_id and s.name = ?  
		where c.name =? and c.company=? and s.company=?";
	$stmt=array();$i=1;
	$stmt[$i++]=$data['subcategory'];
	$stmt[$i++]=$data['category'];
	$stmt[$i++]=$company;
	$stmt[$i++]=$company;
	$rows=$db->exec($sql,$stmt);
	if(empty($rows)){
		$data['category'] = null;
		$data['subcategory'] = null;
	}else if(empty($rows[0]['subcategory'])) $data['subcategory'] = null;	
	return $data;	
}

function checkRating($db,$data){
	$sql = sprintf("select r.id as rating_id from rating r where r.code='%s'",
		$data['rating_code']);
	$rows=$db->exec($sql);
	if(!empty($rows)){
		$data['rating_id'] = $rows[0]['rating_id'];
	}
	return $data;
}
?>