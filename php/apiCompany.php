<?php
include_once('rest.php');
#rest::go(rest::getReq(),orm::db());
rest::go(rest::getReq(),conn::db());

function query($db,$data,$company){
	$perpage = 10;
	$page = 1;
	if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
	}

	$sql = " SELECT distinct c.*  FROM  company c 
		left join account a on a.company_id=c.id and a.admin=1 
		where 1=1";
	$whereSql = "";
	$whereStmt = array();  
	$i=1;
	// $whereStmt[$i++] = $company;
	if ($data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
        $whereSql .= " and c.name like ?";
        $whereStmt[$i++] = '%' . $data['searchTitle'] . '%';
    }

    if (!empty($data['id'])) {
        $whereSql .= " and c.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
    }
	
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 

	// echo print_r($orderSql).','.print_r($orderStmt);
    $rows = $db->exec($orderSql, $orderStmt); 

	$rs = array("data" =>array());
	$i = 0;
	foreach($rows as $row) {
		$rs["data"][$i] = $row;
		$i++;
	}

	$count_sql = "SELECT COUNT(*)  as total FROM  company c	where 1=1";
	$count_sql .= $whereSql;
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
	}
	rest::send($rs);
}

function create($db, $data)
{
	$db->begin();
	foreach ($data as $d) {
		$c = new DB\SQL\Mapper($db,'company');        
		$c = rest::copyFrom($c,$d);     
		$c->id = 0;
        $c->save();	       
        echo 'content last id===='.$c->id;       
        $company_id = $c->id; 
		$db->commit();
		if (!empty($data[0]-> name) && !empty($data[0]-> pass)) {
			$account['company_id'] = (int) $company_id;
			$account['name'] = $data[0]-> name;
			$account['pass'] = $data[0]-> pass;
			$account['admin'] = 1;
			$account_json = json_encode(array($account));
			$account_arr = json_decode($account_json,true);
			rest::create($db, $account_arr, 'account', false);	
		}	
	}
}


function update($db, $data)
{
	rest::update($db, $data, 'company', false);
	rest::update($db, $data, 'account', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'company');
}

?>