<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){	
	$perpage = 10;
    $page = 1;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
    }
	$search = '';
		
	$sql = "SELECT c.id,c.name 
			FROM category c			
			where 1=1 and c.company_id=?";
	$whereSql = "";
	$whereStmt = array(); 
	$i=1;
	$whereStmt[$i++]=$_SESSION['company_id'];
	if(!empty($data['key']) && $data['key'] != 'undefined' && isset($data['key']) && trim($data['key'])!='') {
		$str = '%'.$data['key'].'%';
		$whereSql .= " and (c.name like ?)";
		$whereStmt[$i++]=$str;
	}
	if (!empty($data['id'])) {
        $whereSql .= " and (c.id = ?)";
        $whereStmt[$i++] = $data['id'];
    }

	// $sql .= $whereSql;	
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 

	// echo print_r($orderSql).','.print_r($orderStmt);
    $rows = $db->exec($orderSql, $orderStmt);

	$rs = array("data" =>array());
	$i = 0;	
	foreach($rows as $row) {
			$rs["data"][$i] = $row;
			$i++;
	}

		
	// if($data['key'] != 'undefined' && isset($data['key']) && trim($data['key'])!='') {
	// 	$str = '%'.$data['key'].'%';
	// 	$count_condSql .= " and (c.name like ?)";
	// 	$count_stmt[$i++]=$str;
	// }
	$count_sql = "SELECT COUNT(*)  as total FROM category c 		
		where 1=1 and c.company_id=?";
	$count_sql .= $whereSql;	
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
    }
		
	rest::send($rs);
}

function create($db,$data){	
	rest::create($db,$data,'category',false);
}

function update($db,$data){
	rest::update($db,$data,'category',false);
}

function delete($db,$data){
	rest::delete($db,$data,'category');
}

?>