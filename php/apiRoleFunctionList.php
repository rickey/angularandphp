<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
    $sql = "SELECT f.id id,f.name text
        FROM  role_function rf 
        left join funclist f on f.id=rf.function_id 
        where 1=1 ";
    $stmt = array();
    $i = 1;
    // $stmt[$i++] = $company;   
    if (!empty($data['id'])) {
        $sql .= " and rf.role_id = ?";
        $stmt[$i++] = (int) $data['id'];
    }

    $rows = $db->exec($sql, $stmt);

    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $rs["data"][$i] = $row;
        $i++;
    }
    rest::send($rs);
}

function create($db, $data)
{
    rest::create($db, $data, 'content_tag', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'content_tag', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'content_tag');
}
