<?php

class file {
	public static function parseCSV_old($importedFile){
		if($importedFile){
			$x=0;
			$data=array();
			$file=fopen($importedFile, "r");
			$column=trim(fgets($file));
			$column=preg_replace("/\xef\xbb\xbf/", '', $column);//file contains 小紅點
			$columns=explode(",",$column);
			$num_columns=count($columns);
			while (!feof($file)) {
				$row=trim(fgets($file));
				if(strlen($row)>3){
					$row=preg_replace("/\xef\xbb\xbf/", '', $row);
					$fields=explode(",",$row);
					$data[$x]=array();
					for($i=0; $i<$num_columns;$i++)
						$data[$x][trim($columns[$i])]=(isset($fields[$i])?(preg_replace('/"|\'/', '', trim($fields[$i]))):null);
					$x++;
		        }
		    }
			fclose($file);
			return $data;
		}
		else
			return null;
			
	}

	public static function parseCSV($importedFile){
		if(!$importedFile) return null;
		$input = file::get2DArrayFromCsv($importedFile,',');
		$header = file::format($input[0]);
		$data = array();$index=0;
		for ($i=1; $i < count($input); $i++) { 
			$row =array();
			for ($j=0; $j < count($header); $j++) { 
				$row[$header[$j]] = (isset($input[$i][$j])?(preg_replace('/"|\'/', '', trim($input[$i][$j]))):null);
			}
			$data[$index++]=$row;
		}
		// echo print_r($data);
		return $data;
	}
	private static function format($array){
		for ($i=0; $i < count($array); $i++) { 
			$array[$i]=trim($array[$i]);
			$array[$i]=preg_replace("/\xef\xbb\xbf/", '', $array[$i]);//file contains 小紅點	
		}
		return $array;
	}
	private static function get2DArrayFromCsv($file,$delimiter) { 
        if (($handle = fopen($file, "r")) !== FALSE) { 
            $i = 0; 
            while (($lineArray = fgetcsv($handle, 10000, $delimiter)) !== FALSE) { 
                for ($j=0; $j<count($lineArray); $j++) { 
                    $data2DArray[$i][$j] = $lineArray[$j]; 
                } 
                $i++; 
            } 
            fclose($handle); 
        } 
        return $data2DArray; 
    } 

	public static function upload($field,$dstName=null){
	    if(isset($_FILES)){
	        // echo print_r($_FILES);
	        $tmp_file_name = $_FILES[$field]['tmp_name'];
	        $original_file_name = $_FILES[$field]['name'];
	        
	        // Find file extension
	        $ext = explode ('.', $original_file_name);
	        $ext = $ext [count ($ext) - 1];

	        if($dstName==null) $dstName = $original_file_name;
	        // Remove the extention from the original file name
	        $file_name = substr_replace ($dstName, '', strrpos($dstName, ".")+1);
	        $new_name = $file_name . $ext;

	        return move_uploaded_file ($tmp_file_name, $new_name);
	        // if (move_uploaded_file ($tmp_file_name, $new_name)){
	            
	        //  echo stripslashes(urldecode(json_encode(array("success"=>true,'msg'=>'OK'),JSON_UNESCAPED_UNICODE)));
	        
	     //     }else
	        //   echo stripslashes(urldecode(json_encode(array("success"=>false,'msg'=>$_FILES[$field]['error']),JSON_UNESCAPED_UNICODE)));
	    }
	}

	public static function recursive_mkdir($path, $mode = 0777) {
	    $dirs = explode('/' , $path);
	    $count = count($dirs);
	    $path = '.';
	    for ($i = 0; $i < $count; ++$i) {
	        $path .= '/' . $dirs[$i];
	        if (!is_dir($path) && !mkdir($path, $mode)) {
	            return false;
	        }
	    }
	    return true;
	}

	// dest shouldn't have a trailing slash 
	public static function zip_flatten ( $zipfile, $dest='.' ) 
	{ 
	    // echo 'try to unzip file '.$zipfile.' to '.$dest.'/<br/>';
	    $zip = new ZipArchive; 
	    // echo 'ZipArchive created';
	    $fileList=array();
	    if ( $zip->open( $zipfile ) ) 
	    { 
	        // echo '<br/> zipArchive unzipped<br/>';
	        //echo print_r($zip);
	        // echo '<br/>';
	        for ( $i=0; $i < $zip->numFiles; $i++ ) 
	        { 
	            $entry = $zip->getNameIndex($i); 
	            if ( substr( $entry, -1 ) == '/' ) continue; // skip directories 
	            
	            $fp = $zip->getStream( $entry ); 
	            $ofp = fopen( $dest.'/'.basename($entry), 'w' ); 
	            
	            if ( ! $fp ) 
	                throw new Exception('Unable to extract the file.'); 
	            
	            while ( ! feof( $fp ) ) 
	                fwrite( $ofp, fread($fp, 8192) ); 
	            
	            fclose($fp); 
	            fclose($ofp); 
	            array_push($fileList, basename($entry));
	        } 

	        $zip->close(); 
	    } 
	    else 
	        return false; 
	    
	    return $fileList; 
	} 

}
?>