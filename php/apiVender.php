<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{   
    $perpage = 10;
    $page = 1;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
    }

    $sql = "";
    $whereSql = "";
    $whereStmt = array();
// echo 'session is_super='.$_SESSION['super_user'].', is admin='.$_SESSION['admin'];
// echo 'session is_super null='.($_SESSION['super_user'] === "null").', is admin null='.(['admin'] === "null");
    if (($_SESSION['super_user'] != "null") || ($_SESSION['admin'] != "null")) { 
        $sql = "SELECT v.* 	FROM vender v  where v.company_id=?";
    } else {
        $sql = " select  v.*			
        from group_account ga
        left join group_vender gv on ga.group_id=gv.group_id
        left join vender v on gv.vender_id=v.id            
        where  v.id is not null and v.company_id=? ";  
    }   
    $i = 1;
    $whereStmt[$i++] = $_SESSION['company_id'];   

    if (!empty($data['searchTitle']) && $data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
        $whereSql .= " and v.name like ?";
        $whereStmt[$i++] = '%' . $data['searchTitle'] . '%';
    }

    if (!empty($data['id']) && $data['id'] != 'id') {
        $whereSql .= " and v.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
    }
    if (!empty($_SESSION['account_id']) && (($_SESSION['super_user'] == "null") && ($_SESSION['admin'] == "null"))) {
        $whereSql .= " and ga.account_id = ?";
        $whereStmt[$i++] = (int) $_SESSION['account_id'];
    }

    $orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 
    $rows = $db->exec($orderSql, $orderStmt);

    // echo print_r($orderSql).','.print_r($orderStmt);
    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $rs["data"][$i] = $row;
        $i++;
    }

    // $count_sql = "SELECT COUNT(*)  as total FROM vender v where 1=1 and v.company=?";
    $count_sql = "";
    if (($_SESSION['super_user'] != "null") || ($_SESSION['admin'] != "null")) { 
        $count_sql = "SELECT count(*) as total 	FROM vender v  where v.company_id=?";
    } else {
        $count_sql = "SELECT COUNT(*) as total FROM group_account ga
        left join group_vender gv on ga.group_id=gv.group_id
        left join vender v on gv.vender_id=v.id            
        where  v.id is not null and v.company_id=? ";
    }    
	$count_sql .= $whereSql;	
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
    }

    rest::send($rs);
}

function create($db, $data)
{
    rest::create($db, $data, 'vender', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'vender', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'vender');
}
