<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function create($db, $post, $company)
{
    // echo $_SERVER['DOCUMENT_ROOT'];
    // echo $_SESSION['company_id'];
    if ($_FILES['file']['error'] > 0) {
        echo "檔案上傳失敗";
    } else {        
        move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '//tmp//' . $_FILES['file']['name']);
        //echo "路徑位置：" . '<a href="file/' . $_FILES['file']['name'] . '">file/' . $_FILES['file']['name'] . '</a>';
        // echo "<br />";
        // echo "類型：" . $_FILES['file']['type'] . "<br />大小：" . $_FILES['file']['size'] . "<br />";

        $row = 1;
        $file = fopen($_SERVER['DOCUMENT_ROOT'] . '//tmp//' . $_FILES['file']['name'], "r");

        $rs = array("data" => array());
        $errChannel = "頻道錯誤";
        if (($handle = fopen($_SERVER['DOCUMENT_ROOT'] . '//tmp//' . $_FILES['file']['name'], "r")) !== false) {
            $i = 0;
            $channel_seq = 0;
            $title_seq = 1;
            $start_time_seq = 4;
            $end_time_seq = 5;
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $num = count($data);
                // echo "<p> $num fields in line $row: <br /></p>\n";
                $array = array();

                for ($c = 0; $c < $num; $c++) {
                    // echo $data[$c] . "<br />\n";
                    //check lst field
                    //$a = array("title_seq","channel_seq",'start_time','end_time');
                    if ($row === 1) {
                        switch ($data[$c]) {
                            case "channel_name":
                                $channel_seq = $c;
                                break;
                            case "title":
                                $title_seq = $c;
                                break;                           
                            case "start_time":
                                $start_time_seq = $c;
                                break;
                            case "end_time":
                                $end_time_seq = $c;
                                break;
                            default:
                                break;
                        }
                    }
                }
                if ($row !== 1) {
                    $a['id'] = $i;

                    if (!empty($data[$channel_seq]) && $data[$channel_seq] != 'undefined') {
                        $sql = sprintf("select id,title  from live where title='%s'", urlencode($data[$channel_seq]));
                        $channelData = $db->exec($sql);
                        if (count($channelData) > 0) {
                            $a['live_title'] = $channelData[0]['title'];
                            $a['live_id'] = $channelData[0]['id'];
                        } else { //上傳有值，但查不到資料，給前端訊息吧
                            $errChannel .= "第 ".($row -1) ." 筆 " . $data[$channel_seq] . " ,";
                            $a['live_title'] = "";
                            $a['live_id'] = "";
                        }
                    } else {
                        $a['live_title'] = "";
                        $a['live_id'] = "";
                    }
                    
                    $a['text'] = $data[$title_seq];     //epg.text標題              
                    $a['start_time'] = $data[$start_time_seq];
                    $a['end_time'] = $data[$end_time_seq];                     
                                   
                    $a['company_id'] = $_SESSION['company_id'];
                }

                if ($row != 1) {
                    $rs["data"][$i] = $a;
                    $i++;
                }
                $row++;

            }
            // echo 'errCategory msg='.strlen($errCategory);           
            if (strlen($errChannel) > 15) {
                $rs["errChannel"][0] = substr_replace($errChannel, "", -1) . "選項";
            } 
            //upload example    = '[{"title":"1", "vender_name":"tvbs","category":"1","subcategory":"2" ,"start_time":$date2,"end_time",$date2,"file_path":"DD"}]';            
            fclose($handle);
            //echo print_r($rs);
            rest::send($rs);
        }
    }
}
?>