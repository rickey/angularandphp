<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data){
	// echo print_r($data);
	$sql = "SELECT 
		c.id,
		IFNULL(vender.name,'') as vender,
		IFNULL(c.title,'') as title,
		IFNULL(main.name,'') as category,
		IFNULL(sub.name,'') as subcategory,
		IFNULL(c.description,'') as description,		
		IFNULL(c.thumbnail,'') as thumbnail,
		IFNULL(c.start_time,'') as start_time,
		IFNULL(c.end_time,'') as end_time,
		IFNULL(c.publish_year,'') as publish_year,
		IFNULL(c.create_time,'') as create_time,
		IFNULL(c.run_time,'') as run_time,
		IFNULL(c.update_time,'') as update_time,		
		c.job_id,rss.is_http		
		FROM content c
		left join category main on main.id = c.category_id
		left join subcategory sub on sub.id = c.subcategory_id
		left join status on status.code = c.status
		left join vender on vender.id = c.vender_id
		left join rss_config rss on rss.company_id=c.company_id
		where status in (3,4) and rss.token=?";
		$whereStmt = array();
		$i = 1;
		$whereStmt[$i++] = $data['token'];	
		$whereSql = "";		
 


	// echo $sql;
	// echo print_r($whereStmt);
	if (!empty($data['limit_rows']) && $data['limit_rows'] != 'undefined') {
		$whereStmt[$i++] = (int) $data['limit_rows'];
		$sql .= " limit ? ";
	} else {
		$sql .= " limit 1000 " ;
	}
	$whereSql .= $sql;
	$rows = $db->exec($whereSql,$whereStmt);
    
    //if(count($rows)==0) rest::state(404,'查無此服務或服務中止'); //no content			
	
	$rs = array( "title"=>"HamiVideo 企業影視服務",
		"copyright"=>"中華電信數據通信分公司版權所有 © 2008 HiNet Internet Service by Chunghwa Telecom All Rights Reserved.",
		"version"=>"1.0",
		"contents" => array());
    $i = 0;
    foreach ($rows as $row) {

			$row['is_http'] = $row['is_http'] == "HTTP" ;
			
			if ($row['is_http'] != "HTTP") {
				$row['thumbnail'] =   str_replace("http:","https:",$row['thumbnail']);										
			} else {
				$row['thumbnail'] =   str_replace("https:","http:",$row['thumbnail']);										
			}
			 

		// if($row['job_id']!=null){
		$tagSql = sprintf("select  text from  content_tag ct ,tag t 
		   where  ct.tag_id=t.id and ct.content_id=%s", (int)$row['id']);
		$tagData = $db->exec($tagSql);
		if(count($tagData)!=0) {
			//  echo print_r($tagData);			 
			 $tagArray = array();
			 $j = 0;
			 foreach ($tagData as $tag) {
				array_push($tagArray, $tag['text']);;
				 $j ++;
			 }
			 $row['tag'] = $tagArray;
		} else {
			$row['tag'] = "";
		}
		$urlSql = sprintf("select  format,device,quality,drm,url from  url 
			where  job_id=%s", (int)$row['job_id']);
		$urlData = $db->exec($urlSql);
		if(count($urlData)!=0) {
			//  echo print_r($tagData);			 
			 $Array = array();
			//  $j = 0;
			//  foreach ($urlData as $url) {
			// 	array_push($tagArray, $tag['text']);;
			// 	 $j ++;
			//  }
			$j = 0;
			$array = array();
			foreach ($urlData as $url) {			
				if ($row['is_http'] != "HTTP") {
					$url['url'] =   str_replace("http:","https:",$url['url']);																
				} else {
					$url['url'] =   str_replace("https:","http:",$url['url']);	
				}
				array_push($array, $url);
			}
			$row['urls'] = $array;
		  // $row['urls'] = $urlData;
			 
		} else {
			$row['urls'] = array();
		}
		unset($row['job_id']);
		$rs["contents"][$i] = $row;
        $i++;

	}
	rest::send($rs);
}
?>