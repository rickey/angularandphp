<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function create($db, $post, $company)
{
    setlocale(LC_ALL,'en_US.UTF-8');
    // echo $_SERVER['DOCUMENT_ROOT'];
    // echo $_SESSION['company_id'];
    if ($_FILES['file']['error'] > 0) {
        echo "檔案上傳失敗";
    } else {
        move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '//tmp//' . $_FILES['file']['name']);
        // echo "路徑位置：" . '<a href="file/' . $_FILES['file']['name'] . '">file/' . $_FILES['file']['name'] . '</a>';
        // echo "<br />";
        // echo "類型：" . $_FILES['file']['type'] . "<br />大小：" . $_FILES['file']['size'] . "<br />";

        $row = 1;
        $file = fopen($_SERVER['DOCUMENT_ROOT'] . '//tmp//' . $_FILES['file']['name'], "r");

        $rs = array("data" => array());
        $errVender = "內容商錯誤";
        $errCategory = "主分類錯誤";
        $errSubCategory = "次分類錯誤";
        // $errFileFormat = "檔案路徑錯誤";
        if (($handle = fopen($_SERVER['DOCUMENT_ROOT'] . '//tmp//' . $_FILES['file']['name'], "r")) !== false) {
            $i = 0;
            $vender_id = 0;
            $title_id = 1;
            $category_id = 2;
            $subcategory_id = 3;
            $start_time_id = 4;
            $end_time_id = 5;
            $file_path_id = 6;
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $num = count($data);
                // echo "<p> $num fields in line $row: <br /></p>\n";
                $array = array();

                for ($c = 0; $c < $num; $c++) {
                    // echo $data[$c] . "<br />\n";
                    //check lst field
                    //$a = array("title","vender_id","category","subcategory",'start_time','end_time','file_path');
                    if ($row === 1) {
                        switch ($data[$c]) {
                            case "vender_name":
                                $vender_id = $c;
                                break;
                            case "title":
                                $title_id = $c;
                                break;
                            case "category":
                                $category_id = $c;
                                break;
                            case "subcategory":
                                $subcategory_id = $c;
                                break;
                            case "start_time":
                                $start_time_id = $c;
                                break;
                            case "end_time":
                                $end_time_id = $c;
                                break;
                            // case "file_path":
                            //     $file_path_id = $c;
                            //     break;
                            default:
                                break;
                        }
                    }
                }
                if ($row !== 1) {
                    $a['id'] = $i;
                    //echo $data[$category_id] . "<br />\n";
                    if (!empty($data[$vender_id]) && $data[$vender_id] != 'undefined') {                       
                        $sql = sprintf("select id,name  from vender where name='%s'", $data[$vender_id]);
                        $venderData = $db->exec($sql);
                        if (count($venderData) > 0) {
                            $a['vender_name'] = $venderData[0]['name'];
                            $a['vender_id'] = $venderData[0]['id'];
                        } else { //上傳有值，但查不到資料，給前端訊息吧
                            $errVender .= "第 ".($row -1) ." 筆 " . $data[$vender_id] . " ,";
                            $a['vender_name'] = "";
                            $a['vender_id'] = "";
                        }
                    } else {
                        $a['vender_name'] = "";
                        $a['vender_id'] = "";
                    }
                    $a['title'] = $data[$title_id];
                    if (!empty($data[$category_id]) && $data[$category_id] != 'undefined') {
                        //$cate = iconv('gbk','utf-8',$data[$category_id]);
                        $sql = sprintf("select id,name  from category where name='%s'", $data[$category_id]);
                        $categoryData = $db->exec($sql);
                        if (count($categoryData) > 0) {
                            $a['category_name'] = $categoryData[0]['name'];
                            $a['category_id'] = $categoryData[0]['id'];
                        } else { //上傳有值，但查不到資料，給前端訊息吧
                            $errCategory .=  "第 ".($row -1) ." 筆 " . $data[$category_id] . " ,";
                            $a['category_name'] = "";
                            $a['category_id'] = "";
                        }
                    } else {
                        $a['category_name'] = "";
                        $a['category_id'] = "";
                    }
                    if (!empty($data[$subcategory_id]) && $data[$subcategory_id] != 'undefined') {
                        $sql = sprintf("select s.id,s.name  from subcategory s 
                        left join category c on c.id=s.category_id  where s.name='%s' and c.name='%s'",
                         $data[$subcategory_id],$data[$category_id]);
                        $categoryData = $db->exec($sql);
                        if (count($categoryData) > 0) {
                            $a['subcategory_name'] = $categoryData[0]['name'];
                            $a['subcategory_id'] = $categoryData[0]['id'];
                        } else { //上傳有值，但查不到資料，給前端訊息吧
                            $errSubCategory .= "第 ".($row -1) ." 筆  " . $data[$subcategory_id] . " ,";
                            $a['subcategory_name'] = ""; 
                            $a['subcategory_id'] = "";
                        }
                    } else {
                        $a['subcategory_name'] = "";                        
                        $a['subcategory_id'] = "";
                    }
                    $a['start_time'] = $data[$start_time_id];
                    $a['end_time'] = $data[$end_time_id];
                    // if (!empty($data[$file_path_id]) && $data[$file_path_id] != 'undefined' && !preg_match('/^[\/][A-Za-z0-9\.\-\_]+$/', $data[$file_path_id])) {
                    //     $a['file_path'] = $data[$file_path_id];
                    //     $a['status'] = "1";
                    // } else {
                    //     $a['file_path'] = "";
                    //     $a['status'] = "0";
                    // }

                    //檔案路徑規則：第一碼請輸入符號為/，限英文、數字、底線、連字號或小數點                   
                    // if (!empty($data[$file_path_id]) && $data[$file_path_id] != 'undefined' && !preg_match('/^[\/][A-Za-z0-9\.\-\_]+$/', $data[$file_path_id])) {
                    //     $errFileFormat .= "第 ".($row -1) ." 筆  " . $data[$file_path_id] . " ,";
                    // }                    
                    $a['company_id'] = $_SESSION['company_id'];
                }

                if ($row != 1) {
                    $rs["data"][$i] = $a;
                    $i++;
                }
                $row++;

            }
            // echo 'errCategory msg='.strlen($errCategory);
            if (strlen($errCategory) > 15) {
                $rs["errCategory"][0] = substr_replace($errCategory, "", -1) . "選項";
            }
            if (strlen($errSubCategory) > 15) {
                $rs["errSubCategory"][0] = substr_replace($errSubCategory, "", -1) . "選項";
            } 
            if (strlen($errVender) > 15) {
                $rs["errVender"][0] = substr_replace($errVender, "", -1) . "選項";
            } 
            // if (strlen($errFileFormat) > 18) {
            //     $rs["errFileFormat"][0] = substr_replace($errFileFormat, "", -1). "(檔案路徑格式第一碼請輸入符號為/，限英文或數字和小位數)";
            // } 
            //upload example    = '[{"title":"1", "vender_name":"tvbs","category":"1","subcategory":"2" ,"start_time":$date2,"end_time",$date2,"file_path":"DD"}]';            
            fclose($handle);
            //echo print_r($rs);
            rest::send($rs);
        }
    }
}
?>