<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data)
{
    $day = 10;
    if (!empty($data['recallDay'])) {
        $day = (int) $data['recallDay'];
    }
    # 未來即將下架(時數) => now <    C.end_time  < now + N-Hours
    // if(empty($data['id'])) rest::state(400,'存取方式錯誤'); //bad request
    $sql = "SELECT
  	l.id,
	  IFNULL(l.title,'') as title
	  FROM live l
   	left join rss_config rss on rss.company_id=l.company_id
	  where  l.end_time is not null and rss.token=?
		and UNIX_TIMESTAMP(l.end_time) - UNIX_TIMESTAMP(DATE_ADD(now(), INTERVAL - ? Day))  >= 0
		and UNIX_TIMESTAMP(l.end_time) - UNIX_TIMESTAMP(NOW()) < 0
		and rss.company_id=? ";

//status in (3,4) and
    $whereStmt = array();
    $i = 1;
    $whereStmt[$i++] = $data['token'];
    $whereStmt[$i++] = (int) $data['recallDay'];
    $whereStmt[$i++] = $_SESSION['company_id'];

    if (!empty($data['limit_rows']) && $data['limit_rows'] != 'undefined') {
        $whereStmt[$i++] = (int) $data['limit_rows'];
        $sql .= " limit ? ";
    } else {
        $sql .= " limit 1000 ";
    }
    $rows = $db->exec($sql, $whereStmt);

    // if(count($rows)==0) rest::state(404,'查無此服務或服務中止'); //no content

    $rs = array("title" => "HamiVideo 企業影視服務",
        "copyright" => "中華電信數據通信分公司版權所有 © 2008 HiNet Internet Service by Chunghwa Telecom All Rights Reserved.",
        "version" => "1.0",
        "channel" => array());
    $i = 0;
    foreach ($rows as $row) {
        $sql = "select
				 IFNULL(l_url.format,'') as format,
				 IFNULL(l_url.device,'') as device,
				 IFNULL(l_url.quality,'') as quality,
				 IFNULL(l_url.drm,'') as drm,
				 IFNULL(l_url.url,'') as url
				 from  live_url l_url
				 where l_url.live_id=?";
        $whereStmt = array();
        $j = 1;
        $whereStmt[$j++] = (int) $row['id'];
        $liveUrlData = $db->exec($sql, $whereStmt);
        if (count($liveUrlData) != 0) {
            $row['urls'] = $liveUrlData;
        } else {
            $row['urls'] = "";
        }
        $sql = "select
				IFNULL(l_egp.text,'') as text,
				IFNULL(l_egp.start_time,'') as start_time,
				IFNULL(l_egp.end_time,'') as end_time
					from  live_epg l_egp
				where live_id=?";
        $whereStmt = array();
        $j = 1;
        $whereStmt[$j++] = (int) $row['id'];
        $liveEpgData = $db->exec($sql, $whereStmt);
        if (count($liveEpgData) != 0) {
            $row['epgs'] = $liveEpgData;
        } else {
            $row['epgs'] = array();
        }
        //unset($row['id']);
        $rs["channel"][$i] = $row;
        $i++;

    }
    rest::send($rs);
}
