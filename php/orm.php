<?php
include_once('./orm/base.php');
include_once('./orm/magic.php');
include_once('./orm/db/cursor.php');
include_once('./orm/db/sql.php');
require_once('/tmp/conn.php');

class orm{
	protected static $db;
	 
	private function __construct() {
		try {
			if ($_SERVER['HTTP_HOST'] == 'localhost') // or any other host
			{
				self::$db = new DB\SQL(
					'mysql:host=127.0.0.1;port=3306;dbname=phpwithng',
					'root',
					'root'
				);
			}else{
				self::$db = new DB\SQL(
					'mysql:host=172.16.61.23;port=3306;dbname=ott2b',
					'AppConn',
					'Pa$$1234567!'
				);
			}
			
		}
		catch (PDOException $e) {
			echo "Connection Error: " . $e->getMessage();
		}
	}
	public static function db() {
		if (!self::$db) new orm();
		return self::$db;
	}
}
?>