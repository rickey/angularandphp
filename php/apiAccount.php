<?php
include_once('rest.php');
rest::go(rest::getReq(),orm::db());

function query($db,$data,$company){
	$perpage = 10;
	$page = 1;
	if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
	}

	$sql = "SELECT a.*,c.company_name FROM account a		
			left join company c on c.id = a.company_id	
			where 1=1  ";
	$whereSql = "";
	$whereStmt = array();
	$i = 1;
	
	if (!empty($data['searchTitle'])  && $data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
		$str = "%".$data['searchTitle']."%";
		$whereSql .= " and (a.name like ?)";
		$whereStmt[$i++] = $str;
	}

	if (!empty($data['id'])) {
        $whereSql .= " and a.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
	}
	if (!empty($data['name'])  && !empty($data['pass'])) {
        $whereSql .= " and a.name = ? and a.pass= ? and a.company_id = ?";
		$whereStmt[$i++] =  $data['name'];
		$whereStmt[$i++] =  $data['pass'];
		$whereStmt[$i++] =  $data['company_id'];
	} else { //不是登入時，要驗証公司別
		$whereSql .= " and a.company_id=? ";
		$whereStmt[$i++] = $_SESSION['company_id'];
	}
	
	$orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " limit $page,$perpage";
	} 

	// echo print_r($data);
	// echo print_r($orderSql).','.print_r($orderStmt);
	$rows = $db->exec($orderSql, $orderStmt);    
	
	$rs = array("data" =>array());
	$i = 0;
	foreach($rows as $row) {
		//記錄session
		$_SESSION['company'] = $row['company'];
		$_SESSION['company_id'] = $row['company_id'];
		$_SESSION['super_user'] = $row['super_user'];
		if ($row['admin'] !== null  && $row['admin'] !== "0") {			
			$_SESSION['admin'] = $row['admin'];
		} else {			
			$_SESSION['admin'] = null;
		}  
		
		// echo 'company==========='.$row['company'];
		$rs["data"][$i] = $row;
		$i++;
	}

	$count_sql = "SELECT COUNT(*)  as total FROM account a
		left join company c on c.id=a.company_id
		where 1=1  ";
	$count_sql .= $whereSql;	
	$total = $db->exec($count_sql, $whereStmt);	
	foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
	}

	rest::send($rs);
}

function getRoles($db,$row,$company){
	$roles = array("id"=>array(),"name"=>array());
	$stmt=array();$i=1;
	$sql = "select r.name,r.id from account_role ar 
			left join role r on r.id = ar.role_id
			where ar.account_id=? and ar.company=?";
	$stmt[$i++] = $row['id'];
	$stmt[$i++] = $company;
	$rs = $db->exec($sql,$stmt);
	$roles = array();
	$role_names = array();
	for($i=0;$i<count($rs);$i++) {
		$roles['id'][$i] = $rs[$i]['id'];
		$roles['name'][$i] = $rs[$i]['name'];
	}
	return $roles;
}

function create($db,$data){
	// echo print_r($data);
	$stmt=array();$i=1;
	$sql = "select * from account 			
			where name=? and company_id=?";
	$stmt[$i++] = $data[0]->name;
	$stmt[$i++] =  $_SESSION['company_id'];
	$rows = $db->exec($sql, $stmt);
	if (count($rows) > 0) {
	    $msg =  array('status' => "Fail",
			'code' => "400",
			'text' => "Insert Fail");
		echo json_encode($msg, JSON_UNESCAPED_UNICODE);
	} else {
		rest::create($db,$data,'account',false);
	}
	
}

function update($db,$data){
	rest::update($db,$data,'account',false);
}

function delete($db,$data){
	$delSql = "delete from role_account where account_id=?";
	$delStmt = array();
	$i = 1;
	$delStmt[$i++] = $data[0]->id;
	$db->exec($delSql, $delStmt);
	
	$delSql = "delete from group_account where account_id=?";
	$delStmt = array();
	$i = 1;
    $delStmt[$i++] = $data[0]->id;
	$db->exec($delSql, $delStmt);
	
	rest::delete($db,$data,'account');
}


?>