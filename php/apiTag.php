<?php
include_once 'rest.php';
rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{
    $sql = "SELECT  t.*
			from tag t
			where 1=1 ";
    $stmt = array();
    $i = 0;
    // $stmt[$i++] = $company;
   
    if (!empty($data['id'])) {
        $sql .= " and t.id = ?";
        $stmt[$i++] = (int) $data['id'];
    }
    // $sql .= " limit ?";
    // $stmt[$i++] = (isset($data['rows']) && trim($data['rows'])!='')?(int)$data['rows']:10;
    $rows = $db->exec($sql, $stmt);

    $rs = array("data" => array());
    $i = 0;
    foreach ($rows as $row) {
        $rs["data"][$i] = $row;
        $i++;
    }
    rest::send($rs);
}

function create($db, $data)
{
    rest::create($db, $data, 'tag', false);
}

function update($db, $data)
{
    rest::update($db, $data, 'tag', false);
}

function delete($db, $data)
{
    rest::delete($db, $data, 'tag');
}
