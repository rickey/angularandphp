<?php
set_time_limit(600);
include_once 'rest.php';

rest::go(rest::getReq(), orm::db());

function query($db, $data, $company)
{   
    // echo 'com='.$_SESSION['company'];
    // if(!empty($data['toHtmlTable'])) exportHtmlTable($db,$data,$company);
    $perpage = 10;
    $page = 1;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $page = $data['page'];
        $page = ($page - 1) * $perpage;
    } else {
        $page = 0;
    }

    $sql = "SELECT c.*,main.name as main_category_name,
		sub.name as sub_category_name,
	    status.name as status_name,
		vender.name as vender_name
		FROM content c
		left join category main on main.id = c.category_id
		left join subcategory sub on sub.id = c.subcategory_id
		left join status on status.code = c.status
		left join vender on vender.id = c.vender_id
        left join company com on c.company_id=com.id
		where 1=1 and c.company_id=?";
    $whereStmt = array();
    $i = 1;
    $whereStmt[$i++] = (int) $_SESSION['company_id'];
    $whereSql = "";
    if (isset($data['status']) && trim($data['status']) != '') {
        $whereSql .= " and c.status=?";
        $whereStmt[$i++] = $data['status'];
    }
    if (!empty($data['searchTitle']) && $data['searchTitle'] != 'undefined' && isset($data['searchTitle']) && trim($data['searchTitle']) != '') {
        $whereSql .= " and c.title like ? ";
        $whereStmt[$i++] = '%' . $data['searchTitle'] . '%';
    }

    $day = 10;
    if (!empty($data['recallDay']) && $data['recallDay'] != 'undefined') {
        $day = (int) $data['recallDay'];
        $whereSql .= " and now() > DATE_ADD(c.end_time,INTERVAL -$day DAY)  and now() < c.end_time ";
    }

    if (!empty($data['id'])) {
        $whereSql .= " and c.id = ?";
        $whereStmt[$i++] = (int) $data['id'];
    }

    if (!empty($data['vender_id']) && $data['vender_id'] != 'undefined') {
        $whereSql .= " and c.vender_id = ? ";
        $whereStmt[$i++] = (int) $data['vender_id'];
    }

    if (!empty($data['status']) && $data['status'] != 'undefined') {
        $whereSql .= " and c.status = ? ";
        $whereStmt[$i++] = (int) $data['status'];
    }

    if (!empty($data['category_id']) && $data['category_id'] != 'undefined') {
        $whereSql .= " and (c.category_id = ? )";
        $whereStmt[$i++] = $data['category_id'];
    }
    if (!empty($data['subcategory_id']) && $data['subcategory_id'] != 'undefined') {
        $whereSql .= " and (c.subcategory_id = ? )";
        $whereStmt[$i++] = $data['subcategory_id'];
    }

    if (!empty($data['start_time']) && $data['start_time'] != 'undefined') {
        $whereSql .= " and c.start_time >=?";
        $whereStmt[$i++] = $data['start_time'];
    }
    if (!empty($data['end_time']) && $data['end_time'] != 'undefined') {
        $whereSql .= " and c.end_time <=?";
        $whereStmt[$i++] = $data['end_time'];
    }

    $orderSql = $sql;
    $orderSql .= $whereSql;
    $orderStmt = $whereStmt;
    if (!empty($data['page']) && $data['page'] != 'undefined') {
        $orderSql .= " order by c.update_time desc limit $page,$perpage";
    } else {
        $orderSql .= " order by c.update_time desc limit ?";
        $orderStmt[$i++] = (isset($data['rows']) && trim($data['rows']) != '') ? (int) $data['rows'] : 10;
    }

    // echo print_r($orderSql).','.print_r($orderStmt);
    $rows = $db->exec($orderSql, $orderStmt);
    $rs = array("data" => array());
    // echo 'content rows===='.count($rows);
    // echo print_r($rows);
    $i = 0;
    foreach ($rows as $row) {
        // echo print_r($row);
        //過濾vender
        // echo 'admin='.$_SESSION['admin'].',isSuper='.$_SESSION['super_user'];
        // echo ',equal isSuper='.($_SESSION['super_user']=="null") ;
        // echo ',equal admin='.($_SESSION['admin'] === "null");
        if (($_SESSION['super_user'] == "null") && ($_SESSION['admin'] == "null")) {
            // echo 'vender_list session='.print_r($_SESSION['vender_list']);
            if ($_SESSION['vender_list'] != "undefined") {
                $someArray = json_decode($_SESSION['vender_list'], true);
                //  echo 'venderList='.print_r($someArray);        // Dump all data of the Array
                $hasVender = false;
                foreach ($someArray as $key => $value) {
                    // echo $value["name"] . ", " . $value["id"];
                    if ($row['vender_id'] == $value['id']) {
                        $hasVender = true;
                    }
                }
                if ($hasVender) {
                    $rs["data"][$i] = $row;
                    $i++;
                }
            }
        } else {
            $rs["data"][$i] = $row;
            $i++;
        }
    }

    $count_sql = "SELECT COUNT(*) as total FROM content c
        where 1=1 and c.company_id=? ";
    // $count_condSql = " where 1=1 and c.company=?";
    $count_sql .= $whereSql;
    $total = $db->exec($count_sql, $whereStmt);
    foreach ($total as $row) {
        $rs["total"][0] = $row['total'];
    }

    rest::send($rs);

}

function create($db, $data)
{    
    $db->begin();
    //新增一筆content資料及tag資料
    foreach ($data as $d) {
        //echo print_r($data);        
        $c = new DB\SQL\Mapper($db, 'content');
        $c = rest::copyFrom($c, $d);
        $c->update_time = rest::now();
        $c->id = 0;
        if (!empty($data[0]->file_path)) {  
            $pos = strpos($data[0]->file_path,"/");
            if ($pos === false) {
                $c->file_path = "/" .$data[0]->file_path;
            }              
        }
        $c->save();
        echo 'content last id====' . $c->id;
        $id = $c->id;
        $db->commit();
        if (!empty($data[0]->tags)) {
            foreach ($data[0]->tags as $tag) {
                $count = 0;
                $newid = 0;
                foreach ($tag as $key => $value) {
                    $count++;
                    if ($count == 1 && $key == "text") {
                        //新增tag資料表;

                        $t = new DB\SQL\Mapper($db, 'tag');
                        $t->text = $value;
                        $t->save();
                        $newid = $t->id;
                        $db->commit();
                    } else if ($count == 1 && $key == "id") {
                        $a['id'] = $value;
                        $newid = $value;
                    }
                }
                // echo 'add content_tag.new_id=' . $newid . "content_id=" . $id;
                $content_tag['content_id'] = (int) $id;
                $content_tag['tag_id'] = (int) $newid;
                $content_tag_json = json_encode(array($content_tag));
                $content_tag_arr = json_decode($content_tag_json);
                rest::create($db, $content_tag_arr, 'content_tag', false);
                //echo $count;
            }
        }
    }

    if (!empty($data[0]->vender_id) && !empty($data[0]->file_path)) {
        moveFileToFtp($data, $db);         
    } 

}

function update($db, $data)
{   
    //print_r($data[0]->file_path);
    //1-先更新資料
    //echo print_r($data);
    $data[0]->update_time = rest::now();
    if (!empty($data[0]->file_path)) {  
        $pos = strpos($data[0]->file_path,"/");
        if ($pos === false) {
            $data[0]->file_path = "/"   .$data[0]->file_path;
        }        
    }
    rest::update($db, $data, 'content', false);             

    //2-先刪除content_id中content_id，重新新增一次
    $delSql = "delete from content_tag where content_id=?";
    $delStmt = array();
    $i = 1;
    $delStmt[$i++] = $data[0]->id;
    $db->exec($delSql, $delStmt);

    if (!empty($data[0]->tags)) {
        $content_id = $data[0]->id;
        $sql = "select max(id)+1 id from content";
        $rows = $db->exec($sql);
        foreach ($rows as $row) {
            echo 'id====' . $row['id'];
            $id = $row['id'];
            // if(is_array($row['tags'])) {
            foreach ($data[0]->tags as $tag) {
                $count = 0;
                $newid = 0;
                foreach ($tag as $key => $value) {
                    $count++;
                    if ($count == 1 && $key == "text") {
                        echo 'add tag';
                        $tagSql = "select max(id)+1 id from tag";
                        $tagRows = $db->exec($tagSql);
                        foreach ($tagRows as $tag) {
                            $tag_id = $tag['id'];
                            $a['id'] = $tag_id;
                            $newid = $tag_id;
                            $a['text'] = $value;
                            $str = json_encode(array($a));
                            $str2 = json_decode($str);
                            rest::create($db, $str2, 'tag', false);
                        }
                    } else if ($count == 1 && $key == "id") {
                        $a['id'] = $value;
                        $newid = $value;
                    }
                }
                $content_tag['content_id'] = (int) $data[0]->id;
                $content_tag['tag_id'] = (int) $newid;
                $content_tag_json = json_encode(array($content_tag));
                $content_tag_arr = json_decode($content_tag_json);
                rest::create($db, $content_tag_arr, 'content_tag', false);
                echo $count;
            }
        }
    }  
    //3-搬檔案至ftp
    if (!empty($data[0]->vender_id) && !empty($data[0]->file_path) &&  $data[0]->has_upload) {        
        moveFileToFtp($data, $db);                
    }    
}

//檢查ftp登入是否成功，將上傳的tmp資料刪除，傳至ftp
function moveFileToFtp($data, $db)
{
    $tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $data[0]->file_path;
    $remote_file = $data[0]->file_path; 
    //echo "tmp file====" . $tmpFile."\n";
    //echo "remote file====" . $remote_file."\n";
    $loginFtpSuccess = true;
    $ftp_user = "";
    $ftp_pass = "";
    $ftp_ip = "";
    $ftp_port = "";
    $conn_id = "";

    //取ftp的ip跟port
    //$ftpIpAndPort = getFtpIpAndPort($db);
    $checkFtp = new ftpData;
    $ftpIpAndPort = $checkFtp->getFtpIpAndPort($db);

    $ftp_ip = $ftpIpAndPort[0];
    $ftp_port = $ftpIpAndPort[1];
    //依vender的id查出ftp登入帳號/密碼
    $ftpAccountAndPwd = $checkFtp->getFtpAccountAndPwdByVenderId($data[0]->vender_id, $db);
    $ftp_user = $ftpAccountAndPwd[0];
    $ftp_pass = $ftpAccountAndPwd[1];

    //必要ftp_connt & ftp_login & ftp_put
    if ($loginFtpSuccess) {
        $conn_id = ftp_connect($ftp_ip, $ftp_port);
        $login_result = ftp_login($conn_id, $ftp_user, $ftp_pass);            
        if(file_exists($tmpFile)) {
            ftp_pasv($conn_id, true);
            if (ftp_put($conn_id, $remote_file, $tmpFile, FTP_BINARY)) {
                echo "successfully uploaded \n";
            } else {
                echo "There was a problem while uploading \n";                        
            }
        }
        ftp_close($conn_id);
    } else {
        return "fail";
    }                    

    echo "file exist=".file_exists($tmpFile);
    if(file_exists($tmpFile)) {
        unlink($tmpFile);
    }
    return "ok";
}

 

function delete($db, $data)
{
    rest::delete($db, $data, 'content');

    //先刪除content_id中content_id，重新新增一次
    $delSql = "delete from content_tag where content_id=?";
    $delStmt = array();
    $i = 1;
    $delStmt[$i++] = $data[0]->id;
    $db->exec($delSql, $delStmt);
}
// function create($db,$data){
//     $db->begin();
//     foreach ($data as $d) {
//         $c = new DB\SQL\Mapper($db,'content');
//         if($d->devices&&is_array($d->devices)) $d->devices = join(",",$d->devices);
//         if($d->platforms&&is_array($d->platforms)) $d->platforms = join(",",$d->platforms);
//         $c = rest::copyFrom($c,$d);
//         $c->update_time = rest::now();
//         $c->save();
//         $c->hash = "OTT_VOD_".rest::pad($c->_id,10);
//         $c->update();
//     }
//     $db->commit();
// }

// function update($db,$data){
//     $db->begin();
//     foreach ($data as $d) {
//         $c = new DB\SQL\Mapper($db,'content');
//         $c->load(array('id=?',$d->id));
//         if($d->devices&&is_array($d->devices)) $d->devices = join(",",$d->devices);
//         if($d->platforms&&is_array($d->platforms)) $d->platforms = join(",",$d->platforms);
//         $c = rest::copyFrom($c,$d);
//         if(!empty($d->map_file)&&$d->map_file=="1"){
//             $f = new DB\SQL\Mapper($db,'file');
//             $f->load(array('id=?',$d->source_file_id));
//             $f->status=1;
//             $f->update();
//         }
//         if(!empty($d->map_preview_file)&&$d->map_preview_file=="1"){
//             $f = new DB\SQL\Mapper($db,'file');
//             $f->load(array('id=?',$d->source_preview_file_id));
//             $f->status=1;
//             $f->update();
//         }
//         if(!empty($d->map_dir)&&$d->map_dir=="1"){
//             $f = new DB\SQL\Mapper($db,'dir');
//             $f->load(array('id=?',$d->source_dir_id));
//             $f->status=1;
//             $f->update();
//         }
//         if(!empty($d->map_preview_dir)&&$d->map_preview_dir=="1"){
//             $f = new DB\SQL\Mapper($db,'dir');
//             $f->load(array('id=?',$d->source_preview_dir_id));
//             $f->status=1;
//             $f->update();
//         }
//         if(!empty($d->job_action)){
//             $j = new DB\SQL\Mapper($db,'job');
//             if(!empty($d->source_dir_id)&&$d->source_dir_id!="0")
//                 $j->load(array('content_id=? and dir_id=? and file_id is null order by update_time desc limit 1',$d->id,$d->source_dir_id));
//             else
//                 $j->load(array('content_id=? and file_id=? and dir_id is null order by update_time desc limit 1',$d->id,$d->source_file_id));
//             $j->update_time = rest::now();
//             $j->encrypt = ($d->encrypt !=null && $d->encrypt >=1)?1:0;
//             $j->devices = $d->devices;
//             $j->status = $d->job_action;
//             $j->update();
//         }
//         if(!empty($d->job_action_preview)){
//             $j = new DB\SQL\Mapper($db,'job');
//             if(!empty($d->source_preview_dir_id)&&$d->source_preview_dir_id!="0")
//                 $j->load(array('content_id=? and dir_id=? and file_id is null order by update_time desc limit 1',$d->id,$d->source_preview_dir_id));
//             else
//                 $j->load(array('content_id=? and file_id=? and dir_id is null order by update_time desc limit 1',$d->id,$d->source_preview_file_id));
//             $j->update_time = rest::now();
//             $j->devices = $d->devices;
//             $j->status = $d->job_action_preview;
//             $j->update();
//         }
//         $c->update_time = rest::now();
//         $c->update();
//     }
//     $db->commit();
// }
