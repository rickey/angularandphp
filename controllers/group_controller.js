myApp.controller('group_controller', function ($scope, $state, $http, $location) {
    var vm = this;  
    $scope.selectedRow = null;
    $scope.currentPage = 1;
    
    //初始帳號資料
    this.init_account_data = function () {
        $scope.account_data = [];
        $http({
            method: 'get',
            url:'php/getAccountList.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('accountData=' + JSON.stringify(response.data.data));
            $scope.account_data = response.data.data;            
        });
    };  

    //初始功能資料
    this.init_vender_data = function () {
        $scope.vender_data = [];
        $http({
            method: 'get',
            url: 'php/getVenderList.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {   
            console.log('venderData=' + JSON.stringify(response.data.data));         
            $scope.vender_data = response.data.data;            
        });
    }; 
    
    //初始帳號及功能資料
    this.init_add_info = function () {
        this.init_account_data();
        this.init_vender_data();
    };

    //限制只能從帳號選取
    $scope.account_list = [];
    $scope.on_account_added = function($tag) {
        console.log('id='+ $tag.id +',len='+$scope.account_list.length);
        console.log('equal=' + ($tag.id===undefined));
        console.log('index='+$scope.account_list.indexOf($tag));
        if ($tag.id===undefined) {
            console.log('account_list length='+$scope.account_list);
            console.log('edit_accounts length='+$scope.edit_accounts);
            if ($scope.account_list != undefined) {
                $scope.account_list.splice($scope.account_list.indexOf($tag), 1);
            }                
            if ($scope.edit_accounts != undefined) {
                $scope.edit_accounts.splice($scope.edit_accounts.indexOf($tag), 1);
            }
            
        }  
    };   
    
    //限制只能從內容商清單選取
    $scope.vender_list = [];
    $scope.on_vender_added = function($tag) {
        console.log('on_vender_added='+$tag.id);
        if ($tag.id == undefined) {      
            if ($scope.vender_list != undefined) {
                $scope.vender_list.splice($scope.vender_list.indexOf($tag), 1);
            }
            if ($scope.edit_venders != undefined) {
                $scope.edit_venders.splice($scope.edit_venders.indexOf($tag), 1);
            }                       
         }
    };  


    this.search_data = function (search_input) {
        vm.load_data(1);
    }; 

    this.load_data = function (page_number) {   
        console.log('comid='+  localStorage.getItem('company_id'))     
        $http({
            method:'get',
            url:'php/apiGroup.php?searchTitle='+$scope.searchTitle + "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('main size=' + JSON.stringify(response)); 
            vm.group_list = response.data.data;
            $scope.total_row = response.data.total;
        });
    };

    this.load_data(1);
    
    this.selectContent = function (info) {                
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };


    this.add = function (info) {  
        info.company_id = localStorage.getItem('company_id');      
        info.account_list = $scope.account_list;
        info.vender_list = $scope.vender_list;
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiGroup.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            vm.load_data(1); 
            document.getElementById('close-modal1').click();          
        });
    };

     //編輯時帶出group_account資料
     this.load_group_account = function (id) {
        $http({
            method: 'get',
            url: 'php/apiGroupAccount.php?id=' + id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('content tag data=' + JSON.stringify(response));
            $scope.edit_accounts = response.data.data;            
        });
    };   

    //編輯時帶出 group_vender 資料
    this.load_group_vendor = function (id) {
        $http({
            method: 'get',
            url:'php/apiGroupVender.php?id=' + id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('content tag data=' + JSON.stringify(response));
            $scope.edit_venders = response.data.data;            
        });
    };   

    //編輯時帶出role/role_account/role_fucntion資料
    this.edit_info = function (id) {
        $('#edit_info_modal').modal('show');
        this.load_group_account(id);
        this.load_group_vendor(id);

        console.log('edit id='+id);
        $http({
            method:'get',
            url:'php/apiGroup.php?id='+id+'&searchTitle=',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('data=' + JSON.stringify(response.data.data[0])); 
            vm.group_info = response.data.data[0];                                 
        });        
    };

    this.update = function () { 
        this.group_info.account_list = $scope.edit_accounts;
        this.group_info.vender_list = $scope.edit_venders;
        var FormData = [this.group_info];
        $http({
            method:'PUT',
            url:'php/apiGroup.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
        }).then(function (response) {
            // console.log('data=' + JSON.stringify(response));               
            document.getElementById('close-modal2').click();
            vm.load_data($scope.currentPage);  
        });  
    };

    this.delete = function (id) {
        bootbox.confirm("確定刪除嗎", function(confirm) {
            if (confirm) { 
                var FormData = [{id:id}];
                $http({
                    method:'DELETE',
                    url:'php/apiGroup.php',
                    data: FormData,
                    headers:{'CONTENT_TYPE': 'application/json'}           
                  }).then(function (response) {
                    vm.load_data($scope.currentPage);
                    $('.modal').modal('hide');          
                });
            }
        });
    };
    
    $scope.$watch('currentPage + numPerPage', function () {
        vm.load_data($scope.currentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });
});