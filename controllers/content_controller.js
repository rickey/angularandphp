 

myApp.controller('content_controller', function ($rootScope, $scope, $state, $modal, $http, $location, $timeout, $window, FileUploader) {
    var vm = this;  
    $scope.content = {};  
    $scope.content_info = {};
    $scope.desc = 0;//排序       
    $scope.msg_area = false; 

    $scope.hourData = [];
    for(var i=0;i<=24;i++) {
        if (i< 10) {
            i = "0" + i;
       }
        $scope.hourData.push(i);
    }    
    $scope.minuteData = [];
    for(var i=0;i<=59;i++) {          
          if (i< 10) {
               i = "0" + i;
          }
        $scope.minuteData.push(i);
    }

    function getUploadMetaData(file) {
        var file = document.getElementById(file).files;
        var newFileName = "";
        var fd = new FormData();
        if (file.length > 0) {
            console.log("原始檔名=" + file[0].name);
            console.log(file);
            var str = new Array();
            str = file[0].name.split(".");
            var extname = str[str.length - 1];
            var d = new Date();
            var n = d.getFullYear() + "" + (d.getMonth() + 1) + "" + d.getDate() +
                d.getHours() + "" + d.getMinutes() + "" + d.getSeconds() + "" + d.getTime();
            ;
            newFileName = n + "." + extname;
    
            fd.append('file', file[0], newFileName);
            //fd.append('MAX_FILE_SIZE', 8 * 1024);
            console.log('fd=' + JSON.stringify(fd));
        }
        return { newFileName, fd };
    } 
    
   /*新增前驗証上傳
    上傳前內容商(ftp)必填；size限制1G
    副檔名必需為audio
    */
   document.getElementById('addFile').onchange = function () {
        // alert('Selected file: ' + this.value);
        document.getElementById('addFileName').innerText = "";
        console.clear();
        var selectedFile = document.getElementById('addFile').files[0];
        var fileName = selectedFile.name;
        console.log(fileName);
        console.log("file length="+ fileName.length);
        if (fileName.length > 0) {
            console.log("原始檔名=" + fileName);        
            document.getElementById('addFileName').innerText = fileName;       
        }
        checkUploadFileFormat('addFile');        
    };

    /*更新前驗証上傳
    上傳前內容商(ftp)必填；size限制1G
    副檔名必需為audio
    */
    document.getElementById('editFile').onchange = function () {
        // alert('Selected file: ' + this.value);
        document.getElementById('editFileName').innerText = "";
        console.clear();
        var selectedFile = document.getElementById('editFile').files[0];
        var fileName = selectedFile.name;
        console.log(fileName);
        console.log("file length="+ fileName.length);
        if (fileName.length > 0) {
            console.log("原始檔名=" + fileName);
            $scope.file_name = fileName;   
            document.getElementById('editFileName').innerText = fileName;       
        }

        checkUploadFileFormat('editFile');        
    };

    function checkUploadFileFormat(file) {
        var selectedFile = document.getElementById(file).files[0];
        var fileName = selectedFile.name;
        console.log('checkUploadFileFormat'+fileName);
    
        var ext = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length).toLowerCase();
        console.log(ext);
        var validFilesTypes = ["mp4", "avi", "flv", "wmv", "mov"];
        var isValidFile = false;
        for (var i = 0; i < validFilesTypes.length; i++) {
            if (ext == validFilesTypes[i]) {
                isValidFile = true;
                break;
            }
        }
    
        if (!isValidFile) {
            alert("無效的檔案" + fileName + "。請上傳副檔名為：\n\n" + validFilesTypes.join(","));
            document.getElementById($scope.updFileName).value = "";
        }
    
        //上傳前內容商(ftp)必填；size限制1G
        console.log("file size=" + selectedFile.size);
        var fileSize = (selectedFile.size / 1024 / 1024 / 1024);
        if (parseFloat(fileSize) > parseFloat(1)) {
            alert('檔案大小限制為1G');
            document.getElementById($scope.updFileName).value = "";
        }
    }

    //document.getElementById('url').value = "http://ott2bcmtest-hieventtrial2.cdn.hinet.net/vod_ott2bcmtest/_definst_//smil:ott2bcmtest//test3//hd-hls-cl-pc.smil/playlist.m3u8";
    //document.getElementById('player').src = "templates/content/player.html?url="+document.getElementById("url").value;
    $scope.showPalyer = true;
    $scope.chgPlayUrl = function (url) {
        // alert(url);        
        // alert($scope.url_data[0].url);
        document.getElementById('url').value = url;
        document.getElementById('player').src = "templates/content/player.html?url=" + url;
    }
    $scope.selectedRow = null;
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.status = "";

    var delay = 1000, setTimeoutConst;
    $scope.changeImg = function (img) {
        // alert('hover in');        
        setTimeoutConst = $timeout(function () {
            $scope.imgsrc = img;
            $('#img_modal').modal('show');
        }, delay);
    };

    $scope.removeImg = function () {
        // alert('hover out');        
        setTimeoutConst2 = $timeout(function () {
            document.getElementById('img-close-modal').click();
            $scope.imgsrc = '';
        }, delay);


    };

    //初始tag資料
    this.loadTag = function (query) {
        $scope.tagData = [];
        $http({
            method: 'get',
            url: 'php/apiTag.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('tag data=' + JSON.stringify(response.data.data));
            $scope.tagData = response.data.data;
        });
    };

    //初始主類別資料
    this.loadMainCategory = function () {

        $http({
            method: 'get',
            url: 'php/apiCategory.php?key=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('category data=' + JSON.stringify(response));
            $scope.catalogData = response.data.data;
            $scope.optionDefault = { category_id: "", name: "--請選擇--" };
            $scope.catalogData.splice(0, 0, $scope.optionDefault);
           // $scope.category = "";

           $scope.insertCatalogData = response.data.data;           
           //$scope.insertCatalogData.splice(0, 0, $scope.optionDefault);

            $scope.subCatalogData = [{ subcategory_id: "", name: "--請選擇--" }];
            $scope.subCatalogData.splice(0, 0, $scope.optionDefault);            
            $scope.subcategory = "";
            
            $scope.querySubCatalogData = [{ subcategory_id: "", name: "--請選擇--" }];
            // $scope.querySubCatalogData.splice(0, 0, $scope.subCatalogData);
        });
    };
    

    //初始內容商
    this.loadVender = function () {
        $http({
            method: 'get',
            url: 'php/apiVender.php?searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('vender data1=' + JSON.stringify(response));
            $scope.venderData = response.data.data;
            $scope.optionDefault = { code: "", name: "--請選擇--" };
            $scope.venderData.splice(0, 0, $scope.optionDefault);
        });
    };

    //初始狀態資料
    this.loadStatus = function () {
        $http({
            method: 'get',
            url: 'php/getStatusList.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            //console.log("status data = " + JSON.stringify(response));
            $scope.statusData = response.data;
            $scope.optionDefault = { code: "", name: "--請選擇--" };
            $scope.statusData.splice(0, 0, $scope.optionDefault);
            $scope.status = "";
        });
    };

    this.loadContentTag = function (id) {
        $http({
            method: 'get',
            url: 'php/apiContentTag.php?id=' + id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('content tag data=' + JSON.stringify(response));
            $scope.edit_tags = response.data.data;
            $scope.view_tags = response.data.data;
        });
    };

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
      };

    this.loadData = function (page_number, action) {   
        if (action == "query") {
            $scope.setPage(1);    
        }        
        console.log('page='+JSON.stringify($scope.currentPage));
        //何留以後要查開始/結束時間
        // var query_start_time = document.getElementById("query_start_time");
        // console.log('startTime=' + query_start_time.value);
        // var query_end_time = document.getElementById("query_end_time");
        // console.log('endTime=' + query_end_time.value);

        //+ "&start_time=" + query_start_time.value + "&end_time=" + query_end_time.value
        $http({
            method: 'get',
            url: 'php/apiContent.php?searchTitle=' + $scope.searchTitle + "&page=" + page_number
                + "&status=" + $scope.status 
                + "&vender_id=" + $scope.query_vender_id + "&category_id=" + $scope.query_category_id + "&subcategory_id=" + $scope.query_subcategory_id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            //console.log('main size=' + JSON.stringify(response));
            vm.content_list = response.data.data;
            $scope.total_row = response.data.total;
            console.log('total=' + response.data.total);
            //console.log('data=' + JSON.stringify(response.data.data));
            if (action == 'query' && response.data.total == 0) {
                $scope.msg = '查無資料!!';
                $scope.msg_area = true;
            } else if(action == 'query' && response.data.total > 0) {
                $scope.msg = '';
                $scope.msg_area = false;
            } 
            angular.forEach(vm.content_list, function (content) {
                content.id = parseFloat(content.id);
                if (content.job_note != null) {
                    console.log("[=" + content.job_note.indexOf("["));
                    console.log("]=" + content.job_note.indexOf("]"));
                    content.job_note = content.job_note.substring(content.job_note.indexOf("[") +2,content.job_note.indexOf("]")-1);
                }                
                
                // if (content.start_time != null) {
                //     content.start_time = content.start_time.substring(0,10) + "\n"+content.start_time.substring(11,16);
                // }
                // if (content.end_time != null) {
                //     content.end_time = content.end_time.substring(0,10) + "\n"+content.end_time.substring(11,16);
                // }
                if (content.run_time !== null) {
                    content.run_time = parseFloat(content.run_time);
                }                
                $http({
                    method: 'get',
                    url: 'php/getUrlList.php?job_id=' + content.job_id,
                    data: FormData,
                    headers: { 'CONTENT_TYPE': 'application/json' }
                }).then(function (urlData) {
                    // console.log('url data=' + JSON.stringify(urlData));                                        
                    angular.forEach(urlData.data.data, function (data) {
                        
                        if (data.format == "jpg") {
                            console.log(content.title + ',' + data.format + ',' + data.url);
                            content.thumbnail = data.url;
                        }
                    });
                });
            });

            //儲存前一頁選取資料
            $scope.selectAllData = [];
            angular.forEach($scope.selectData, function (parentData) {
                $scope.selectAllData.push(parentData);
            });
            //從前頁跳回本頁時，判斷是否已勾選
            angular.forEach($scope.selectData, function (parentData) {
                angular.forEach(vm.content_list, function (item) {
                    // console.log('item='+JSON.stringify(item));
                    
                    if (item.id == parentData.id) {
                        vm.content_list.checked = true;
                        
                        item.checked = true;
                    }
                });
            });

            // this.loadContentTag(1);    
            //this.loadTag();
        });
    };

    $scope.disableApprove = true;    

    // $scope.init = function () {        
    //     vm.loadData($scope.currentPage);        
    // };
    // $scope.init();
    vm.loadVender();
    vm.loadMainCategory();
    vm.loadStatus();
    // vm.loadData($scope.currentPage);           

    //checked box evnent
    $scope.selectData = [];
    $scope.selectAllData = [];

    this.batchApprove = function () {
        bootbox.confirm("審核確定嗎", function (confirm) {
            if (confirm) {
                angular.forEach($scope.selectData, function (item) {
                    console.log('item=' + JSON.stringify(item));
                    console.log(item.status);
                    if (item.status == 3) {
                        item.status = 4;
                        var FormData = [item];
                        $http({
                            method: 'PUT',
                            url: 'php/apiContent.php',
                            data: FormData,
                            headers: { 'CONTENT_TYPE': 'application/json' }
                        }).then(function (response) {
                            // console.log('data=' + JSON.stringify(response)); 
                            vm.loadData(1,'batchApprove');
                        });
                    }
                });
                alert('已審核完成');
            }
        });
    };    

    this.search_data = function (search_input) {        
        
        $scope.alert_style = "alert-info";        
        vm.loadData(1,'query');

    };

   
    $scope.$watch('currentPage + numPerPage', function () {
        console.log('currentPage=' + $scope.currentPage);
        vm.loadData($scope.currentPage,'paging');
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
            , end = begin + $scope.numPerPage;
    });

    $scope.checkFilePath = true;
    this.checkFirstWord = function (content) {
        if (content.file_path != "" && content.file_path.substring(0, 1) != "/") {
            $scope.checkFilePath = false;
        } else {
            $scope.checkFilePath = true;
        }
        console.log('key=' + content.file_path.substring(0, 1));
    };

    this.dblClick = function (clicked) {
        alert("My ng-dbl-Click function is called.");
    };  

    //修改時：標題改變
    this.editChangeByTitle = function (title) {
        console.log(title === undefined);
        if ((title !== undefined)) {
            $scope.edit_content_info.title.invalid = false;
        } else {
            $scope.edit_content_info.title.invalid = true;            
        }        
        $scope.enableEditBtn();        
    };

     //修改時：內容商改變
     this.editChangeByVenderId = function (venderId) {
        console.log(venderId === undefined);
        if ((venderId !== undefined)) {
            $scope.edit_content_info.vender_id.invalid = false;
        } else {
            $scope.edit_content_info.vender_id.invalid = true;
        }  
        $scope.enableEditBtn(); 
     };

     //修改時：次類別異動
     this.editChangeBySubcategoryId = function (subcategory_id) {
        console.clear();        
        console.log("editChangeBySubcategoryId=====");  
        console.log(subcategory_id === "");
        if ((subcategory_id !== "")) {
            $scope.edit_content_info.subcategory_id.invalid = false;
        } else {
            $scope.edit_content_info.subcategory_id.invalid = true;
        } 
        $scope.enableEditBtn();
    };

    //修改時：編輯主分類異動時
    this.editChangeByCategory = function (category_id) {
        console.clear();        
        console.log("editChangeByCategory=====");        
        console.log('1:category id===' + category_id);
        console.log("1:this subcategory_id===="+ this.content_info.subcategory_id);
        console.log("2:DOM subcategory_id="+document.getElementById("subcategory_id").value);
        console.log(document.getElementById("subcategory_id").value === "");      
        console.log(category_id === undefined);
       
        //初始化次分類
        $scope.edit_content_info.subcategory_id.invalid = true;
        $scope.subCatalogData = [];
        this.content_info.subcategory_id = "";
        if ((category_id !== undefined)) {
            $scope.edit_content_info.category_id.invalid = false;
            $http({
                method: 'get',
                url: 'php/apiSubCategory.php?category_id=' + category_id + "&key=",
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (response) {
                console.log('sub data size=' + JSON.stringify(response.data.data));
                $scope.subCatalogData = response.data.data;
                $scope.optionDefault = { id: "", name: "--請選擇--" };
                $scope.subCatalogData.splice(0, 0, $scope.optionDefault);
                $scope.subcategory = "";
                document.getElementById("subcategory_id").value = "";
            });
        } else {
            $scope.edit_content_info.category_id.invalid = true;
             $scope.optionDefault = { id: "", name: "--請選擇--" };
            $scope.subCatalogData.splice(0, 0, $scope.optionDefault);
            document.getElementById("subcategory_id").value = "";
        }   
        $scope.enableEditBtn();        
    };

        
 
    //修改時：開始時間改變
    $scope.editChangeByStime = function (time) {
        
        var start_time_lenth = document.getElementById("edit_start_time").value.length;
        console.log("time="+time +',isDate='+ angular.isDate(time)+",time size="+ start_time_lenth
            +",len=" + start_time_lenth +",size=10="+(start_time_lenth == 10));
        if ((angular.isDate(time) && (start_time_lenth == 10)) || (start_time_lenth===0)) {                   
            $scope.edit_content_info.edit_start_time.invalid = false;            
        } else {
            $scope.edit_content_info.edit_start_time.invalid = true;
        } 
        $scope.enableEditBtn();  
    }

    //修改時：結束時間改變
    $scope.editChangeByEtime = function (time) {
        console.log('fStime='+ angular.isDate(time));
        var end_time_lenth = document.getElementById("edit_end_time").value.length;
        if ((angular.isDate(time) && end_time_lenth == 10) || time == "") {
            $scope.edit_content_info.edit_end_time.invalid = false;
        } else {
            $scope.edit_content_info.edit_end_time.invalid = true;
        } 
        $scope.enableEditBtn();  
    } 

     //修改時，判斷可否按"更新"
    $scope.enableEditBtn = function() {  
        $scope.disableEditBtn = true;      
        if (!$scope.edit_content_info.title.invalid &&
            !$scope.edit_content_info.vender_id.invalid &&
            !$scope.edit_content_info.category_id.invalid &&
            !$scope.edit_content_info.subcategory_id.invalid &&
            !$scope.edit_content_info.edit_start_time.invalid &&
            !$scope.edit_content_info.edit_end_time.invalid) {
            $scope.disableEditBtn = false;
        } 
        console.log('disableEditBtn===' +  $scope.disableEditBtn);
    }

     this.disableUpdate = function (content_info) {
        console.log("disableUpdate");
        console.log("vender_id===="+ content_info.vender_id);    
        console.log(content_info.vender_id === undefined);
        console.log("category_id===="+ content_info.category_id);    
        console.log(content_info.category_id === undefined);
        console.log("subcategory_id===="+ content_info.subcategory_id);        
        console.log(content_info.subcategory_id === undefined);
        // $scope.edit_content_info.category_id.invalid = false;
        // $scope.edit_content_info.category_id.invalid = false;
        // $scope.edit_content_info.category_id.invalid = false;
        if(content_info.vender_id === undefined) {
            $scope.edit_content_info.vender_id.invalid = true;
        } else {
            $scope.edit_content_info.vender_id.invalid = false;
        }
        if(content_info.category_id === undefined) {
            $scope.edit_content_info.category_id.invalid = true;
        } else {
            $scope.edit_content_info.category_id.invalid = false;
        }
        if(content_info.subcategory_id === undefined) {
            $scope.edit_content_info.subcategory_id.invalid = true;
        } else {
            $scope.edit_content_info.subcategory_id.invalid = false;
        }

        // if (!edit_content_info.title.invalid &&
        //     !edit_content_info.vender_id.invalid &&
        //     !edit_content_info.category_id.invalid &&
        //     !edit_content_info.subcategory_id.invalid) {
        //         $scope.edit_content_info.invalid = false;
        // }
        // else {
        //     $scope.edit_content_info.invalid = true;
        // }
    }
     

    //編輯：點選某列
    this.selectContent = function (info) {       

        console.clear(); 
        $scope.progressCounter = 0;
        this.disableUpdate(info);
        document.getElementById("editFile").value = "";
        
        console.log("info data = "+ JSON.stringify(info));
        document.getElementById("editFileName").innerText = info.file_name;
        $scope.updBtn = true;

        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }

        if (info.checked == undefined) {
            info.checked = true;
        } else {
            info.checked = !info.checked;
        }
        if (info.status === "3" && info.checked) {
            $scope.disableApprove = false;
        } else {
            $scope.disableApprove = true;
        }
        if (info.status === "2" || info.status === "1") {
            $scope.showUnuploadDesc = true;
        } else {
            $scope.showUnuploadDesc = false;
        }
        console.log('this id====' + info.id + "," + info.status + "," + info.checked +
        ",status=1 =>" + (info.status === "1") +
        ",status=1 OR 2=>" + (info.status === "2" || info.status === "1"));
        //check box event
        vm.changeCurrent(info);
    };

    //編輯點選某列 & check box event
    this.changeCurrent = function (info) {
        console.log('file path=' +  info.file_path);
       // document.getElementById('editFile').value = "xxx";//info.file_path.substring(1,info.file_path.length)
        console.log('selected id=' + info.id);
        console.log('selected  ALL data Before=' + JSON.stringify($scope.selectAllData));
        // console.log('list data='+ JSON.stringify(vm.content_list));
        $scope.selectData = [];
        angular.forEach(vm.content_list, function (item) {
            // console.log('item='+JSON.stringify(item));
            if (item.checked) {
                $scope.selectData[$scope.selectData.length] = item;
            }
        });
        //把前一頁選取的資料捉出來至目前的selectData
        angular.forEach($scope.selectAllData, function (parentData) {
            $scope.selectData.push(parentData);
        });
        // $scope.selectData.push($scope.selectAllData);
        // console.log('selected data=' + JSON.stringify($scope.selectData));
        // console.log('selected  ALL data=' + JSON.stringify($scope.selectAllData));
    };

    //編輯視窗
    this.edit_content_info = function (id) {                
        $scope.msg_area = false;
        $scope.isUpload = "";
        $scope.updBtn = true;
        $scope.content_info.editStartHour = "";
        $scope.content_info.editStartMinute = "";
        $scope.content_info.editStartSecond = "";
        $scope.content_info.editEndHour = "";
        $scope.content_info.editEndMinute = "";
        $scope.content_info.editEndSecond = "";
        //document.getElementById("editFile").value = "";
        $('#edit_content_info_modal').modal('show');
        
        this.loadContentTag(id);
        this.loadTag();
        console.log('edit id=' + id);
        $http({
            method: 'get',
            url: 'php/apiContent.php?id=' + id + '&searchTitle=&page=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('edit data=' + JSON.stringify(response.data.data[0]));
            vm.content_info = response.data.data[0];
            
            console.log("stime len="+ response.data.data[0].start_time.length);
            console.log("etime len="+ response.data.data[0].end_time.length);
            if (response.data.data[0].start_time.length == 19) {
                var date = response.data.data[0].start_time.substring(0,10);
                var hour = response.data.data[0].start_time.substring(11,13);
                var minute = response.data.data[0].start_time.substring(14,16);
                var second = response.data.data[0].start_time.substring(17,19);
                console.log("date="+date+",h="+hour+",m="+minute);
                vm.content_info.edit_start_time = date;
                $scope.content_info.editStartHour = hour;
                $scope.content_info.editStartMinute = minute;
                $scope.content_info.editStartSecond = second;
            }  
            if (response.data.data[0].end_time.length == 19) {
                var date = response.data.data[0].end_time.substring(0,10);
                var hour = response.data.data[0].end_time.substring(11,13);
                var minute = response.data.data[0].end_time.substring(14,16);
                var second = response.data.data[0].end_time.substring(17,19);
                console.log("date="+date+",h="+hour+",m="+minute);
                vm.content_info.edit_end_time = date;
                $scope.content_info.editEndHour = hour;
                $scope.content_info.editEndMinute = minute;
                $scope.content_info.editEndSecond = second;
            }
            // vm.content_info.edit_start_time = response.data.data[0].start_time;
            // vm.content_info.edit_end_time = response.data.data[0].end_time;
            
            console.log("status=" + response.data.data[0].status + ",equals=====" + (response.data.data[0].status === "4"));
            //未上傳 or 已轉完檔，才允許再上傳
            if (response.data.data[0].status == 0 || response.data.data[0].status == 4 || response.data.data[0].status == 100) {
                $scope.disableFilepath = false;                
            } else {
                $scope.disableFilepath = true;  
                $scope.bodyHeight = true;
            }
            if (response.data.data[0].status == 2) {
                $scope.isUpload = "轉案中無法上傳檔案";
            }
            vm.chgSubCategory(vm.content_info.category_id);

            $http({
                method: 'get',
                url: 'php/getUrlList.php?job_id=' + response.data.data[0].job_id,
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (urlData) {
                // console.log('url data=' + JSON.stringify(urlData));
                $scope.url_data = [];
                // alert($scope.url_data.length);

                angular.forEach(urlData.data.data, function (data) {
                    console.log(data.format);
                    if (data.format == "jpg") {
                        $scope.thumbnail = data.url;
                    } else {
                        $scope.url_data.push(data);
                    }
                });
                if ($scope.url_data.length > 0) {
                    $scope.showPalyer = true;
                    $scope.bodyHeight = "height: 1450px";
                    document.getElementById('player').src = "templates/content/player.html?url=" + $scope.url_data[0].url;
                } else {
                    $scope.showPalyer = false;
                    $scope.bodyHeight = "height: 850px";
                }
            });
        });
    };

    //次目錄
    this.chgSubCategory = function (category) {
        // $rootScope.$apply(function() {
        //     $scope.subCatalogData = [];            
        // });        
        // console.log('subCatalogData=' + JSON.stringify(subCatalogData2));
        console.log('edit chg=' + category);
        $http({
            method: 'get',
            url: 'php/apiSubCategory.php?category_id=' + category + "&key=",
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('sub data size=' + JSON.stringify(response.data.data));
            $scope.subCatalogData = response.data.data;
            $scope.optionDefault = { id: "", name: "--請選擇--" };
            $scope.subCatalogData.splice(0, 0, $scope.optionDefault);
            $scope.subcategory = "";
        });
    };     

    //更新前驗証上傳
    //上傳前內容商(ftp)必填；size限制1G
    //副檔名必需為audio
    // this.onFileEditChange = function () {   
    //     console.clear();     
    //     $scope.updFileName = 'editFile';
    //     var file = document.getElementById("editFile").files;         
    //     console.log("ng file length="+ file.length);
    //     if (file.length > 0) {
    //         console.log("原始檔名=" + file[0].name);
    //         $scope.content_ctrl.content_info.file_name = file[0].name;
    //         checkUploadFileFormat('editFile');//document.getElementById('editFile').files[0]
    //     }
         
    // };


    //更新
    this.updateContent = function () {
        $scope.loading = false;
        $scope.progressCounter = 0;
        $scope.isUpload = "";
        console.clear();

        var edit_start_time = document.getElementById("edit_start_time");        
        this.content_info.start_time = edit_start_time.value;
        var edit_end_time = document.getElementById("edit_end_time");        
        this.content_info.end_time = edit_end_time.value;

        //判斷可否更新:start
        edit_content_info.edit_start_time.invalid = false;
        edit_content_info.edit_end_time.invalid = false;

         var edit_start_time = document.getElementById("edit_start_time");
         var start_time_lenth = document.getElementById("edit_start_time").value.length;
         var end_time_lenth = document.getElementById("edit_end_time").value.length;
         this.content_info.start_time = edit_start_time.value +" "+ 
            $scope.content_info.editStartHour+":"+ $scope.content_info.editStartMinute+":"+ $scope.content_info.editStartSecond;

        var edit_end_time = document.getElementById("edit_end_time");
        this.content_info.end_time = edit_end_time.value +" "+ 
            $scope.content_info.editEndHour+":"+ $scope.content_info.editEndMinute +":"+ $scope.content_info.editEndSecond;
        console.log('start len=' + start_time_lenth);
        console.log('end len=' + end_time_lenth);
        console.log('sTime='+edit_start_time.value+","+(edit_start_time.value !== ""));
        console.log('sHour='+$scope.content_info.editStartHour +","+($scope.content_info.editStartHour !== undefined));
        console.log('sMinute='+$scope.content_info.editStartMinute +","+($scope.content_info.editStartMinute !== undefined));
        console.log('eTime='+edit_end_time.value+","+(edit_end_time.value !== ""));
        console.log('sHour='+$scope.content_info.editEndHour +","+($scope.content_info.editEndHour !== undefined));
        console.log('sMinute='+$scope.content_info.editEndMinute +","+($scope.content_info.editEndMinute !== undefined));
        console.log('full start=' +this.content_info.start_time);
        console.log('full end=' +this.content_info.end_time);
 
        $scope.needSDate = false;
        $scope.needEDate = false;
        //判斷開始日期:1.年月日|時|分任一有值時
        var start_time_lenth = document.getElementById("edit_start_time").value.length;
        var end_time_lenth = document.getElementById("edit_end_time").value.length;

        if ((edit_start_time.value !== "" || $scope.content_info.editStartHour !== undefined 
        || $scope.content_info.editStartMinute !== undefined || $scope.content_info.editStartSecond !== undefined)) {
            if ((edit_start_time.value !== "" && $scope.content_info.editStartHour !== undefined 
            && $scope.content_info.editStartMinute !== undefined && $scope.content_info.editStartSecond!== undefined)) {

            } else {//2.不是全部都有值情況
                alert("開始日期與開始時間必須都為空或都有值");                
                return ;
            }  
            if (start_time_lenth != 10) {
                $scope.edit_content_info.edit_start_time.invalid = true;
                alert("開始日期輸入有誤，格式為YYY-MM-DD");                
                return ;
            }          
            $scope.needSDate = true;
        }
        if ((edit_end_time.value !== "") || ($scope.content_info.editEndHour !== undefined) 
        || ($scope.content_info.editEndMinute !== undefined) || ($scope.content_info.editEndSecond !== undefined)) {
            if ((edit_end_time.value !== "" && $scope.content_info.editEndHour !== undefined
             && $scope.content_info.editEndMinute !== undefined  && $scope.content_info.editEndSecond !== undefined)) {

            } else {
                alert("結束日期與結束時間必須都為空或都有值");                
                return ;
            }
            if (end_time_lenth != 10) {
                $scope.edit_content_info.edit_end_time.invalid = true;
                alert("結束日期輸入有誤，格式為YYY-MM-DD");                
                return ;
            }  
            $scope.needEDate = true;
        }
        if (($scope.needSDate && !$scope.needEDate) || (!$scope.needSDate && $scope.needEDate)) {
            alert("開始日期與結束日期必須都為空或都有值");
            return ;
        }
        
        //判斷結束日期不能小於開始日期
        $scope.diff = $scope.diffDate(this.content_info.start_time, this.content_info.end_time);
        if ($scope.diff <= 0 && $scope.needSDate && $scope.needEDate) {
            alert("結束日期要大於開始日期");            
            return ;
        }         
        //判斷可否更新:end
        
        this.content_info.tags = $scope.edit_tags;
        console.log('update data=>>>>fild is disable=' + $scope.disableFilepath + ",file_path is empty=" + (this.content_info.file_path == null)
            + ",file Path=" + this.content_info.file_path);
        var selectedFile = document.getElementById('editFile').files[0];
        console.log("file=" + selectedFile);
        if (!$scope.disableFilepath && this.content_info.file_path !== null && selectedFile != null) {
            this.content_info.status = "1";
        }
        console.log('file path=' +  this.content_info.file_path);        
        var { newFileName, fd } = getUploadMetaData("editFile");
        if (newFileName != "") {
            this.content_info.file_path = newFileName;
            this.content_info.has_upload = true;
        } else {
            this.content_info.has_upload = false;
        }   
        var file = document.getElementById("editFile").files;        
        if (file.length > 0) {
            console.log("原始檔名=" + file[0].name);
            this.content_info.file_name = file[0].name;
        }
        var updateData = [this.content_info];

        console.log('update data success=' + JSON.stringify(updateData));
        console.log("file=" + this.content_info.file_path);

        if (newFileName !== "" && this.content_info.vender_id !== "") {
            
            $http({
                method: 'get',
                url: 'php/checkFtpConnect.php?vender_id=' + this.content_info.vender_id,
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (response) {
                console.log('checkFtpConnect=' + response.data.isSuccess); 
                if(response.data.isSuccess) {
                    $scope.isUpload = "檔案已上傳";
                    //alert('ok');               
                    if (newFileName !== "") { 
                        console.log('progress');                       
                        //上傳至temp
                        $http({
                            method: 'POST',
                            url: 'php/apiContent_upload.php',
                            data: fd,
                            headers: { 'Content-Type': undefined },
                            transformRequest: angular.identity,
                            uploadEventHandlers: {
                                progress: function (e) {                                       
                                        if (e.lengthComputable) {
                                             $scope.progressBar = Math.round((e.loaded / e.total) * 100);
                                             $scope.progressCounter = $scope.progressBar;
                                             console.log('progress'+$scope.progressBar);
                                        }
                                }
                            }
                        }).then(function (response) {
                            console.log('update success');
                            editData(updateData, $http, vm, $scope);
                        });
                    } else {
                        editData(updateData, $http, vm, $scope);
                    }
                } else {                    
                    alert('上傳失敗，請檢查內容商帳密及FTP資訊是否正確!!');
                    $scope.updBtn = true;
                    $scope.loading = false;
                }               
            });
        } else {
            editData(updateData, $http, vm, $scope);
            $scope.loading = false;
        }
    };

    //從temp發至ftp
    function editData(updateData, $http, vm, $scope) {
        //console.dir('editData=' + JSON.stringify(updateData));
        $http({
            method: 'PUT',
            url: 'php/apiContent.php',
            data: updateData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            //console.log('update data success=' + JSON.stringify(response));
            document.getElementById('close-modal2').click();
            vm.loadData($scope.currentPage,'edit');
            $scope.msg = '更新成功!!';
            $scope.alert_style = "alert-success";
            $scope.msg_area = true;
            $scope.loading = false;
        });
    }

    $scope.checkFirstWordOnEdit = true;
    this.checkFirstWordOnEdit = function () {
        if (this.content_info.file_path != "" && this.content_info.file_path.substring(0, 1) != "/") {
            $scope.checkFirstWordOnEdit = false;
        } else {
            $scope.checkFirstWordOnEdit = true;
        }
        console.log('checkFirstWordOnEdit key====' + this.content_info.file_path.substring(0, 1));
    };


     //查詢時：跟著主類別更動次類別
     $scope.queryChangeByCategory = function (category_id) {
        $scope.querySubCatalogData = [];
        console.log(category_id === undefined); 
        if (category_id !== undefined) {
            $http({
                method: 'get',
                url: 'php/apiSubCategory.php?category_id=' + category_id + "&key=",
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (response) {
                console.log('res data='+response.data.data);
                $scope.querySubCatalogData = response.data.data;
                $scope.optionDefault = { id: "", name: "--請選擇--" };
                $scope.querySubCatalogData.splice(0, 0, $scope.optionDefault);   
            });
        }
        $scope.optionDefault = { id: "", name: "--請選擇--" };
        $scope.querySubCatalogData.splice(0, 0, $scope.optionDefault);            
        $scope.query_subcategory_id = "";
        
    };

    //新增時，判斷可否按"新增"
    $scope.enableAddBtn = function() {  
        $scope.disableAddBtn = true;      
        if (!$scope.create_content_info_frm.title.invalid &&
            !$scope.create_content_info_frm.vender_id.invalid &&
            !$scope.create_content_info_frm.insert_category_id.invalid &&
            !$scope.create_content_info_frm.insert_subcategory_id.invalid &&
            !$scope.create_content_info_frm.insert_start_time.invalid &&
            !$scope.create_content_info_frm.insert_end_time.invalid) {
            $scope.disableAddBtn = false;
        } 
        console.log('disableAddBtn===' +  $scope.disableAddBtn);
    }

     //查詢時：跟著主類別更動次類別
     $scope.queryChangeBySubcategory = function (query_subcategory_id) {
        console.log(query_subcategory_id === "");      
     }
     //新增時：標題改變
     $scope.insertChangeByTitle = function (title) {
        console.log(title === undefined);
        if ((title !== undefined)) {
            $scope.create_content_info_frm.title.invalid = false;            
        } else {
            $scope.create_content_info_frm.title.invalid = true;                      
        }        
        $scope.enableAddBtn();  
    };
    //新增時：開始時間改變
    $scope.insertChangeByStime = function (time) {
        console.log('fStime='+ angular.isDate(time));
        var start_time_lenth = document.getElementById("insert_start_time").value.length;
        if ((angular.isDate(time) && start_time_lenth == 10) || (time===null)) {
            $scope.create_content_info_frm.insert_start_time.invalid = false;            
        } else {
            $scope.create_content_info_frm.insert_start_time.invalid = true;
        } 
        $scope.enableAddBtn();  
    }

    //新增時：結束時間改變
    $scope.insertChangeByEtime = function (time) {
        console.log('fStime='+ angular.isDate(time));
        var end_time_lenth = document.getElementById("insert_end_time").value.length;
        if ((angular.isDate(time) && end_time_lenth == 10) || (time===null)) {
            $scope.create_content_info_frm.insert_end_time.invalid = false;
        } else {
            $scope.create_content_info_frm.insert_end_time.invalid = true;
        } 
        $scope.enableAddBtn();  
    } 

     //新增時：內容商改變
     $scope.insertChangeByVenderId = function (venderId) {
        console.log(venderId === undefined);
        if ((venderId !== undefined)) {
            $scope.create_content_info_frm.vender_id.invalid = false;
        } else {
            $scope.create_content_info_frm.vender_id.invalid = true;
        }     
        $scope.enableAddBtn();   
     };   

     //新增時：跟著主類別更動次類別
     $scope.insertChangeByCategory = function (insert_category_id) {
        console.clear();
        console.log("insertChangeByCategory");
        console.log('category id=' + insert_category_id);
        console.log(insert_category_id === undefined);
        $scope.insertSubCatalogData = [];
        if (insert_category_id !== undefined) {
            $scope.create_content_info_frm.insert_category_id.invalid = false;
            $http({
                method: 'get',
                url: 'php/apiSubCategory.php?category_id=' + insert_category_id + "&key=",
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (response) {
                //console.log('subcategory data by insert'+ JSON.stringify(response.data.data));
                $scope.insertSubCatalogData = response.data.data;
                $scope.optionDefault = { id: "", name: "--請選擇--" };
                $scope.insertSubCatalogData.splice(0, 0, $scope.optionDefault);
                document.getElementById("insert_subcategory_id").value = "";
                $scope.enableAddBtn();                
            });
        } 
        $scope.create_content_info_frm.insert_subcategory_id.invalid = true;
        console.log($scope.insertSubCatalogData.indexOf(""));
        $scope.insertSubCatalogData = [{ id: "", name: "--請選擇--" }];
        $scope.content.insert_subcategory_id = "";                   
        $scope.enableAddBtn();
    };

     //新增時：次類別異動
     $scope.insertChangeBySubcategory = function (subcategory_id) {
        console.log("insertChangeBySubcategory");
        if ((subcategory_id !== undefined)) {
            $scope.create_content_info_frm.insert_subcategory_id.invalid = false;
        } else {
            $scope.create_content_info_frm.insert_subcategory_id.invalid = true;
        }
        $scope.enableAddBtn();
    };
    
   

     //新增時初始化畫面
     this.initAddTag = function () {
        console.clear();
        console.log('initAddTag');              
        console.log("scope.content category_id="+$scope.content.insert_category_id);
        console.log("category_id="+document.getElementById("insert_category_id").value);
        console.log("scope.content subcategory_id="+$scope.content.insert_subcategory_id);
        console.log("subcategory_id="+document.getElementById("insert_subcategory_id").value);
        
        document.getElementById('addFileName').innerText = "";
        $scope.content = {};  
        document.getElementById("create_content_info_frm").reset();
        // $scope.content.insert_category_id = "undefined:undefined";
        $scope.msg_area = false;
        $scope.isUpload = "";
         $scope.disableAddFile = false;
        document.getElementById("addFile").value = "";                
        
        $scope.insertSubCatalogData = [{ id: "", name: "--請選擇--" }];        
        
        $scope.create_content_info_frm.vender_id.invalid = true;         
        $scope.create_content_info_frm.insert_category_id.invalid = true;
        $scope.create_content_info_frm.insert_subcategory_id.invalid = true;
        $scope.create_content_info_frm.title.invalid = true;
        $scope.enableAddBtn();
        // console.log("category_id="+document.getElementById("category_id").value);
        document.getElementById("insert_category_id").value = "undefined:undefined";
        //document.getElementById("insert_category_id").selectedIndex = 0;
        //document.getElementById("insert_subcategory_id").value = "";
        // $scope.content.insert_category_id = "undefined";
       
        $scope.content.insert_subcategory_id = "";
        
        
        $(insert_category_id).change("");
        // $scope.content.subcategory_id = "undefined";
        // document.getElementById("subcategory_id").value = "undefined:undefined";
        // console.log("subcategory_id="+document.getElementById("subcategory_id").value);
        $('#create_content_info_modal').modal('show');
        
        //this.loadContentTag(0);
        this.loadTag();
    };    

    //新增前驗証上傳
    //上傳前內容商(ftp)必填；size限制1G
    //副檔名必需為audio
    $scope.onFileAddChange = function () {        
        $scope.updFileName = 'addFile';
        console.log('add');
        checkUploadFileFormat(checkUploadFileFormat);       
    };

    $scope.diffDate = function(date1, date2){
        console.log('date1='+date1 +","+typeof(date1));
        var date1 = date1;
        console.log('date1='+date1 +","+typeof(date1));
        var date2 = date2;
        var sdate = new Date(Date.parse(date1, "yyyy-MM-dd HH:mm:ss"));
        var edate = new Date(Date.parse(date2, "yyyy-MM-dd HH:mm:ss"));
        var timeDiff =edate.getTime() - sdate.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
        var diffHour = Math.ceil(timeDiff / (1000 * 3600)); 
        var diffMinute = Math.ceil(timeDiff / (1000 * 60)); 
        var diffSecond = Math.ceil(timeDiff / (1000)); 
        console.log('eTime diff sTime ='+timeDiff+",day"+diffDays+",H="+diffHour+",M="+diffMinute+",second="+diffSecond);
        return diffMinute;
    };

    //新增
    this.addContent = function (info) {        
        console.clear();
        console.log(info);
        $scope.loading = false;        
        $scope.enableAddBtn();

        $scope.create_content_info_frm.insert_start_time.invalid = false;
        $scope.create_content_info_frm.insert_end_time.invalid = false;

         var insert_start_time = document.getElementById("insert_start_time");
         info.start_time = insert_start_time.value +" "+ info.addStartHour+":"+ info.addStartMinute+":"+ info.addStartSecond;

        var insert_end_time = document.getElementById("insert_end_time");
         info.end_time = insert_end_time.value +" "+ info.addEndHour+":"+ info.addEndMinute +":"+ info.addEndSecond;

        console.log('sTime='+insert_start_time.value+","+(insert_start_time.value !== ""));
        console.log('sHour='+info.addStartHour +","+(info.addStartHour !== undefined));
        console.log('sMinute='+info.addStartMinute +","+(info.addStartMinute !== undefined));
        console.log('sMinute='+info.addStartSecond +","+(info.addStartSecond !== undefined));
        console.log('eTime='+insert_end_time.value+","+(insert_end_time.value !== ""));
        console.log('sHour='+info.addEndHour +","+(info.addEndHour !== undefined));
        console.log('sMinute='+info.addEndMinute +","+(info.addEndMinute !== undefined));
        console.log('sMinute='+info.addEndSecond +","+(info.addEndSecond !== undefined));

        var start_time_lenth = document.getElementById("insert_start_time").value.length;
        var end_time_lenth = document.getElementById("insert_end_time").value.length;

        $scope.needSDate = false;
        $scope.needEDate = false;
        //判斷開始日期
        if ((insert_start_time.value !== "" || info.addStartHour !== undefined 
        || info.addStartMinute !== undefined || info.addStartSecond !== undefined)) {
            if ((insert_start_time.value !== "" && info.addStartHour !== undefined 
            && info.addStartMinute !== undefined && info.addStartSecond !== undefined)) {

            } else {
                alert("開始日期與開始時間必須都為空或都有值");                
                return ;
            }   
            if (start_time_lenth != 10) {
                $scope.create_content_info_frm.insert_start_time.invalid = true;
                alert("開始日期輸入有誤，格式為YYY-MM-DD");                
                return ;
            }            
            $scope.needSDate = true;
        }
        if ((insert_end_time.value !== "") || (info.addEndHour !== undefined) 
        || (info.addEndMinute !== undefined) || (info.addEndSecond !== undefined)) {
            if ((insert_end_time.value !== "" && info.addEndHour !== undefined
             && info.addEndMinute !== undefined && info.addEndSecond !== undefined)) {

            } else {
                alert("結束日期與結束時間必須都為空或都有值");                
                return ;
            }
            if (end_time_lenth != 10) {
                $scope.create_content_info_frm.insert_end_time.invalid
                alert("結束日期輸入有誤，格式為YYY-MM-DD");                
                return ;
            } 
            $scope.needEDate = true;
        }
        if (($scope.needSDate && !$scope.needEDate) || (!$scope.needSDate && $scope.needEDate)) {
            alert("開始日期與結束日期必須都為空或都有值");
            return ;
        }
        
        //判斷結束日期不能小於開始日期
        $scope.diff = $scope.diffDate(info.start_time, info.end_time);
        if ($scope.diff <= 0 && $scope.needSDate && $scope.needEDate) {
            alert("結束日期要大於開始日期");            
            return ;
        }
        
        info.status = "0";
        console.log('filePath=========' + info.file_path);
        console.log('filePath=========' + (info.file_path === undefined));
        if (info.file_path === undefined) {
            info.status = "0";
        } else {
            info.status = "1";
        }        

        
        info.company_id = localStorage.getItem('company_id');

        console.info("venver_id=" + info.vender_id);
        //rename file upload name=YYYYMMDD HHMMSS
        var { newFileName, fd } = getUploadMetaData("addFile");
        if (newFileName != "") {
            info.file_path = newFileName;
        }
        info.category_id = info.insert_category_id;
        info.subcategory_id = info.insert_subcategory_id;
        
        var file = document.getElementById("addFile").files;        
        if (file.length > 0) {
            console.log("原始檔名=" + file[0].name);
            info.file_name = file[0].name;
        }
        //info.file_path = newFileName;
        console.log('req' + JSON.stringify(info));
        console.log('req' + [info]);
        var insertContent = [info];
        console.log("file=" + info.file_path);
        console.log('req' + insertContent);
        if (newFileName !== "" && info.vender_id !== "") {
            $scope.disableAddFile = true;  
            
            $http({
                method: 'get',
                url: 'php/checkFtpConnect.php?vender_id=' + info.vender_id,
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (response) {
                console.log('checkFtpConnect=' + response.data.isSuccess); 
                if(response.data.isSuccess) {
                    $scope.isUpload = "檔案已上傳";
                    if (newFileName !== "" && info.vender_id !== "") {
                        $http({
                            method: 'POST',
                            url: 'php/apiContent_upload.php',
                            data: fd,
                            headers: { 'Content-Type': undefined },
                            transformRequest: angular.identity,
                            uploadEventHandlers: {
                                progress: function (e) {                                       
                                        if (e.lengthComputable) {
                                             $scope.progressBar = Math.round((e.loaded / e.total) * 100);
                                             $scope.progressCounter = $scope.progressBar;
                                             console.log('progress'+$scope.progressBar);
                                        }
                                }
                            }
                        }).then(function (response) {
                            console.log('upload success');
                            insertData($http, insertContent, vm);
                        });
                    } else {
                        insertData($http, insertContent, vm);
                    }                    
                } else {                    
                    $scope.addBtn = true;
                    $scope.loading = false;
                    alert('上傳失敗，請檢查內容商帳密及FTP資訊是否正確!!');
                    $scope.msg = '新增失敗，請檢查內容商帳密及FTP資訊是否正確!!!!';
                    $scope.alert_style = "alert-danger";
                    $scope.msg_area = true;
                }
            });

        } else {
            $scope.loading = false;
            insertData($http, insertContent, vm);
        }
    };
    
    function insertData($http, insertContent, vm) {
        console.log('insert data=' + JSON.stringify(insertContent));        
        $http({
            method: 'POST',
            url: 'php/apiContent.php',
            data: insertContent,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            $scope.msg = '新增成功!!';        
            $scope.loading = false;
            vm.loadData(1,'insert');
            document.getElementById('close-modal').click();
            
            $scope.msg = '新增成功!!';
            $scope.alert_style = "alert-success";
            $scope.msg_area = true;
        });
    }    

    this.get_content_info = function (id) {
        console.log('id==' + id);
        $http({
            method: 'get',
            url: 'php/apiContent.php?id=' + id + '&searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('view_content_info=' + JSON.stringify(response.data.data[0]));
            vm.view_content_info = response.data.data[0];
            vm.loadContentTag(id);
        });
    };


    this.delete_content_info = function (id) {
        bootbox.confirm("確定刪除嗎", function (confirm) {
            if (confirm) {
                var FormData = [{ id: id }];
                $http({
                    method: 'DELETE',
                    url: 'php/apiContent.php',
                    data: FormData,
                    headers: { 'CONTENT_TYPE': 'application/json' }
                }).then(function (response) {
                    vm.loadData($scope.currentPage,'delete');
                    $('.modal').modal('hide');
                });
            }
        });

    };

});





