myApp.controller('rss_controller', function ($scope, $state, $http, $location, $window,
     $crypto) {
    var vm = this;
    $scope.page_number = 10;

    $scope.rss_info = {};
    $scope.rss_info.company_id = "0";
    $scope.data = {};
    $scope.data.company_id = "0";
    $scope.optionDefault = { id: "0", company_name: "請選擇公司別" };

    this.load_company_data = function () {
        $http({
            method: 'get',
            url: 'php/apiCompany.php?searchTitle=&page=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('login company size=' + JSON.stringify(response.data));
            $scope.company_data = response.data.data;
            $scope.company_data.splice(0, 0, $scope.optionDefault);
        });
    };

    this.load_company_data();


    //初始主類別資料
    this.loadMainCategory = function () {

        $http({
            method: 'get',
            url: 'php/apiCategory.php?key=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('category data=' + JSON.stringify(response));
            $scope.catalogData = response.data.data;
            $scope.optionDefault = { category_id: "", name: "--請選擇--" };
            $scope.catalogData.splice(0, 0, $scope.optionDefault);
            $scope.category = "";

            $scope.subCatalogData = [{ subcategory_id: "", name: "--請選擇--" }];
            $scope.subcategory = $scope.subCatalogData[0];
            $scope.subcategory = "";
        });
    };

    //跟著主類別更動次類別
    $scope.insertChange = function (category) {
        console.log('category chging' + category);
        $scope.subCatalogData = [];
        $http({
            method: 'get',
            url: 'php/apiSubCategory.php?category_id=' + category + "&key=",
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // if (response && response.data.data > 0) {
            //     console.log('sub data size=' + JSON.stringify(response.data.data));
            $scope.subCatalogData = response.data.data;
            $scope.optionDefault = { id: "", name: "--請選擇--" };
            $scope.subCatalogData.splice(0, 0, $scope.optionDefault);
            $scope.subcategory = "";
            // }

        });

    };

    //初始內容商
    this.loadVender = function () {
        $http({
            method: 'get',
            url: 'php/apiVender.php?searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            //console.log('vender data1=' + JSON.stringify(response));
            $scope.venderData = response.data.data;
            $scope.optionDefault = { code: "", name: "--請選擇--" };
            $scope.venderData.splice(0, 0, $scope.optionDefault);
        });
    };

    this.search_data = function (search_input) {
        vm.loadData(1);
    };
    this.loadData = function (page_number) {             
        $http({
            method: 'get',
            url: 'php/getRssConfigList.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('RSS size=' + JSON.stringify(response));
            vm.content_list = response.data.data;
        });
    };

    this.edit_content_info = function (id) {
        $('#edit_info_modal').modal('show');
        $http.get('php/apiRss.php?id=' + id).then(function (response) {
            vm.rss_info = response.data.data[0];
            $scope.rss_info.company_id = response.data.data[0].company_id;
            console.log('rss data size=' + JSON.stringify(response.data));
        });
    };

    this.create_content_info = function () {
        $('#create_info_modal').modal('show');
        console.log('set company sesion=' + localStorage.getItem('company_id'));
        $scope.data.company_id = localStorage.getItem('company_id');
    };

    


    this.add = function (info) {
        //加密=company_id  +  token input text
        vm.eAES = $crypto.encrypt(localStorage.getItem('company_id')+info.token);
        vm.dAES = $crypto.decrypt(vm.eAES);
        console.log('eAES='+vm.eAES);
        console.log('dAES='+$crypto.decrypt(vm.eAES));
        info.token = vm.eAES;

        var FormData = [info];
        console.log('add rss data=' + JSON.stringify(FormData));
        $http({
            method: 'POST',
            url: 'php/apiRss.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(
            function (res) {
                vm.loadData(1);
                document.getElementById('close-modal1').click();
            }, function (res) {
                // Handle error here
                console.log('add rss err=' + JSON.stringify(res));
                console.log('status=' + res.status + ": " + res.statusText);
            }
        );

    };

    this.update = function () {
        var FormData = [this.rss_info];
        $http({
            method: 'PUT',
            url: 'php/apiRss.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('data=' + JSON.stringify(response));               
            document.getElementById('close-modal').click();
            vm.loadData($scope.currentPage);
        });
    };

    //vod發佈
    this.openLaunch = function (token) {
        $url = 'php/getLaunch.php?token=' + encodeURIComponent(token);
        $window.open($url, '_blank');
    }

    //VOD緊急下架時數
    this.openRecall = function (token) {
        console.log('token' + token.token);
        $url = 'php/getRecall.php?token=' + encodeURIComponent(token.token);
        $window.open($url, '_blank');
    }
  
    this.get_rss_info = function (id) {
        $http({
            method: 'get',
            url: 'php/apiContent.php?id=' + id + '&searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('view_content_info=' + JSON.stringify(response.data.data[0]));
            vm.view_rss_info = response.data.data[0];
        });
    };



    vm.loadData();
    vm.loadVender();
    vm.loadMainCategory();
    // vm.loadStatus();

});