myApp.controller('index_contrloer', function ($scope, $state, $http, $location) {
    $scope.functionList = [];
    $scope.info = {};
    $scope.index = [];  
    $scope.index.company_list = [];  
    $scope.index.company_id = "0";
    $scope.optionDefault = {id:"0", company_name:"請先切換公司別"};
    $scope.errmsg = "";

    unflatten = function (arr) {
        var tree = [],
            mappedArr = {},
            arrElem,
            mappedElem;
  
        // First map the nodes of the array to an object -> create a hash table.
        if (arr !=null) {
            for(var i = 0, len = arr.length; i < len; i++) {
                arrElem = arr[i];
                mappedArr[arrElem.id] = arrElem;
                mappedArr[arrElem.id]['children'] = [];
              }
        }
            
        for (var id in mappedArr) {
          if (mappedArr.hasOwnProperty(id)) {
            mappedElem = mappedArr[id];
            // If the element is not at the root level, add it to its parent array of children.
            console.log('pid='+(mappedElem.parent_id !== "0"));
            if (mappedElem.parent_id !== "0") {
                //console.log(typeof mappedArr[mappedElem['parent_id']]);
                mappedArr[mappedElem['parent_id']]['children'].push(mappedElem);                
            }
            // If the element is at the root level, add it to first level elements array.
            else {
              tree.push(mappedElem);
            }
          }
        }
        return tree;
      }

    $scope.set_company = function(id) {
        console.log(id);
        localStorage.setItem("company_id", id);  
        window.location.reload();        
    };    

    $scope.init = function () {                    
        console.log('index page');
        createCode();
        if (localStorage.getItem('company_id') == "0") {
            alert('請先切換公司別');
            $scope.errmsg = "查無資料，請先選擇公司別!!";
        } 
        // $scope.index.company_list = localStorage.getItem("company_list");
        $http({
            method:'get',
            url:'php/apiCompany.php?searchTitle=&page=',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {              
            //console.log('company size=' + JSON.stringify(response.data)); 
            $scope.index.company_list = response.data.data;            
            $scope.index.company_list.splice(0, 0, $scope.optionDefault);            
            $scope.index.company_id = localStorage.getItem('company_id');
            localStorage.setItem("company_list", response.data.data);  
        });

        if (localStorage.getItem('account') != null) {    
            console.log('set company sesion='+ localStorage.getItem('company_id'));
            $scope.functionList = JSON.parse(localStorage.getItem('functionList'));      
            //console.log('func list='+ JSON.stringify(localStorage.getItem('functionList')));  
            // var tree = unflatten($scope.functionList);
            // console.log('tree=' + JSON.stringify(tree));
           
           if (localStorage.getItem('functionList') !== null) {
                $scope.functionList = unflatten(JSON.parse(localStorage.getItem('functionList')));
           }
           

            $scope.account = localStorage.getItem('account');
            $scope.company = localStorage.getItem('company');
            $scope.company_id = localStorage.getItem('company_id');
            $scope.loginAction = "登出";            

            $scope.info.account = $scope.account;
            $scope.info.company = $scope.company;
            $scope.info.company_id = $scope.company_id;
            // $scope.info.vender_list = JSON.parse(localStorage.getItem('vender_list'));            
            $scope.info.vender_list = localStorage.getItem('vender_list');
            $scope.info.account_id = localStorage.getItem('account_id');
            $scope.info.super_user = localStorage.getItem('super_user');            
            $scope.info.admin = localStorage.getItem('admin');            
            $scope.admin = "";
            if (localStorage.getItem('admin')!='null') {
                $scope.admin = "管理者";
            }
            var FormData = [$scope.info];            
            //console.log('index set session data=' + JSON.stringify(this.info));
            $http({
                method:'PUT',
                url:'php/indexSession.php',
                data: FormData,
                headers:{'CONTENT_TYPE': 'application/json'}           
            }).then(function (response) {
                console.log('finish set company session');
            });  
        } else {            
            $scope.loginAction = "登入";
            $scope.functionList = "";
            // localStorage.removeItem('account');
            // localStorage.removeItem('password');
            // localStorage.removeItem('functionList');
            $state.go('login'); 
            // window.location.reload();
        }  
          
    };   

    $scope.init();

    
        // var delay = 1000, setTimeoutConst;
        
        // var lastTime = new Date().getTime();
        // var currentTime = new Date().getTime();
        // var timeOut = 5 * 60 * 1000; //設定超時時間: 1分

    // setTimeout(()=>{    //<<<---    using ()=> syntax
    //     console.log('ttt');
    //     var deleteRows = [];
    //     for (var i = 0; i < localStorage.length; i++) {
    //         var key = localStorage.key(i);
    //         var partsArray = key.split('-');
    //         // The last value will be a timestamp
    //         var lastRow = partsArray[partsArray.length - 1];

    //         // if (new Date().getTime() > localStorage.getItem('expiryTime')) {
    //             console.log('timeout>>>>>>');
    //             window.location.reload();
    //             deleteRows.push(key);
    //             $state.go('logout');
    //         // }
    //     }
    //     for (var j = 0; j < deleteRows.length; j++) {
    //         localStorage.removeItem(deleteRows[j]);
    //     }
    //     // console.log('timeout' + (new Date().getTime()) +"," +localStorage.getItem('expiryTime'));
    //     // console.log('timeout' + (new Date().getTime() - localStorage.getItem('expiryTime')));
    // }, 1 * 60 * 1000); //1000 second

   
    $scope.getAccount = function () { 
        return localStorage.getItem('account');
    }
    $scope.logout = function () {
        console.log('logout,account'+ localStorage.getItem('account'));
        if (localStorage.getItem('account') != null) {
            //localStorage.clear;
            localStorage.removeItem('account');
            localStorage.removeItem('password');
            localStorage.removeItem('functionList');        
            //localStorage.removeItem('company_list');                            
            $state.go('login');            
            window.location.reload();   
                        
        }        
    }
    $scope.logoutShowMessage = function () {
        console.log('logout,account'+ localStorage.getItem('account'));
        if (localStorage.getItem('account') != null) {
            //localStorage.clear;
            localStorage.removeItem('account');
            localStorage.removeItem('password');
            localStorage.removeItem('functionList');        
            //localStorage.removeItem('company_list');                    
            alert("操作已閒置超過一小時，已自動登出系統");
            $state.go('login');            
            window.location.reload();   
                        
        }        
    }
});
