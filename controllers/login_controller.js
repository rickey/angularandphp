myApp.controller('login_controller', function ($scope, $state, $http, $location, $rootScope) {
    var vm = this;
    $scope.inputData = [];
    $scope.inputData.company_id = "0";
    $scope.optionDefault = { id: "0", company_name: "請先切換公司別" };
    console.log('login page');
    // localStorage.setItem("loginCount", parseInt(0));//for test
    $scope.init = function () {
        console.log('login count===' + localStorage.getItem("loginCount"));
        if (parseInt(localStorage.getItem("loginCount")) > 2) {
            $scope.disableLogin = true;
        } else {
            $scope.disableLogin = false;
        }
        $scope.company_data = localStorage.getItem('company_list');
        // console.log('account=' + localStorage.getItem('account'));        
        if (localStorage.getItem('account') != null) {
            console.log('enter');
            $state.go('default');
        }
    };

    this.load_company_data = function () {
        $http({
            method: 'get',
            url: 'php/apiCompany.php?searchTitle=&page=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('login company size=' + JSON.stringify(response.data));
            $scope.company_data = response.data.data;
            $scope.company_data.splice(0, 0, $scope.optionDefault);
            localStorage.setItem("company_list", response.data.data);
        });
        console.log('account=' + localStorage.getItem('account'));
        if (localStorage.getItem('account') == null) {
            var FormData = [$scope.info];
            console.log('ckear session');
            $http({
                method: 'PUT',
                url: 'php/clearSession.php',
                data: FormData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (response) {
                console.log('finish clear session');
            });
        }
    };

    this.load_company_data();
    $scope.init();

    function countLoginFail(inputData) {
        return $http({
            method: 'get',
            url: 'php/addAccountCount.php?searchTitle=&name=' + inputData.username + '&pass=' + inputData.password + "&company_id=" + inputData.company_id,
            data: FormData,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json'
            }
        }).then(function (response) {
            console.log('account size=' + JSON.stringify(response));
            console.log('response code=' + response.data.code);
            if (response && response.data.code == 200) {
                localStorage.setItem("loginCount", response.data.login_count);
                 alert('帳號或密碼或公司別登入失敗, 累計登入次數：' + response.data.login_count);
                 if (response.data.login_count > 3) {
                    $scope.disableLogin = true;
                }
            }
        });
    }

    this.postForm = function (inputData) {
        if (inputData.company_id == 0) {
            alert('公司別不能為空');
            return '';
        }
        
        if (parseInt(localStorage.getItem("loginCount")) > 2) {
            $scope.disableLogin = true;
            $('#showMessage').modal('show');
        }
        console.log('loginCount=' + localStorage.getItem("loginCount"));
        var numValidate = validate();
        console.log('num validate=' + numValidate);
        if (numValidate != 2) {
            if (numValidate == 0) {
                alert('請輸入驗証碼！');
            } else if (numValidate == 1) {
                alert('驗証碼錯誤');
            }
            return '';
        }
        console.log('login' + inputData.company_id);        

        if (localStorage.getItem('account') != null) {
            console.log('enter');
            $state.go('default');
        } else {
            $http({
                method: 'get',
                url: 'php/apiAccount.php?searchTitle=&name=' + inputData.username + '&pass=' + inputData.password + "&company_id=" + inputData.company_id,
                data: FormData,
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Accept': 'application/json'
                }
            }).then(function (response) {
                console.log('account size=' + JSON.stringify(response));
                console.log('size=' + response.data.data.length);
                if (response && response.data.data.length > 0) {
                    localStorage.setItem("account", inputData.username);
                    localStorage.setItem("password", inputData.password);
                    localStorage.setItem("company", response.data.data[0]["company_name"]);
                    localStorage.setItem("company_id", response.data.data[0]["company_id"]);
                    localStorage.setItem("super_user", response.data.data[0]["super_user"]);
                    console.log('admin' + response.data.data[0]["admin"])
                    if (response.data.data[0]["admin"] === "1") {
                        console.log('admin');
                        localStorage.setItem("admin", response.data.data[0]["admin"]);
                    } else {
                        console.log('not admin');
                        localStorage.setItem("admin", null);
                    }
                    localStorage.setItem("account_id", response.data.data[0]["id"]);

                    console.log("login id=" + response.data.data[0]["id"]) + ',' + response.data.data[0]["company"];
                    //取得role_funclist by account
                    var FormData = { account_id: response.data.data[0]["id"] };
                    console.log('FormData=' + JSON.stringify(FormData));
                    $http({
                        method: 'get',
                        url: 'php/getAccountFunctionList.php?account_id=' + response.data.data[0]["id"],
                        data: FormData,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function (funcData) {
                        // localStorage.setItem("loginCount", parseInt(0));
                        $scope.functionList = [];
                        console.log('func rtn data=' + JSON.stringify(funcData.data.data));
                        localStorage.setItem("functionList", JSON.stringify(funcData.data.data));
                        // window.location.reload();
                        // $state.go('default')                        
                    });

                    //取得group_vender by account
                    $http({
                        method: 'get',
                        url: 'php/getAccountVenderList.php?account_id=' + response.data.data[0]["id"],
                        data: FormData,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function (venderData) {
                        $scope.venderList = [];
                        console.log('vender rtn data=' + JSON.stringify(venderData.data.data));
                        localStorage.setItem("vender_list", JSON.stringify(venderData.data.data));
                        window.location.reload();
                        $state.go('default')

                    });
                } else {
                    //登入失敗時累計次數by comp_id +account
                    countLoginFail(inputData);
                }
            });
        }
    }


});