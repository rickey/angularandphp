myApp.controller('role_controller', function ($scope, $state, $http, $location) {
    var vm = this;      
    $scope.selectedRow = null;
    $scope.currentPage = 1;

    this.search_data = function () {
        vm.loadData(1);
    }; 

    this.loadData = function (page_number) {        
        $http({
            method:'get',
            url:'php/apiRole.php?searchTitle='+$scope.searchTitle + "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('main size=' + JSON.stringify(response)); 
            vm.role_list = response.data.data;
            $scope.total_row = response.data.total;
        });
    };

    this.loadData(1);

    this.selectContent = function (info) {                
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };
    
     //初始帳號資料
     this.initAccountData = function () {
        $scope.accountData = [];
        $http({
            method: 'get',
            url:'php/getAccountList.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('accountData=' + JSON.stringify(response.data.data));
            $scope.accountData = response.data.data;            
        });
    };  

    //初始功能資料
    this.initFunctionData = function () {
        $scope.accountData = [];
        $http({
            method: 'get',
            url:'php/getFunctionList.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('tag data=' + JSON.stringify(response.data.data));
            $scope.functionData = response.data.data;            
        });
    }; 
    
    //初始帳號及功能資料
    this.init_add_info = function () {
        this.initAccountData();
        this.initFunctionData();
    };

    //新增時限制只能從帳號選取
    $scope.accounts = [];
    $scope.onTagAdded = function($tag) {
        console.log('onTagAdded==='+$tag.id);
        if ($tag.id === undefined) {   
            if ($scope.accounts != undefined) {
                $scope.accounts.splice($scope.accounts.indexOf($tag), 1);
            }   
            if ($scope.edit_accounts != undefined) {
                $scope.edit_accounts.splice($scope.edit_accounts.indexOf($tag), 1);
            }                         
         }
    };   

    // //編輯時限制只能從帳號選取
    // $scope.edit_accounts = [];
    // this.AddEditAccount = function($tag) {
    //     console.log('edit---');
    //     console.log('edit---'+$tag.id);
    //     if ($tag.id == undefined) {      
    //         vm.group_ctrl.edit_accounts.splice(vm.group_ctrl.edit_accounts.indexOf($tag), 1);;
    //      }
    // };   
    
    //限制只能從功能清單選取
    $scope.functionlists = [];
    $scope.onFunctionAdded = function($tag) {
        console.log('add function list='+$tag.id);
        if ($tag.id === undefined) {     
            if ($scope.functionlists != undefined) { 
                $scope.functionlists.splice($scope.functionlists.indexOf($tag), 1);
            }
            if ($scope.edit_functions != undefined) { 
                $scope.edit_functions.splice($scope.edit_functions.indexOf($tag), 1); 
            }                       
         }
    };     

    this.add = function (info) {  
        info.company_id = localStorage.getItem('company_id');      
        info.accounts = $scope.accounts;
        info.functionlist = $scope.functionlist;
        console.log('add data=' + JSON.stringify(info));
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiRole.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            vm.loadData(1); 
            document.getElementById('close-modal1').click();          
        });
    };

    //編輯時帶出role_account資料
    this.loadRoleAccount = function (id) {
        $http({
            method: 'get',
            url: 'php/apiRoleAccount.php?id=' + id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('edit account data=' + JSON.stringify(response));
            $scope.edit_accounts = response.data.data;
            // $scope.view_tags = response.data.data;
        });
    };   

    //編輯時帶出role_function資料
    this.loadRoleFuntionList = function (id) {
        $http({
            method: 'get',
            url: 'php/apiRoleFunctionList.php?id=' + id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('content tag data=' + JSON.stringify(response));
            $scope.edit_functions = response.data.data;
            // $scope.view_tags = response.data.data;
        });
    };   

    //編輯時帶出role/role_account/role_fucntion資料
    this.init_edit_info = function (id) {
        $('#edit_info_modal').modal('show');
        this.loadRoleAccount(id);
        this.loadRoleFuntionList(id);
        console.log('edit id='+id);
        $http({
            method:'get',
            url:'php/apiRole.php?id='+id+'&searchTitle=',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log(' edit_info data=' + JSON.stringify(response.data.data[0])); 
            vm.role_info = response.data.data[0];                                 
        });        

        // $http.get('php/content/selectone.php?student_id=' + student_id).then(function (response) {
        //     vm.content_info = response.data;
        // });
    };

    //按更新
    this.update = function () { 
        this.role_info.accounts = $scope.edit_accounts;
        this.role_info.functionlists = $scope.edit_functions;
        var FormData = [this.role_info];
        console.log('update data=' + JSON.stringify(this.role_info));
        $http({
            method:'PUT',
            url:'php/apiRole.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
        }).then(function (response) {
            // console.log('data=' + JSON.stringify(response));               
            document.getElementById('close-modal2').click();
            vm.loadData($scope.currentPage);  
        });  
    };

    this.delete = function (id) {
        bootbox.confirm("確定刪除嗎", function(confirm) {
            if (confirm) { 
                var FormData = [{id:id}];
                $http({
                    method:'DELETE',
                    url:'php/apiRole.php',
                    data: FormData,
                    headers:{'CONTENT_TYPE': 'application/json'}           
                  }).then(function (response) {
                    vm.loadData($scope.currentPage);
                    $('.modal').modal('hide');          
                });
            }
        });
    };
    
    $scope.$watch('currentPage + numPerPage', function () {
        vm.loadData($scope.currentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });
});