myApp.controller('vender_controller', function ($scope, $state, $http, $location) {
    var vm = this; 
    $scope.selectedRow = null;
    $scope.currentPage = 1;
    $scope.vender = {};
    $scope.vender.pass_audit = "0";
     
    this.search_data = function (search_input) {
        vm.loadData(1);
    }; 
    this.loadData = function (page_number) {        
        $http({
            method:'get',
            url:'php/apiVender.php?searchTitle='+$scope.searchTitle + "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('main size=' + JSON.parse(JSON.stringify(response))); 
            vm.vender_list = response.data.data;
            $scope.total_row = response.data.total; 
        });
    };

    //單擊時，列變顏色
    this.selectContent = function (info) {        
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };

    // this.edit_category_info = function (id) {
    //     $('#edit_info_modal').modal('show');
    //     $http.get('php/maincategory/selectone.php?id=' + id).then(function (response) {
    //         vm.category_info = response.data;
    //         console.log('main data size=' + JSON.stringify(response.data)); 
    //     });
    // };

    $scope.$watch('currentPage + numPerPage', function () {
        vm.loadData($scope.currentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });

    this.loadData(1);


    this.add_data = function (info) {   
        info.company_id = localStorage.getItem('company_id');     
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiVender.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            vm.loadData(1); 
            document.getElementById('close-modal1').click();          
        });
    };

    this.edit_info = function (id) {
        $('#edit_info_modal').modal('show');
        console.log('edit id='+id);
        $http({
            method:'get',
            url:'php/apiVender.php?id='+id+'&searchTitle=',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('data=' + JSON.stringify(response.data.data[0])); 
            vm.vender_info = response.data.data[0];                                 
        });        

        // $http.get('php/content/selectone.php?student_id=' + student_id).then(function (response) {
        //     vm.content_info = response.data;
        // });
    };

    this.update = function () { 
        var FormData = [this.vender_info];
        $http({
            method:'PUT',
            url:'php/apiVender.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
        }).then(function (response) {
            // console.log('data=' + JSON.stringify(response));               
            document.getElementById('close-modal2').click();
            vm.loadData($scope.currentPage);  
        });  
    };

    this.delete = function (id) {
        bootbox.confirm("確定刪除嗎", function(confirm) {
            if (confirm) { 
                var FormData = [{id:id}];
                $http({
                    method:'DELETE',
                    url:'php/apiVender.php',
                    data: FormData,
                    headers:{'CONTENT_TYPE': 'application/json'}           
                  }).then(function (response) {
                    vm.loadData($scope.currentPage);
                    $('.modal').modal('hide');          
                });
            }
        });
    };
});