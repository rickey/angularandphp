myApp.controller('epg_upload_controller', function ($rootScope, $scope, $state, $http, $location, FileUploader) {
    var vm = this;

    //按編輯帶出視窗
    this.edit_content_info = function (info) {
        $('#edit_content_info_modal').modal('show');
        console.log('edit id=' + JSON.stringify(info));
        vm.content_info = info;
        //$scope.resetMsg();
    };

    //編輯頁按更新
    this.updateContent = function () {
        //帶回頁面顯示文字               
        var vender_name = document.getElementById("vender_id");
        vm.content_info.vender_name = vender_name.options[vender_name.selectedIndex].text;
        vm.content_info.tags = $scope.edit_tags;
    };

    //load live id/title
    this.loadLive = function (page_number) {
        $http({
            method: 'get',
            url: 'php/apiLiveContent.php?searchTitle=' + $scope.searchTitle + "&page=" + page_number,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('live data=' + JSON.stringify(response));
            $scope.liveData = response.data.data;
        });
    };

    vm.loadLive(1);

    $scope.fileNameChaged = function () {
        console.log('file is ');
    };
    $scope.onInputChange = function () {
        $scope.resetMsg();
        console.log("input change");
    };

    $scope.resetMsg = function () {
        $scope.showMsg = false;
        $scope.msg = "";
    };

    //批次儲存
    $scope.batchSave = function () {
        $scope.msg = "";
        console.log('batchSave' + JSON.stringify($scope.content_list));
        var num = 1;
        angular.forEach($scope.content_list, function (content) {
            
            $http({
                method: 'get',
                url: 'php/getEpg.php?text=' + content.text + '&live_id=' + content.live_id,
                data: '',
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (queryRes) {                               
                console.log('chk exit' + (queryRes.data.data.length == 1) + "," + JSON.stringify(queryRes.data.data));
                if (content.live_id == "" || content.text == "") { //live_id + text必填
                    console.log('頻道或標題不能為空!!');
                    $scope.msg += '第' + content.id + '筆, 頻道或標題不能為空 ;' + ' \n\r ';
                } else {
                    if (queryRes.data.data.length == 1) {//update     
                        $scope.msg += '第' +  content.id  + '筆, 批次更新完成 ;' + ' \n\r ';
                        console.log('id=' + queryRes.data.data[0].id);
                        content.id = queryRes.data.data[0].id;
                        var insertData = [content];
                        $http({
                            method: 'PUT',
                            url: 'php/apiLiveEpg.php',
                            data: insertData,
                            headers: { 'CONTENT_TYPE': 'application/json' }
                        }).then(function (updRes) {
                            console.log('更新完成');                            
                        });
                    } else {//insert
                        $scope.msg += '第' +  content.id  + '筆, 批次新增完成 ;' + ' \n\r ';
                        content.id = null;
                        var insertData = [content];
                        $http({
                            method: 'POST',
                            url: 'php/apiLiveEpg.php',
                            data: insertData,
                            headers: { 'CONTENT_TYPE': 'application/json' }
                        }).then(function (insertRes) {
                            console.log('新增完成');                            
                        });
                    }

                }

            });
        });
        $scope.showMsg = true;
        $scope.content_list = "";
        $("#file").val('');
    };

    $scope.clearUpload = function () {
        $scope.content_list = "";
        $("#file").val('');
        $scope.msg = "";
    };

    this.delete_content_info = function (info) {
        bootbox.confirm("確定刪除嗎" + JSON.stringify(info), function (confirm) {
            if (confirm) {
                var idx = $scope.content_list.indexOf(info);
                if (idx > -1) {// make sure can index this object                    
                    $rootScope.$apply(function () {
                        $scope.content_list.splice(idx, 1);
                        console.log('Successfully deleted ' + $scope.content_list);
                    });
                }
            }
        });
    };

    $scope.uploadSimple = function () {
        $scope.resetMsg();
        var file = document.getElementById('file').files;
        console.log('file is ');
        console.dir(file[0]);

        var fd = new FormData();
        fd.append('file', file[0]);
        fd.append('MAX_FILE_SIZE', 8 * 1024 * 1024);

        $http({
            method: 'post',
            url: 'php/epg_upload.php',//importContent
            data: fd,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).then(function (response) {
            console.log('upload success' + JSON.stringify(response.data.data));
            // console.log('upload err msg' + JSON.stringify(response.data.errCategory));            
            if (response.data.errChannel != undefined) {
                $scope.msg += response.data.errChannel + "  ; \n\r ";
                $scope.showMsg = true;
            }
            $scope.content_list = response.data.data;
        });
    };

    this.selectContent = function (info) {
        // console.log('this id=' + info);
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };

    $scope.uploadFile = function () {
        $scope.onFileSelect($scope.files);
    };

    $scope.onSelect = function ($files) {
        $scope.noFile = false;
        $scope.files = $files;
    };

    $scope.onFileSelect = function ($files) {
        var $file = $files[0];
        console.log('file=');
    };
});