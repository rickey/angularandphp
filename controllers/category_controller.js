myApp.controller('category_controller', function ($scope, $state, $http, $location) {
    var vm = this;
    $scope.currentPage = 1;
    $scope.subCurrentPage = 1;
    $scope.maxSize = 3;
    
    this.search_data = function (compCatalog) {
        vm.loadData(1);
    };  
    
    // this.search_sub_data = function (compCatalog) {
    //     vm.loadSubData(1);
    // };   

    this.loadData = function (page_number) {        
        $http({
            method:'get',
            url:'php/apiCategory.php?key='+$scope.compCatalog + "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            // console.log('main size=' + JSON.stringify(response)); 
            vm.category_list = response.data.data;            
            // console.log('catalogData====='+ JSON.stringify(vm.catalogData));         
            // console.log('total='+ response.data.total);
            $scope.total_row = response.data.total;           
        });
    };

    this.loadSubData = function (page_number) { 
        console.log('start loading subData'+page_number);
        $http({
            method:'get',
            url:'php/apiSubCategory.php?key='+$scope.compSubCatalog+"&category_id="+$scope.category_id+ "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            // console.log('sub size=' + JSON.stringify(response)); 
            vm.sub_category_list = response.data.data;
            $scope.sub_total_row = response.data.total; 
            
            vm.loadMainCategory(); 
        });
    };

    this.loadMainCategory = function () {        
        $http({
            method:'get',
            url:'php/apiCategory.php?key=',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            // console.log('category data=' + JSON.stringify(response)); 
            $scope.catalogData = response.data.data;
        });
    };    

    $scope.init = function () {
        // console.log('start init Data'+ $scope.currentPage +"," +$scope.subCurrentPage);
        vm.loadData($scope.currentPage);
        vm.loadSubData($scope.subCurrentPage);
    };
    
    $scope.init();

    $scope.$watch('currentPage + numPerPage', function () {
        vm.loadData($scope.currentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });

    //次分類點頁次
    $scope.$watch('subCurrentPage + numPerPage', function () {
        vm.loadSubData($scope.subCurrentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });

    //瀏覽主分類詳細資料
    this.get_category_info = function (id) {
        console.log('id='+id);
        $http.get('php/maincategory/selectone.php?id=' + id).then(function (response) {            
            vm.view_category_info = response.data;
            console.log('data=' + response.data);
        });
    };  

    this.edit_category_info = function (id) {
        $('#edit_category_info_modal').modal('show');        
        $http.get('php/apiCategory.php?id=' + id).then(function (response) {
            vm.category_info = response.data.data[0];  
            console.log('main data size=' + JSON.stringify(response.data.data[0])); 
        });
    };

    //新增次分類
    this.initSubCategoryData =  function() {        
        $('#create_sub_category_info_modal').modal('show');
        console.log('initSubCategoryData');
        vm.loadMainCategory(); 
    };

    //編輯次分類查詢畫面
    this.edit_sub_category_info = function (id) {        
        vm.loadMainCategory(); 
        $('#edit_sub_category_info_modal').modal('show');
        $http({
            method:'get',
            url:'php/apiSubCategory.php?id='+id,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('sub data size=' + JSON.stringify(response.data.data)); 
            vm.sub_category_info = response.data.data[0];                       
        });     
    };

    //單擊主類別
    this.selectContent = function (info) {        
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };

    //單擊次類別
    this.selectSubCategory = function (info) {        
        if ($scope.selectSubCategory !== null) {
            $scope.selectedSubRow = info.id;
        }
    };
   
    //編輯次分類欄位
    this.updateSubCategory = function () {
        var FormData = [this.sub_category_info];
        $http({
            method:'PUT',
            url:'php/apiSubCategory.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
        }).then(function (response) {
            console.log('add size=' + JSON.stringify(response)); 
            vm.loadSubData($scope.currentPage);     
            document.getElementById('close-modal4').click();
        });
  
};

    this.updateCategory = function () {
            var FormData = [this.category_info];
            $http({
                method:'PUT',
                url:'php/apiCategory.php',
                data: FormData,
                headers:{'CONTENT_TYPE': 'application/json'}           
            }).then(function (response) {
                console.log('add size=' + JSON.stringify(response)); 
                vm.loadData($scope.currentPage);     
                document.getElementById('close-modal2').click();
            });
      
    };

    this.delete_category_info = function (id) {
        bootbox.confirm("確定刪除嗎", function(confirm) {
            if (confirm) { 
                var FormData = [{id:id}];
                $http({
                    method:'DELETE',
                    url:'php/apiCategory.php',
                    data: FormData,
                    headers:{'CONTENT_TYPE': 'application/json'}           
                  }).then(function (response) {
                    vm.loadData($scope.currentPage);
                    $('.modal').modal('hide');          
                });
            }
        });         
    };

    this.delete_sub_category_info = function (id) {
        bootbox.confirm("確定刪除次分類資料嗎", function(confirm) {
            if (confirm) { 
                var FormData = [{id:id}];
                $http({
                    method:'DELETE',
                    url:'php/apiSubCategory.php',
                    data: FormData,
                    headers:{'CONTENT_TYPE': 'application/json'}           
                  }).then(function (response) {
                    vm.loadSubData($scope.currentPage);
                    $('.modal').modal('hide');          
                });
            }
        });         
    };

    this.addCategory = function (info) {
        // console.log('form='+ JSON.stringify(info)+",name="+info.name);
        info.company_id = localStorage.getItem('company_id');
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiCategory.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            vm.loadData(1); 
            document.getElementById('close-modal1').click();          
        });
    };

    this.addSubCategory = function (info) { 
        info.company_id = localStorage.getItem('company_id');;       
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiSubCategory.php',
            data: FormData,
            headers:{ 'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Accept': 'application/json'}           
          }).then(function (response) {
            vm.loadSubData(1);
            document.getElementById('close-modal3').click();        
        });
    };
   
});