myApp.controller('default_controller', function ($scope, $state, $http, $location) {
    var vm = this;  
    $scope.searchTitle = "";
    $scope.selectedRow = null;
    $scope.currentPage = 1;

    $scope.init = function () {     
        console.log('default page =>'+ localStorage.getItem('account') +localStorage.getItem('company') +'com_id='+localStorage.getItem('company_id'));
        if (localStorage.getItem('account') != null) {
            $scope.functionList = JSON.parse(localStorage.getItem('functionList'));            
            $scope.loginAction = "登出";
            // $state.go('login');           
        } else {
            $scope.loginAction = "登入";
            $state.go('login');
        }       
    };   

    $scope.init();

  
    //登出使用
    this.postForm = function (inputData) {        
        localStorage.removeItem('account');
        localStorage.removeItem('password');
        localStorage.removeItem('functionList');
        $state.go('login'); 
    }


    $scope.$watch('currentPage + numPerPage', function () {        
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });
});