var code; //全域驗証碼變數   
window.onload = function () {
	createCode();
}

function createCode() {
	code = "";
	var codeLength = 4; //驗証碼長度   
	var checkCode = document.getElementById("checkCode");
	var random = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
		'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
	for (var i = 0; i < codeLength; i++) {
		var charIndex = Math.floor(Math.random() * 36); //取得随机数的索引   
		code += random[charIndex]; //根据索引取得随机数加到code上   
	}
	try {
		checkCode.value = code; //把code值赋给验证码   
	} catch (err) {
		//非login畫面時，產生null，不需處理
	}
}
//驗証
function validate() {
	var inputCode = document.getElementById("input").value.toUpperCase(); //取得輸入並轉為大寫
	if (inputCode.length <= 0) { //驗証碼長度為0沒有輸入
		// alert("請輸入驗証碼！");
		return 0;
	} else if (inputCode != code) { //驗証失敗
		// alert("驗証碼錯誤！");
		createCode(); //更新驗証碼
		return 1;
	} else { //正確
		// alert("ok"); 
		return 2;
	}
}