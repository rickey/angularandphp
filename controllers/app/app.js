var myApp = angular.module('ott2b', ['ui.bootstrap.datetimepicker', 'ui.router', 'ui.bootstrap',
    'file-uploader', 'ngTagsInput', 'mdo-angular-cryptography','ui.bootstrap']);

myApp.directive('script', function () {
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, elem, attr) {
            if (attr.type == 'text/javascript-lazy') {
                var code = elem.text();
                var f = new Function(code);
                f();
            }
        }
    };
});

myApp.directive('loading', function () {
    return {
      restrict: 'E',
      replace:true,
      template: '<div class="loading" style="line-height:600px;text-align:center;border:2px #00F0F0 solid;" >LOADING........<img src="./img/ajax-loader.gif" width="30" height="30" /><progressbar max="100" value="progressCounter"><span style="color:black; white-space:nowrap;"></span></progressbar><div style="position:absolute;top:20%;text-align:center; left:50%; ">{{progressCounter}}%</div></div>',
      link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val)
                    $(element).show();
                else
                    $(element).hide();
            });
      }
    }
});

myApp.directive("filesInput", function () {
    return {
        require: "ngModel",
        link: function postLink(scope, elem, attrs, ngModel) {
            elem.on("change", function (e) {
                var files = elem[0].files;
                ngModel.$setViewValue(files);
            })
        }
    }
});


myApp.config(function ($stateProvider, $locationProvider, $urlRouterProvider, $cryptoProvider) {
    $cryptoProvider.setCryptographyKey('1qaz@WSX');

    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('default', {
            url: '/',
            templateUrl: 'templates/default.html',
            controller: 'default_controller',
            controllerAs: "default_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "default Content";
                }]
            }
        })
        .state('company', {
            url: '/',
            templateUrl: 'templates/company.html',
            controller: 'company_controller',
            controllerAs: "company_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "default Company";
                }]
            }
        })
        .state('login', {
            url: '/',
            templateUrl: 'templates/login.html',
            controller: 'login_controller',
            controllerAs: "login_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "Login Content";
                }]
            }
        })
        .state('public', {
            url: '/',
            templateUrl: 'templates/content/list.html',
            controller: 'content_controller',
            controllerAs: "content_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "VOD Content";
                }]
            }

        })
        .state('home', {
            url: '/',
            templateUrl: 'templates/category.html',
            controller: 'category_controller',
            controllerAs: "cat_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "Category";
                }]
            }
        })
        .state('vender', {
            url: '/',
            templateUrl: 'templates/vender/list.html',
            controller: 'vender_controller',
            controllerAs: "vender_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "vender";
                }]
            }
        })
        .state('group', {
            url: '/',
            templateUrl: 'templates/group.html',
            controller: 'group_controller',
            controllerAs: "group_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "group";
                }]
            }
        })
        .state('role', {
            url: '/',
            templateUrl: 'templates/role.html',
            controller: 'role_controller',
            controllerAs: "role_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "Category";
                }]
            }
        })
        .state('funclist', {
            url: '/',
            templateUrl: 'templates/funclist.html',
            controller: 'func_controller',
            controllerAs: "func_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "Category";
                }]
            }
        })
        .state('upload', {
            url: '/',
            templateUrl: 'templates/upload/vod_upload.html',
            controller: 'uploadContent_controller',
            controllerAs: "upd_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "Upload Content";
                }]
            }
        })
        .state('epg_upload', {
            url: '/',
            templateUrl: 'templates/upload/epg_upload.html',
            controller: 'epg_upload_controller',
            controllerAs: "epg_upload_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "Upload Content";
                }]
            }
        })
        .state('rss', {
            url: '/',
            templateUrl: 'templates/rss/list.html',
            controller: 'rss_controller',
            controllerAs: "rss_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "RSS Content";
                }]
            }
        })
        .state('liverss', {
            url: '/',
            templateUrl: 'templates/live/list.html',
            controller: 'liverss_controller',
            controllerAs: "liverss_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "LIVE RSS Content";
                }]
            }
        })
        .state('live_content', {
            url: '/',
            templateUrl: 'templates/live/live_content.html',
            controller: 'live_content_controller',
            controllerAs: "live_content_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "LIVE content Content";
                }]
            }
        })
        .state('live_epg', {
            url: '/',
            templateUrl: 'templates/live/live_epg.html',
            controller: 'live_epg_controller',
            controllerAs: "live_epg_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "LIVE EPG Content";
                }]
            }
        })
        .state('account', {
            url: '/',
            templateUrl: 'templates/account.html',
            controller: 'account_controller',
            controllerAs: "account_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                    $rootScope.title = "account Content";
                }]
            }
        })
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });




});


