myApp.controller('uploadContent_controller', function ($rootScope, $scope, $state, $http, $location, FileUploader) {
    var vm = this;
    $scope.disableUpload = true;
    //初始tag資料
    this.loadTag = function (query) {
        $scope.tagData = [];
        $http({
            method: 'get',
            url: 'php/apiTag.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('tag data=' + JSON.stringify(response.data.data));
            $scope.tagData = response.data.data;
        });
    };

    //按編輯帶出視窗
    this.edit_content_info = function (info) {
        $('#edit_content_info_modal').modal('show');
        $scope.editChange(info.category_id, info);
        // this.loadTag();
        console.log('edit id=' + JSON.stringify(info));
        $scope.edit_tags = info.tags;
        vm.content_info = info;
        //$scope.resetMsg();
    };

    //編輯頁按更新
    this.updateContent = function () {
        //帶回頁面顯示文字       
        var category_id = document.getElementById("category_id");
        vm.content_info.category_name = category_id.options[category_id.selectedIndex].text;
        var subcategory_id = document.getElementById("subcategory_id");
        vm.content_info.subcategory_name = subcategory_id.options[subcategory_id.selectedIndex].text;
        var vender_name = document.getElementById("vender_id");
        vm.content_info.vender_name = vender_name.options[vender_name.selectedIndex].text;
        vm.content_info.tags = $scope.edit_tags;
    };

    //初始主類別資料
    this.loadMainCategory = function () {

        $http({
            method: 'get',
            url: 'php/apiCategory.php?key=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('category data=' + JSON.stringify(response));
            $scope.catalogData = response.data.data;
            $scope.optionDefault = { id: "", name: "--請選擇--" };
            $scope.catalogData.splice(0, 0, $scope.optionDefault);
            $scope.category = "";

            $scope.subCatalogData = [{ id: "", name: "--請選擇--" }];
            $scope.subcategory = $scope.subCatalogData[0];
            $scope.subcategory = "";
        });
    };

    //跟著主類別更動次類別
    $scope.insertChange = function (category) {
        $scope.subCatalogData = [];
        console.log('insert chg' + category);
        $http({
            method: 'get',
            url: 'php/apiSubcategory.php?category_id=' + category + "&key=",
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('sub data size=' + JSON.stringify(response.data.data));
            $scope.subCatalogData = response.data.data;
            $scope.optionDefault = { id: "", name: "--請選擇--" };
            $scope.subCatalogData.splice(0, 0, $scope.optionDefault);
            $scope.subcategory = "";
        });

    };

    //初始內容商
    this.loadVender = function () {
        $http({
            method: 'get',
            url: 'php/apiVender.php?searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            //console.log('vender data=' + JSON.stringify(response));
            $scope.venderData = response.data.data;
            $scope.optionDefault = { code: "", name: "--請選擇--" };
            $scope.venderData.splice(0, 0, $scope.optionDefault);
            // $scope.status = "";
        });
    };



    vm.loadVender();
    vm.loadMainCategory();
    vm.loadTag();

    //選主類別後，更新次類別資料
    $scope.editChange = function (category_id, info) {
        $scope.subCatalogData = [];
        console.log('edit chg' + category_id);
        console.log('editChange info=' + JSON.stringify(info));
        $http({
            method: 'get',
            url: 'php/apiSubcategory.php?category_id=' + category_id + "&key=",
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('sub data size=' + JSON.stringify(response.data.data));
            $scope.subCatalogData = response.data.data;
            $scope.optionDefault = { id: "", name: "--請選擇--" };
            $scope.subCatalogData.splice(0, 0, $scope.optionDefault);
            $scope.subcategory = "";

            if (info != undefined) {
                vm.content_info = info;
            }
            //$scope.edit_tags = info.tags;

        });
    };


    $scope.fileNameChaged = function () {
        console.log('file is ');
    };
    $scope.onInputChange = function () {
        $scope.resetMsg();
        console.log("input change");
    };

    $scope.resetMsg = function () {
        $scope.showMsg = false;
        $scope.msg = "";
    };

    //批次儲存
    $scope.batchSave = function () {
        console.log('batchSave' + JSON.stringify($scope.content_list));
        angular.forEach($scope.content_list, function (content) {
            var queryData = [{ compandy_id: content.company_id, title: content.title }];
            
            console.log('query=' + JSON.stringify(queryData));
            $http({
                method: 'get',
                url: 'php/getContent.php?company_id=' + content.company_id + '&title=' + content.title,
                data: queryData,
                headers: { 'CONTENT_TYPE': 'application/json' }
            }).then(function (queryRes) {
                console.log('chk exit' + (queryRes.data.data.length == 1) + "," + JSON.stringify(queryRes.data.data));
                if (queryRes.data.data.length == 1) {//upd     
                    console.log('id='+queryRes.data.data[0].id);
                    content.id = queryRes.data.data[0].id;
                    var insertData = [content];
                    $http({
                        method: 'PUT',
                        url: 'php/apiContent.php',
                        data: insertData,
                        headers: { 'CONTENT_TYPE': 'application/json' }
                    }).then(function (updRes) {
                        console.log('更新完成');
                    });
                } else { //insert
                    var insertData = [content];
                    $http({
                        method: 'POST',
                        url: 'php/apiContent.php',
                        data: insertData,
                        headers: { 'CONTENT_TYPE': 'application/json' }
                    }).then(function (insertRes) {
                        console.log('新增完成');
                        $scope.showMsg = true;
                       
                    });
                }
                $scope.msg = '批次新增完成';
                $scope.content_list = "";
                $scope.disableUpload = true;
                $("#file").val('');
            });
        });
    };

    $scope.clearUpload = function () {
        $scope.content_list = "";
        $("#file").val('');
        $scope.msg = "";
    };

    this.delete_content_info = function (info) {
        bootbox.confirm("確定刪除嗎" + JSON.stringify(info), function (confirm) {
            if (confirm) {
                var idx = $scope.content_list.indexOf(info);
                if (idx > -1) {// make sure can index this object                    
                    $rootScope.$apply(function () {
                        $scope.content_list.splice(idx, 1);
                        console.log('Successfully deleted ' + $scope.content_list);
                    });
                }
            }
        });
    };

    $scope.download = function () {
        console.log('download');
        $state.go('/newott2b/php/downloadFile.php');
    };

    //預覽
    $scope.uploadSimple = function () {
        $scope.resetMsg();
        var file = document.getElementById('file').files;
        console.log('file is ');
        console.dir(file[0]);

        var fd = new FormData();
        fd.append('file', file[0]);
        fd.append('MAX_FILE_SIZE', 8 * 1024 * 1024);

        $http({
            method: 'post',
            url: 'php/upload.php',//importContent
            data: fd,
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
        }).then(function (response) {
            console.log('upload success'+ JSON.stringify(response.data));
            // console.log('upload err msg' + JSON.stringify(response.data.errCategory));
            if (response.data.errCategory != undefined) {
                $scope.msg += response.data.errCategory + "  ; \n\r ";
                $scope.showMsg = true;
            }
            if (response.data.errSubCategory != undefined) {
                $scope.msg += response.data.errSubCategory + "  ;\n\r ";
                $scope.showMsg = true;
            }
            if (response.data.errVender != undefined) {
                $scope.msg += response.data.errVender + "  ; \n\r ";
                $scope.showMsg = true;
            }
            if (response.data.errCategory != undefined
                 || response.data.errSubCategory != undefined
                  || response.data.errVender != undefined) {
                    $scope.disableUpload = true;
            } else {
                $scope.disableUpload = false;
            }
            // if (response.data.errFileFormat != undefined) {
            //     $scope.msg += response.data.errFileFormat;
            //     $scope.showMsg = true;
            // }
            $scope.content_list = response.data.data;
        });
    };

    this.selectContent = function (info) {
        // console.log('this id=' + info);
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };

    $scope.uploadFile = function () {
        $scope.onFileSelect($scope.files);
    };

    $scope.onSelect = function ($files) {
        $scope.noFile = false;
        $scope.files = $files;
    };

    $scope.onFileSelect = function ($files) {
        var $file = $files[0];
        console.log('file=');
    };
});