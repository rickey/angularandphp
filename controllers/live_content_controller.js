myApp.controller('live_content_controller', function ($scope, $state, $http, $location) {
    $scope.searchTitle = "";
    var vm = this;      
    $scope.selectedRow = null;
    this.searchTitle = "";
    $scope.currentPage = 1;
    
     //初始tag資料
     this.loadTag = function (query) {
        $scope.tagData = [];
        $http({
            method: 'get',
            url: 'php/apiLiveContent.php?searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('group data=' + JSON.stringify(response.data.data));
            $scope.tagData = response.data.data;            
        });
    };

    
    
    this.search_data = function (search_input) {        
        vm.loadData(1);
    }; 
    this.loadData = function (page_number) {        
        $http({
            method:'get',
            url:'php/apiLiveContent.php?searchTitle='+$scope.searchTitle+ "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('live size=' + JSON.stringify(response)); 
            vm.live_list = response.data.data;
            $scope.total_row = response.data.total;            
            console.log('searchTitle='+$scope.searchTitle);
        });
    };

//    this.loadData(1);


    this.add = function (info) {    
        console.log('startTime=' + $scope.insert_start_time);
        var insert_start_time = document.getElementById("insert_start_time");
        console.log('startTime=' + insert_start_time.value);
        info.start_time = insert_start_time.value;

        var insert_end_time = document.getElementById("insert_end_time");
        console.log('insert_end_time=' + insert_end_time.value);
        info.end_time = insert_end_time.value;
       
        info.company_id = localStorage.getItem('company_id');
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiLiveContent.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            vm.loadData(1); 
            document.getElementById('close-modal').click();          
        });
    };

    this.selectContent = function (info) {                
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.lu_id;
        }
    };

    this.edit_content_info = function (lu_id) {
        $('#edit_content_info_modal').modal('show');           
        console.log('edit id=' + lu_id);
        $http({
            method: 'get',
            url: 'php/apiLiveContent.php?lu_id=' + lu_id + '&searchTitle=&page=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('live data=' + JSON.stringify(response.data.data[0]));
            vm.content_info = response.data.data[0];            
        });
    };

    this.updateContent = function () {
        console.log('startTime=' + $scope.edit_start_time);
        var edit_start_time = document.getElementById("edit_start_time");
        console.log('startTime=' + edit_start_time.value);
        this.content_info.start_time = edit_start_time.value;

        var edit_end_time = document.getElementById("edit_end_time");
        console.log('edit_end_time=' + edit_end_time.value);
        this.content_info.end_time = edit_end_time.value;
        console.log(JSON.stringify(this.content_info));
        var FormData = [this.content_info];
        $http({
            method: 'PUT',
            url: 'php/apiLiveContent.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('data=' + JSON.stringify(response));               
            document.getElementById('close-modal2').click();
            vm.loadData($scope.currentPage);
        });
    };

    this.delete_content_info = function (id) {
        bootbox.confirm("確定刪除嗎", function (confirm) {
            if (confirm) {
                var FormData = [{ id: id }];
                $http({
                    method: 'DELETE',
                    url: 'php/apiLiveContent.php',
                    data: FormData,
                    headers: { 'CONTENT_TYPE': 'application/json' }
                }).then(function (response) {
                    vm.loadData($scope.currentPage);
                    $('.modal').modal('hide');
                });
            }
        });
    };

    $scope.$watch('currentPage + numPerPage', function () {
        vm.loadData($scope.currentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });
});