myApp.controller('account_controller', function ($scope, $state, $http, $location) {
    $scope.searchTitle = "";
    $scope.msg_area = false;
    var vm = this;      
    $scope.selectedRow = null;
    this.searchTitle = "";
    $scope.currentPage = 1;
    
    $scope.init = function () {     
        console.log('super_user='+localStorage.getItem('super_user'));
        $scope.super_user = localStorage.getItem('super_user'); 
        console.log('admin='+localStorage.getItem('admin'));
        $scope.admin = localStorage.getItem('admin'); 
    };   

    $scope.init();


     //初始tag資料
     $scope.loadTag = function (query) {
        $scope.tagData = [];
        $http({
            method: 'get',
            url: 'php/apiGroup.php?searchTitle=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('group data=' + JSON.stringify(response.data.data));
            $scope.tagData = response.data.data;            
        });
    };

    this.loadContentTag = function (id) {
        $http({
            method: 'get',
            url: 'php/apiAccountGroup.php?id='+id,
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('account_group tag data=' + JSON.stringify(response));
            $scope.edit_tags = response.data.data;
            $scope.view_tags = response.data.data;
        });
    };
    
    $scope.initAddTag = function () { 
        $scope.accounts.admin = "0"  ;
        $('#create_content_info_modal').modal('show');          
        $scope.msg = "";
        $scope.msg_area = false;   
        console.log('initAddTag');
        $scope.loadTag();
    };

    
    this.search_data = function (search_input) {        
        vm.loadData(1);
    }; 
    this.loadData = function (page_number) {        
        $http({
            method:'get',
            url:'php/apiAccount.php?searchTitle='+$scope.searchTitle+ "&page=" + page_number,
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
            console.log('account size=' + JSON.stringify(response)); 
            vm.account_list = response.data.data;
            $scope.total_row = response.data.total;            
            console.log('searchTitle='+$scope.searchTitle);
        });
    };

//    this.loadData(1);


    this.add = function (info) {    
        // console.log('startTime=' + $scope.insert_expire_time);
        var insert_expire_time = document.getElementById("insert_expire_time");        
        info.expire_time = insert_expire_time.value;
        console.log('insert data=' + JSON.stringify(info));
        info.company_id = localStorage.getItem('company_id');
        var FormData = [info];
        $http({
            method:'POST',
            url:'php/apiAccount.php',
            data: FormData,
            headers:{'CONTENT_TYPE': 'application/json'}           
          }).then(function (response) {
              console.log('add response ='+ JSON.stringify(response) +','+response.data.code);
              console.log(response.data.code === "400");
              if (response.data.code === "400") {
                $scope.msg = '新增失敗，帳號重覆!!!!';
                $scope.alert_style = "alert-danger";
                $scope.msg_area = true;
              } else {
                $scope.msg = '新增成功!!!!';
                $scope.alert_style = "alert-success";
                $scope.msg_area = true;
              }
              vm.loadData(1); 
              document.getElementById('close-modal').click();          
        });
    };

    this.selectContent = function (info) {                
        if ($scope.selectContent !== null) {
            $scope.selectedRow = info.id;
        }
    };

    this.edit_content_info = function (id) {
        $scope.msg_area = false;
        $('#edit_content_info_modal').modal('show');   
        this.loadContentTag(id);            
        console.log('edit id=' + id);
        $http({
            method: 'get',
            url: 'php/apiAccount.php?id=' + id + '&searchTitle=&page=',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            console.log('data=' + JSON.stringify(response.data.data[0]));
            vm.content_info = response.data.data[0]; 
            if (response.data.data[0].admin == null) {
                vm.content_info.admin = "0";
            }
        });
    };

    this.updateContent = function () {
        console.log('startTime=' + $scope.edit_expire_time);
        var edit_start_time = document.getElementById("edit_expire_time");
        console.log('startTime=' + edit_expire_time.value);
        this.content_info.expire_time = edit_expire_time.value;
        if (this.content_info.admin === "0") {
            this.content_info.admin = null;
        }
        var FormData = [this.content_info];
        $http({
            method: 'PUT',
            url: 'php/apiAccount.php',
            data: FormData,
            headers: { 'CONTENT_TYPE': 'application/json' }
        }).then(function (response) {
            // console.log('data=' + JSON.stringify(response));               
            document.getElementById('close-modal2').click();
            vm.loadData($scope.currentPage);
            $scope.msg = '更新成功!!!!';
            $scope.alert_style = "alert-success";
            $scope.msg_area = true;
        });
    };

    this.delete_content_info = function (id) {
        bootbox.confirm("確定刪除嗎", function (confirm) {
            if (confirm) {
                var FormData = [{ id: id }];
                $http({
                    method: 'DELETE',
                    url: 'php/apiAccount.php',
                    data: FormData,
                    headers: { 'CONTENT_TYPE': 'application/json' }
                }).then(function (response) {
                    vm.loadData($scope.currentPage);
                    $('.modal').modal('hide');
                });
            }
        });
    };

    $scope.$watch('currentPage + numPerPage', function () {
        vm.loadData($scope.currentPage);
        var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                , end = begin + $scope.numPerPage;
    });
});