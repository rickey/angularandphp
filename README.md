20200609
cicd將資料庫的連線抽離：
1：把要用資料庫的controller改用conn::db()
rest::go(rest::getReq(),conn::db());
2：orm.php加上
require_once('/tmp/conn.php');
conn.php放在根目錄


<b>本機執行方式</b>
$cd C:\xampp
$xampp_start.exe
DB設定：orm.Php

# ANGULARJS PHP MySQL CRUD
<b>Resource:</b> 
1. HTML 
2. CSS 
3. Bootstrap 
4. Angularjs
5. PHP
6. MySQL


<b>Features:</b> 
1. Create 
2. Update 
3. View 
4. Delete
5. Search Option
6. Pagination

<b>環境</b>
環境需求：PHP v5.4、MYSQL v5.7.12 x86_64(linux_glibc2.5)
Server version: Apache/2.2.15 (Unix)
檔案位置：/home/www/htdocs/ott2b
執行方式：重啟Apache (sudo service httpd start/stop)
監控方式：WEB http://IP位址/ott2b/psm  =>登入帳/密：rickey/rickey
API說明文件： http://IP位址/ott2b/dist/
設定檔：/otp/remi/php54/root/home/php.ini
dbname：ott2b

<b>改版說明</b>
20190523片長欄位相關修改：vod編輯畫面、發佈及下架api已加上片長欄位
	1.新增content欄位run_time
	2.getLaunch.php新增此欄位
	3.getRecall.php新增此欄位
修改vod 內容時，只有下列條件成立，才更改狀態為2
	條件一：狀態為0(檔案未關聯)、3(檔案處理完成)、4(已審查)、100(檔案處理失敗)
	條件二：檔案路徑不為空


<b>編碼</b>
PHP 版本是 5.4 以上的版本支援：
rest.php的rest加上json_encode("我愛水煮魚", JSON_UNESCAPED_UNICODE); 
影響程式：getLaunch.php (http://localhost/angularjs-php-mysql-crud-master/php/getLaunch.php顯示content.status(6-已審核,7-已上架)的json格式)

緊急下架：content判斷是過去end_time最近已下架x小時 =>  (現時間-n小時) < end_time < 現在時間

<a  href="http://dev.techcanvas.org/angularjs-php-mysql-crud-demo/" target="_blank" >Demo</a>
