<?php

//測試company
/*
RESPONSE CODE：
    400 =>bad request
    404 =>no company data
    200 =>ok
*/
class testCreateCDN extends PHPUnit_Framework_TestCase
{
    // //return code=400, MSG=Bad Request
    public function test_AddCompany_400()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createCDN.php';
        $curl = curl_init($service_url);    
        $curl_post_data = json_encode(array([
            "message" => "test message"
        ]));

        
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $curl, $options );
        echo $result = curl_exec($curl);        

        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj){
            echo "test 400 Bad Request=".$obj->code;
            $this->assertEquals("400",$obj->code);
        }       
        curl_close($curl);        
    }

    //測試回傳：return code=200, MSG=OK
    public function test_AddCompany_200()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createCDN.php';        
        $curl = curl_init($service_url);
        $curl_post_data = json_encode(array([
            "vender_name"=>"HBO電影台4",
            "vender_code"=>"HBO4",
            "company_code"=>"CHT",            
            "domain_names"=>["domain.name2", "domain.name3"],
            "optional"=>"其他"
        ]));       

        $ch = curl_init($service_url);
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $ch, $options );
        echo $result = curl_exec($ch);
        
        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj){
             echo "test 200 , OK=".$obj->code;
             $this->assertEquals("200",$obj->code);
        }       
        curl_close($curl);        
    }

    //測試回傳：return code=404, MSG=No Company Data Found
    public function test_AddCompany_404()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createVender.php';        
        $curl = curl_init($service_url);
        $curl_post_data = json_encode(array([
            "vender_name"=>"HBO電影台1",
            "vender_code"=>"HBO3",
            "company_code"=>"CHT22",           
            "domain_names"=>["domain.name1", "domain.name2"],
            "optional"=>"其他"
        ]));       

        $ch = curl_init($service_url);
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $ch, $options );
        echo $result = curl_exec($ch);
        
        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj){
             echo "test 404 No Company Data Found=".$obj->code;
             $this->assertEquals("404",$obj->code);
        }       
        curl_close($curl);        
    }
}
?>