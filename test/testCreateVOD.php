<?php

//測試company
/*
RESPONSE CODE：
    400 =>bad request
    404 =>no company data
    200 =>ok
*/
class testCreateVender extends PHPUnit_Framework_TestCase
{
    // //return code=400, MSG=Bad Request
    public function test_AddCompany_400()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createVOD.php';
        $curl = curl_init($service_url);    
        $curl_post_data = json_encode(array([
            "message" => "test message"
        ]));

        
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $curl, $options );
        echo $result = curl_exec($curl);        

        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj){
            echo "test 400 Bad Request=".$obj->code;
            $this->assertEquals("400",$obj->code);
        }       
        curl_close($curl);        
    }

    //測試回傳：return code=200, MSG=OK
    public function test_AddCompany_200()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createVOD.php';        
        $curl = curl_init($service_url);
        $url_arr = new stdClass();        
        $url_arr->format = "a";
        $url_arr->quality = "b";
        $url_arr->device = "c";
        $url_arr->drm = "d";
        $url_arr->url = "e";
        $url_arr2 = new stdClass();
        $url_arr2->format = "a2";
        $url_arr2->quality = "b2";
        $url_arr2->device = "c2";
        $url_arr2->drm = "d2";
        $url_arr2->url = "e2";
        $response = array($url_arr, $url_arr2);
           
        $curl_post_data = json_encode(array([
            "company_name" => "中華",
            "company_code" => "CHT",   
            "vod_title" => "text0402",
            "channel_name" => "CHT_CHANNEL",   
            "urls" => $response,
            "optional" => "其他"
        ]));       

        $ch = curl_init($service_url);
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $ch, $options );
        echo $result = curl_exec($ch);
        
        $data= json_decode('['.$result.']');//得到的是 object
        echo '200->'.print_r($result);
        foreach($data as $obj){
             echo "test 200 , OK=".$obj->code;
             $this->assertEquals("200",$obj->code);
        }       
        curl_close($curl);        
    }

    //測試回傳：return code=404, MSG=No Company Data Found
    public function test_AddCompany_404()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createVOD.php';        
        $curl = curl_init($service_url);
        $url_arr = json_encode(array([           
            "format"=>"aa2","quality"=>"bb2","devie"=>"cc2","drm"=>"dd2","url"=>"ee2"
            ]));
        $curl_post_data = json_encode(array([
            "company_name" => "中華",
            "company_code" => "CHT222",   
            "vod_title" => "text0402",
            "channel_name" => "CHT_CHANNEL",   
            "urls" => $url_arr,
            "optional" => "其他"
        ]));          

        $ch = curl_init($service_url);
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $ch, $options );
        echo $result = curl_exec($ch);
        
        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj){
             echo "test 404 No Company Data Found=".$obj->code;
             $this->assertEquals("404",$obj->code);
        }       
        curl_close($curl);        
    }
}
