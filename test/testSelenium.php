<?php
class GitHubTests extends PHPUnit_Framework_TestCase {

    protected $webDriver;

	public function setUp()
    {
        $capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'chrome');
        $this->webDriver = RemoteWebDriver::create('http://localhost:4444/wd/hub', $capabilities);
    }

    protected $url = 'https://github.com';
   
    public function testGitHubHome()
    {
        $this->webDriver->get($this->url);
        // checking that page title contains word 'GitHub'
        $this->assertContains('GitHub', $this->webDriver->getTitle());
    }  
}


// require_once "./php-webdriver/lib/__init__.php";
// $capabilities = array(
//     WebDriverCapabilityType::BROWSER_NAME => 'chrome'
// );
// $seleniumUrl = 'http://localhost:4444/wd/hub';
// $driver = RemoteWebDriver::create($seleniumUrl, $capabilities, 5000);
// $driver->get("http://www.google.com.tw/");
// sleep(5);
// $driver->close();
?>