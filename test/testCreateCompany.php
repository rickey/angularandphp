<?php

//測試company
/*
RESPONSE CODE：
    400 =>bad request
    404 =>no company data
    200 =>ok
*/
class testCreateCompany extends PHPUnit_Framework_TestCase
{
    //測試1-post data亂key
    public function test_AddCompany_404()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createCompany.php';
        $curl = curl_init($service_url);    
        $curl_post_data = json_encode(array([
            "message" => "test message" 
        ]));
        
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $curl, $options );
        echo $result = curl_exec($curl);        

        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj){
            echo "test 400 bad request=".$obj->code;
            $this->assertEquals("400",$obj->code);
        }       
        curl_close($curl);        
    }


    public function test_AddCompany_200()
    {        
        $service_url = 'http://127.0.0.1/ott2b/php/createCompany.php';        
        $curl = curl_init($service_url);
        $curl_post_data = json_encode(array([
            "company_name" => "中華",
            "company_code" => "CHT01",
            "api_user" => "C方案帳號2(tenant)",
            "api_pass" => " C方案密碼v2(tenant)",
            "optional" => "其他"
        ]));       

        $ch = curl_init($service_url);
        $options = array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,            
            CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
            CURLOPT_POSTFIELDS => $curl_post_data
        );
        curl_setopt_array( $ch, $options );
        echo $result = curl_exec($ch);
        
        $data= json_decode('['.$result.']');//得到的是 object
        foreach($data as $obj) {
             echo "test 200 code2=".$obj->code;
             $this->assertEquals("200",$obj->code);
        }       
        curl_close($curl);        
    }
}
?>