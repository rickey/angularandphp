//20190812-新增上傳時，顯示原始檔名
ALTER TABLE content
ADD COLUMN file_name VARCHAR(200) NULL;

//20190812-刪除不需要的欄位
ALTER TABLE vender
DROP COLUMN ftp_ip;

//20190709
create  table  ftp_config
(
	id bigint auto_increment
		primary key,
	ftp_ip  varchar(45) null,
	ftp_port varchar(45) null
);


INSERT INTO ftp_config ( ftp_ip, ftp_port) VALUES ('172.16.41.200', '20021');


//20190701
ALTER TABLE vender
ADD COLUMN ftp_ip VARCHAR(200) NULL;

//20190619
ALTER TABLE rss_config
ADD COLUMN is_http VARCHAR(20) NULL;

//20190619
ALTER TABLE rss_config
ADD COLUMN is_http VARCHAR(20) NULL;

//20190523
ALTER TABLE content
ADD COLUMN run_time VARCHAR(200) NULL;


//0408
alter table liverss_config alter column recall_day set default 1;
alter table liverss_config alter column limit_rows set default 1000;

alter table rss_config alter column limit_rows set default 1000;
alter table rss_config alter column recall_hour set default 24;


//0329
ALTER TABLE `content` 
ADD COLUMN `channel_name` VARCHAR(200) NULL;




//20190326
ALTER TABLE rss_config DROP COLUMN  recall_day;

create   table  liverss_config
(
	id int auto_increment
		primary key,
	token varchar(45) not null,
	company_id bigint null,
	limit_rows bigint default 0 null,
	recall_day bigint null,
	constraint live_config_token_uindex
		unique (token)
);




//20190325
create  table  live
(
	id bigint auto_increment
		primary key,
	channel_id varchar(32) null,
	display_number varchar(45) null,
	title varchar(200) null,
	start_time datetime null,
	end_time datetime null,
	company_id bigint(20) null,
	company varchar(45) null,
	status bigint null,
	description varchar(500) null,
	comment varchar(255) null,
	update_time datetime null,
	hami_billing_code varchar(255) null,
	program_recording varchar(255) null,
	vender_id varchar(0) null
)
collate=utf8_unicode_ci;

create  table live_epg
(
	id bigint auto_increment
		primary key,
	live_id bigint(20) null,
	text varchar(200) null,
	start_time datetime null,
	end_time datetime null,
	update_time datetime null,
	description varchar(1000) null,
	comment varchar(1024) null,
	category varchar(255) null,
	subcategory varchar(255) null
)
collate=utf8_unicode_ci;

create   table  live_url
(
	id bigint auto_increment
		primary key,
	live_id bigint(20) null,
	company varchar(45) null,
	format varchar(45) null,
	device varchar(45) null,
	quality varchar(45) null,
	bitrate varchar(45) null,
	drm varchar(45) null,
	url varchar(500) null
)
collate=utf8_unicode_ci;

ALTER TABLE `live` 
ADD COLUMN `company_id` BIGINT(20) NULL AFTER `update_time`;


ALTER TABLE `rss_config` 
ADD COLUMN `recall_hour` BIGINT(20) NULL ;





//20190320  新增vender三個欄位
alter table vender add column transcode_profiles TEXT NULL;
alter table vender add column publish_profiles TEXT NULL;
alter table vender add column domain_names TEXT NULL;




//0314
ALTER TABLE `vender`
ADD COLUMN `pass_audit` BOOLEAN NULL ;

alter table rss_config add column limit_rows varchar(45) NULL;
alter table rss_config alter column limit_rows set default 1000;

UPDATE `ott2b`.`status` SET `name`='檔案處理完成/待審核' WHERE `id`='4';
UPDATE `ott2b`.`status` SET `name`='已審核/已發佈' WHERE `id`='8';


//0313
create   table  rss_config
(
	id int auto_increment
		primary key,
	token varchar(45) not null,
	company_id bigint null,
	http varchar(20) null,
	url varchar(200) null,
	recall_day bigint default 0 null,
	constraint rss_config_token_uindex
		unique (token)
);





//0312
ALTER TABLE `ott2b`.`account` 
ADD COLUMN `company_id` BIGINT(20) NULL AFTER `expire_time`,
ADD COLUMN `super_user` INT NULL AFTER `company_id`,
ADD COLUMN `admin` INT NULL AFTER `super_user`;

ALTER TABLE `ott2b`.`company` 
ADD COLUMN `company_name` VARCHAR(45) NULL AFTER `used_storage_size`;

create   table  group_account
(
	id bigint auto_increment
		primary key,
	group_id bigint null,
	account_id bigint null
);

create   table group_vender
(
	id bigint auto_increment
		primary key,
	group_id bigint null,
	vender_id bigint null
);


ALTER TABLE  `group` 
ADD COLUMN `company_id` BIGINT(20) NULL AFTER `company`;


ALTER TABLE  `funclist` 
ADD COLUMN `company_id` BIGINT(20) NULL AFTER `seq`;

ALTER TABLE `vender` 
ADD COLUMN `company_id` BIGINT(20) NULL AFTER `ftp_pass`;


//0306
create   table role_account
(
	id bigint auto_increment
		primary key,
	role_id bigint null,
	account_id bigint null
);

create   table  role_function
(
	id bigint auto_increment
		primary key,
	role_id bigint null,
	function_id bigint null
);

ALTER TABLE `role` 
ADD COLUMN `company_id` BIGINT(20) NULL AFTER `code`;




//0305
create   table funclist
(
	id bigint auto_increment
		primary key,
	name varchar(45) null,
	code varchar(45) null,
	url varchar(200) null,
	level bigint(2) default 1 null,
	parent_id bigint null,
	seq bigint(2) null
)
collate=utf8_unicode_ci;




create  table account
(
	id bigint auto_increment
		primary key,
	name varchar(45) null,
	company varchar(45) default 1 null,
	group_id bigint null,
	pass varchar(45) null,
	expire_time datetime null
)
collate=utf8_unicode_ci;

create  table account_group
(
	id bigint auto_increment
		primary key,
	account_id bigint null,
	group_id bigint null
);

create  table category
(
	id bigint auto_increment
		primary key,
	company_id bigint default 1 null,
	name varchar(45) null
)
collate=utf8_unicode_ci;

create  table subcategory
(
	id bigint auto_increment
		primary key,
	company_id bigint default 1 null,
	category_id bigint null,
	name varchar(45) null
)
collate=utf8_unicode_ci;


create  table company
(
	id bigint auto_increment
		primary key,
	company varchar(45) null,
	ftp_user varchar(45) null,
	ftp_pass varchar(45) charset utf8 null,
	ftp_dir varchar(45) null,
	live_stations int null,
	used_live_stations int null,
	storage_size bigint null,
	used_storage_size bigint null
)
collate=utf8_unicode_ci;

create  table content
(
	id bigint auto_increment
		primary key,
	vender_id bigint null,
	company_id bigint default 1 null,
	category_id bigint null,
	subcategory_id bigint null,
	title varchar(100) null,
	status bigint null,
	description text null,
	tag varchar(100) null,
	runtime int null,
	start_time datetime null,
	end_time datetime null,
	thumbnail varchar(200) null,
	rating_id bigint null,
	publish_year varchar(45) null,
	create_time datetime null,
	update_time datetime null,
	file_path varchar(255) null,
	preview_url varchar(255) null,
	comment varchar(1024) null,
	job_id bigint null,
	job_note varchar(255) null
)
collate=utf8_unicode_ci;

create  table content_tag
(
	id bigint auto_increment
		primary key,
	content_id bigint null,
	tag_id bigint null,
	company varchar(200) default 'SUPER' null
);

create  table `group`
(
	id bigint auto_increment
		primary key,
	name varchar(45) null,
	company varchar(45) null,
	constraint group_company_uindex
		unique (company)
)
collate=utf8_unicode_ci;

create  table role
(
	id bigint auto_increment
		primary key,
	name varchar(45) null,
	code varchar(45) null
)
collate=utf8_unicode_ci;

create  table rules
(
	account_id varchar(255) null,
	group_id varchar(255) null,
	rule_id varchar(255) null,
	function_id varchar(255) null
);

create  table status
(
	id bigint auto_increment
		primary key,
	code int null,
	name varchar(100) null
)
collate=utf8_unicode_ci;

create  table  tag
(
	id bigint auto_increment
		primary key,
	text varchar(200) charset utf8 null
);

create  table  url
(
	id bigint auto_increment
		primary key,
	job_id bigint null,
	format varchar(45) null,
	device varchar(45) null,
	quality varchar(45) null,
	bitrate varchar(45) null,
	drm varchar(45) null,
	url varchar(200) null,
	updatetime datetime null,
	createtime datetime null,
	type int null
)
collate=utf8_unicode_ci;

create  table  vender
(
	id bigint auto_increment
		primary key,
	name varchar(45) null,
	un varchar(45) null,
	company varchar(45) default 'SUPER' null
)
collate=utf8_unicode_ci;

